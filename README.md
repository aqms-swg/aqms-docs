
:no_entry_sign: Caution, work in progress :no_entry_sign:

This repository contains source files for documentation as well as configuration 
files for Doxygen. By specifying the other aqms.swg repositories as git 
submodules in the .gitmodule file, it allows all the documentation to be created 
from this "superproject".

See the documentation rendered here: https://aqms-swg.gitlab.io/aqms-docs

Contributors: To learn how your documentation can show up there, see:

https://aqms-swg.gitlab.io/aqms-docs/how-to-contrib-aqms-docs.html
