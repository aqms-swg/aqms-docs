# Man Page  Template {#man-template}

Use this template to create a man page you have been asked to write.

## PAGE LAST UPDATED ON

     date of the last update made on this page

## NAME

     provide name of the software here

## VERSION and STATUS

     You can set status to any of the following:
      ACTIVE,DEPRECATED,UNDER DEVELOPMENT,UNKNOWN

## PURPOSE

     provide information on why it exists and what does it do.

## HOW TO RUN

     provide information on how to run this software
     and about all command-line options and execution modes.

## CONFIGURATION FILE

     provide complete information about configuration file format and parameters in following format.

     <parameter name> <data type> <default value>
               <description>

     <parameter name> <data type> <default value>
               <description>

## ENVIRONMENT

   If this program can be influenced by environment variables, describe them here.

## DEPENDENCIES

     List AQMS software, third party softwares and others resources (e.g database tables, files) it depends on to work properly.

## MAINTENANCE

     give information on how to maintain its execution/operations under production environment without site specific details.

## BUG REPORTING

    Link to gitlab issues

## MORE INFORMATION

    Provide links to other documentation related to the software.
