# System Requirements {#aqms-requirements}  
  

The following are software environment requirements for all productions systems.  
  
### All Systems  
  
*  Linux - AQMS tested to work on RHEL/CentOS 6,7
*  Perl - latest rev.
*  Java - latest rev.
*  Oracle 12 release 2 (
*  pcap library is need in /usr/local (from sunfreeware.com)
*  Xerces C/C++ 2.7.0 - XML library
*  TAO/ACE 1.3a (PATCH 10)
*  NTP sync of system clocks
*  AQMS code from http://gitlab.com/aqms-swg  
    
  

### Additional Requirements: RT Systems  
  
*  qlib2 from UCBerkeley (UCB version)  
*  Earthworm 7.2 or greater  
*  Oracle 12 release 2 
*  OTL 4.0.68 (note, this is from 2003)  
*  gmake (needed for Xerces, ACE/TAO compilations)  
*  pcap library is need in /usr/local (from sunfreeware.com)  
*  AQMS code from http://gitlab.com/aqms-swg  


### Additional Requirements: PostProcessing Systems  
  
* AQMS code http://gitlab.com/aqms-swg  
    *  Optional post processing modules - depend on function of the machine
        *  PCS  
        *  Amplitude import/export  
        *  Waveform import/export  
        *  Solution servers  
        *  Focal mechanism

