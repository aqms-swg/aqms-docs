# %Database {#db-overview}
  
  
### %Database Overview  
 
Everything needed to set up an AQMS Oracle database.  

The AQMS database schema was developed at the CISN. CISN schema used by AQMS is officially documented at NCEDC at http://www.ncedc.org/db/Documents/NewSchemas/schema.html  
      
There is a database schema working group that works through changes to this schema and an official request policy for changes. You can find the minutes of all the meetings at the NCEDC web site http://www.ncedc.org/db/SCWG. Contact Stephane Zuzlewski at UC Berkeley for requests for now.    
  
- <a href="http://www.ncedc.org/db/Documents/NewSchemas/schema.html">Schema Documentation</a>
- \subpage schema-change-notes  
- \subpage default-db-users  
- \subpage adding-event-types
- \subpage aqms-stored-procs
  - <a href="ampsetmaker-stored-package.html">AMPSETMAKER</a>
  - <a href="assocamp-stored-package.html">ASSOCAMP</a>
  - <a href="copydata-stored-package.html">COPYDATA</a>
  - <a href="datum-stored-package.html">DATUM</a>
  - <a href="db_cleanup-stored-package">DB_CLEANUP</a>
  - <a href="deltaz-stored-package">DELTAZ</a>
  - <a href="epref-stored-package">EPREF</a>
  - <a href="eventpriority-stored-package">EVENTPRIORITY</a>
  - <a href="file_util-stored-package">FILE_UTIL</a>
  - <a href="formats-stored-package">FORMATS</a>
  - <a href="geo_region-stored-package">GEO_REGION</a>
  - <a href="importevent-stored-package">IMPORTEVENT</a>
  - <a href="irinfo-stored-package">IRINFO</a>
  - <a href="magpref-stored-package">MAGPREF</a>
  - <a href="match-stored-package">MATCH</a>
  - <a href="orgpref-stored-package">ORGPREF</a>
  - <a href="pcs-stored-package">PCS</a>
  - <a href="quarry-stored-package">QUARRY</a>
  - <a href="requestcard-stored-package">REQUESTCARD</a>
  - <a href="sequence-stored-package">SEQUENCE</a>
  - <a href="schema_util-stored-package">SCHEMA_UTIL</a>
  - <a href="truetime-stored-package">TRUETIME</a>
  - <a href="util-stored-package">UTIL</a>
  - <a href="wave-stored-package">WAVE</a>
  - <a href="wavearc-stored-package">WAVEARC</a>
  - <a href="wavefile-stored-package">WAVEFILE</a>
  - <a href="wheres-stored-package">WHERES</a>

- \subpage db-map  
  
- \subpage ora-db
- \subpage db-postgres-overview
