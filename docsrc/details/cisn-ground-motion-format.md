# CISN Ground Motion Message {#cisn-ground-motion-format}  
  
This message format for the exchange of amplitude information was adopted by CISN in October 2010.   
  
1 Ver. 3.0.1 2011/05/18   
  
Ground motion message packets are intended to characterize and communicate key aspects about the ground motion recorded at a site to other network centers for use in applications such as ShakeMaps. Station metadata is not included in the package as it is assumed to be available from lookup files. Each message packet contains parameters for the motion on a single channel. The message packets for all channels of a record may be concatenated into a single file for transmission. The ground motion file name is not prescribed except that the file name ends with a 3-character suffix like “.gXY”, where XY is the 2-character network code of the network generating the packet. The message packet does not include explicit quality indication factors since by agreement CISN networks do not distribute ground motion packets that do not meet internal quality control standards.   
  
Example :  
  
```
SCNL: CMB.BHZ.BK.
WINDOW: 2001/02/25 02:36:57.000 LENGTH: 45.000
PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000 METH: MS
PGV: 1.0400 TPGV: 2001/02/25 02:37:07.000 METH: IF 3.3
PGD: 0.0250 TPGD: 2001/02/25 02:37:10.000 METH: IR KMH
SA: 0.3 4.41541 TSA: 2001/02/25 02:37:05.000 METH: NJ
SA: 1.0 0.92562 TSA: 2001/02/25 02:37:08.000 METH: NJ
SA: 3.0 0.29793 TSA: 2001/02/25 02:37:12.000 METH: KMH
EVID: NC 41059467
AUTH: CI 2001/02/25 02:40:30.000
```
  
## Explanation of Tags  
  
**SCNL:**  Station Identifier Code, Channel Identifier, Network Code, and Location Identifier, in dot-separated fields. The fields are derived from the FDSN standards (https://www.fdsn.org)  
  
    1.  Station Identifier Code – Station code, number or identification, defined by the network.  
    2.  Channel Identifier Code – Code of the recording channel.
    3.  Network – Network code.
    4.  Location – Channel sensor location identifier (a blank or „–„ may be used if there is no value for the location code)  
      
Note on times: All times are in UTC, in the format yyyy/mo/dy hr:mn:ss.sss (year, month, day, hour, minute, seconds to thousandths). If no time value is available, the field should be set to the null date-time string, “0000/00/00 00:00:00.000”.   
 
**WINDOW START:** Date-time stamp giving the start time of the record (i.e., the time of the first sample scanned).  

**LENGTH:** Length of time segment scanned in seconds (i.e., time to last sample).  
  
**PGA:** Peak ground acceleration (in cm/sec/sec).  
  
**TPGA:** Time that the peak occurred.  
  
**METH:** Method used to determine the peak value. Either:  
  
    1.  MS – Measured, then scaled to cm/sec/sec  
    2.  DV – Differentiation from measured velocity  
    3.  Ver. 3.0, 9/21/10  
  
**PGV:** Peak ground velocity (in cm/sec).  
  
**TPGV:** Time that the peak occurred.   
  
**METH:** Method used to determine the peak value. Either:  
  
    1.  MS – Measured, then scaled to cm/sec  
    2.  IF x.x – Integration, frequency domain, with long-period filter corner (in sec)  
    3.  IT x.x – Integration, time domain, with long-period filter corner (in sec)  
    4.  IR – Integration, recursive; followed by “KMH” if Kanamori et al. method is used (which does not have an explicit long period corner), otherwise long-period filter corner (sec).  
  
**PGD:** The peak ground displacement (in cm).  
  
**TPGD:** Time that the peak occurred.  
  
**METH:** Method used to determine the peak value. Either:  
  
    1.  IF x.x – Integration, frequency domain, with long period corner (in sec)  
    2.  IT x.x – Integration, time domain, with low frequency filter corner (in sec)  
    3.  IR – Integration, recursive, followed by “KMH” if Kanamori et al. method used, as above.  
  
**Spectral Acceleration Lines:** Each spectral acceleration line has information on the response spectral acceleration (Sa) at a certain period. Specifically, the line has the period, the peak Sa value at that period, the time of the peak and the method used to determine Sa. One line is used for each period; at this time, CISN produces response spectrum maps at three periods, 0.3 seconds, 1.0 seconds, and 3.0 seconds. The fields of each line are:  
  
**SA:** Period (in sec) followed by the peak Sa value at this period (in cm/sec/sec) 
  
**TSA:** Time that the peak occurred.  
  
**METH:** Method used to determine the Sa value. Either:  
  
    1.  NJ – Classical method of Nigam and Jennings (BSSA, 1969), with 5% damping.
    2.  KMH – Recursive approximate method of Kanamori, Maechling and Hauksson (BSSA, 1999), in which SD is determined and the pseudo spectral acceleration (PSA) is calculated as PSA = (2π/T)2PSD, and taken equivalent to Sa.
  

Note on Values and Precision: Precision of at least 4 or more decimal places, as in the example, if warranted by the precision of the computation, is important, especially for small earthquakes. Any numeric values that are not available should be assigned a value of -1.0, the null value. None of the amplitude lines are required, though they are important for the targeted application of the packet.  
  
**EVID:** Identification of the earthquake associated with the record (2-character Network code followed by the EventId assigned by the authoritative network for the earthquake). If the record has not been associated with a specific event (e.g., data from a self-triggered field instrument, and not yet associated), a '–' should be used for the network and the EventId.  
  
**AUTH:** Network code of the network that created the ground motion packet followed by the time it was created.   

