# AQMS Application Stored Procedure Usage {#db-map}    
  
## C++ code  

| Program | Library/Method | Stored Procedures | Comments |
------- | -------------- | ----------------- | ----------  
| [alarmact](https://aqms-swg.gitlab.io/aqms-docs/man-alarmact.html) | otl | | |
| [alarmdec](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdec.html) | otl | [truetime](stored-packages/truetime.md) | |
| [alarmdist](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdist.html) | otl | | |
| [alarmevent](https://aqms-swg.gitlab.io/aqms-docs/man-alarmevent.html) | otl | | |
| [ampgen](https://aqms-swg.gitlab.io/aqms-docs/man-ampgen.html) | otl | [assocamp](stored-packages/assocamp.md),[truetime](stored-packages/truetime.md) | |
| [ec](https://aqms-swg.gitlab.io/aqms-docs/man-ec.html) | otl | [truetime](stored-packages/truetime.md), [geo_region](stored-packages/geo_region.md), [epref](stored-packages/epref.md), [sequence](stored-packages/sequence.md) | |
| ??? ew2wda - deprecated? | otl | | |
| ??? ewmag2cisn - deprecated ? | otl | [epref](stored-packages/epref.md), [magpref](stored-packages/magpref.md), [truetime](stored-packages/truetime.md) | used by most RSNs with localmag or specmag (Mw) |
| makewda | otl | | |
| [pws](https://aqms-swg.gitlab.io/aqms-docs/man-proxy-waveserver.html) | otl | | |
| [rad2](https://aqms-swg.gitlab.io/aqms-docs/man-rad2.html) | otl | | |
| [sendcancel](https://aqms-swg.gitlab.io/aqms-docs/man-sendcancel.html) | otl | | |
| [tc](https://aqms-swg.gitlab.io/aqms-docs/man-tc.html) | otl | [truetime](stored-packages/truetime.md) | |
| [tmts2](https://aqms-swg.gitlab.io/aqms-docs/man-tmts2.html) | otl | [sequence](stored-packages/sequence.md), [irinfo](stored-packages/irinfo.md) | |
| [trig2db](https://aqms-swg.gitlab.io/aqms-docs/man-trig2db.html) | otl | [sequence](stored-packages/sequence.md), [truetime](stored-packages/truetime.md) | |
| [trimag](https://aqms-swg.gitlab.io/aqms-docs/man-trimag.html) | otl | [epref](stored-packages/epref.md), [magpref](stored-packages/magpref.md), [truetime](stored-packages/truetime.md) | |
| tss_qdm |	otl | ??? teleseism code - used only at SCSN? | |
| [wa](https://aqms-swg.gitlab.io/aqms-docs/man-wavearchiver.html) | otl | [truetime](stored-packages/truetime.md) | |
  

## Others  
  
| Program |	library/Method | Stored Procedures | Comments |
--------- | -------------- | ----------------- | ---------
| [DRP](https://gitlab.com/aqms-swg/aqms-dutyreview) | httpd |	[epref](stored-packages/epref.md), [truetime](stored-packages/truetime.md) | | 	
| ​[Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs/) | JDBC | [assocamp](stored-packages/assocamp.md), [epref](stored-packages/epref.md), [geo_region](stored-packages/geo_region.md), [magpref](stored-packages/magpref.md), [match](stored-packages/match.md), [pcs](stored-packages/pcs.md), [quarry](stored-packages/quarry.md), [truetime](stored-packages/truetime.md), [wave](stored-packages/wave.md) | |	
  

## Dependencies  

Please see section titled **Dependencies** at [Stored Procedures](stored-packages.md) to see stored procedure dependencies

## Database Users  
  
AQMS has a number of oracle users that are created that each have a different role. These can be found in the documentation at the [DBuser pages](docsrc/default-db-users.md). 