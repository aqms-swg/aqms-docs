# %Magnitudes {#aqms-magnitude-product}  
  
  
%Magnitude computation can be done as part of AQMS' automated or manual event detection. The values are calculated and stored in the AQMS database. 

## Magnitude Calculation from Real Time Data Acquisition
[trimag](https://aqms-swg.gitlab.io/aqms-docs/man-trimag.html) is the module for computing Me (Energy) and Ml (Richter) magnitudes. It uses the %Amplitude Data Area (ADA) for pulling me100 and ml100 values and computing the magnitudes.  
Moment magnitude calculations are done by TMTS  

The hypoinverse FMAG feature can be used to calculate duration magnitudes.
   
 ## Magnitude Calculation from Post Processing 
 In post processing magnitudes can be calculated through the Jiggle UI. There is also the hypomag application which can be run in an automated fashion on existing events in the AQMS database.
 Moment magnitude calculation can done by an analyst using the WebTMTS.

## Importing Magnitudes

### From QuakeML
Users can use [PDL2AQMS](https://gitlab.com/aqms-swg/aqms-pdl (pg port)) (https://gitlab.com/aqms-swg/aqms-pdl-ora for ora port)to insert a magnitude into the AQMS database.  It will do so under a new event id.

## Exporting and Distributing Magnitudes

Users retrieve origins in a variety of ways from the AQMS database. AQMS also distributes its origins to ComCat via PDL.

* qml: perl script - QuakeML
* FDSN webservice: QuakeML
* dbselect: a compiled client QuakeML, various delimited text file formats
* STP: a compiled compiled client - various delimited text file formats
* cattail: perl script - delimited text file



<br> 

## %Magnitude rules   
  
There are many ways to calculate an earthquake's magnitude. Each technique is valid over a finite range so several methods are needed to cover the entire range of earthquakes that are physically possible. In addition, some magnitudes can be calculated in real-time while others require more time, so the magnitude will necessarily “evolve” over time. Finally, each magnitude result can vary in quality and poorly constrained magnitudes should not be used.  
  
%Magnitude rules have been developed to select the preferred (best) magnitude as the various magnitude methods are executed after an event.  
  
The actual magnitude rules are defined and enforced using the *magpref* database package. If properly defined a human can always override the rules and select any magnitude.

#### NEIC %Magnitude Rules  

**Policy:**   
  
When reviewing earthquakes as part of NEIC response operations, NEIC will use the automatically determined value of Mwp from the Hydra system as the initial preferred magnitude for earthquakes with automatic Mwp of 5.8 or greater and at least six stations contributing to the Mwp estimate. A threshold of Mwp5.8 was set because of the likelihood of computing a reliable Mwp for this magnitude or higher. As we collect more observations and we can compute robust statistics on each magnitude, we will re-evaluate the threshold for reporting of Mwp.  
  
The analysts will update the magnitude, if the moment tensor (Mw) differs from Mwp by more that ±0.3 units. For events larger than about M7.5, the magnitude may be revised upon consultation with the Event Coordinator*. If updated, the event will be only resent to finger, unless the updated magnitudes differ from the last reported magnitude by at least 0.5 magnitude unit, in which case the revised event will be given a full release, if needed.  
  
It has been determined that, on average, Mwp is accurate to fairly low (M~5.0) magnitudes. The body-wave Mw, while introducing less scatter for moderate-sized earthquakes (M5.5-M7.0), takes longer to determine and starts to become problematic around M~7.0-7.4.  
  
Given the reliability of Mw and Mwp, mb should almost never be used as the initial preferred magnitude, and then, only if no other magnitude has been determined.    
  

<br>  

## MagPref  
  
- \subpage magpref

<br>
  
## Source code  
  
*  **trimag** : https://gitlab.com/aqms-swg/aqms-rapid-magnitude  
*  **magpref database stored procedure**  
    *  Oracle : https://gitlab.com/aqms-swg/aqms-db-ora/-/blob/master/storedprocedures/magpref_pkg.sql  
    *  Postgres: ???
*  **TMTS** : ??
*  **WebTMTS** : ??
