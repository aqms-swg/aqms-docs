# MagPref {#magpref}  
  
MagPref is the concept of determining the best (preferred) magnitude from among several magnitudes that may be calculated for an earthquake. The determination of the “best” magnitude is accomplished by applying well considered, quantitative rules that are applied uniformly to all events.   
  
These rules are defined and applied by using the MagPref package of stored procedures in the AQMS database. This database package provides a rational, centralized way to implement magnitude rules that can be used by all applications. Any application that calculates a magnitude can insert a [NetMag](http://www.ncedc.org/db/Documents/NewSchemas/PI/v1.5.6/PI.1.5.6/Model2/Tbl22.htm) row into the database. The decision to make it the *preferred magnitude* is handled by the [MagPref Package](???) not application logic. When the magpref rules engine is run ***all*** available magnitudes are compared and the best is set as preferred. 
  

The MagPref rules will:  
  
1.  Set the best magnitude of each type in the (*EventPrefMag* table).  
2.  Set the best magnitude of all types as the preferred magnitude for the event and origin.  
  
The “best” magnitude is the one with the highest priority value as defined in the *MagPrefPriority* table (see below).     



### What Applications Should Do  
  
Applications should **NEVER SET PREFMAG** directly.
  
The proper steps for committing and associating a magnitude are in the documentation of the [MagPref Package](???). 


  
### Defining MagPrefs Rules  
  
Magnitude preference rules are based on priority values that are assigned to “signatures” of magnitude attributes. These are are stored in a database table called MagPrefPriority. The signature is compared to the magnitude and event attributes in the NetMag table to assign a priority value to the magnitude. The magnitude with the highest value is set as preferred. If a field in the MagPrefPriority definition is left null, it is not used in the evaluation.  
  
Different magnitude priorities rules may be defined for different geographical regions. Named regions are specified using the [Gazetteer_Region](???) table.  
  
#### Magnitude Preference Criteria   
  
1.  **region** : A named region as defined in the *Gazetteer_Region.name* table. The event must be inside one or more regions defined in the Gazetteer_Region table having priority constraints defined in the MagPrefPriority table. Constraints should always be defined in the MagPrefPriority table for a region named 'DEFAULT'.  
2.  **magtype** : This is the subscript of the magnitude type and is constrained to the set allowed in the *NetMag.magtype* field ('a', 'b', 'e', 'l', 'l1', 'l2', 'lg', 'c', 's', 'w', 'z', 'B', 'un', 'd', 'h', 'n').  
3.  **auth** : This is the “authority” for the magnitude. Usually an FDSN-style network code like 'CI', 'US“, etc. This string is case sensitive.  
4.  **subsource** : This is the source of the data. E.g. 'RT1', 'Jiggle', 'mung', etc. This string is case sensitive.  
5.  **magalgo** : The *NetMag.magalgo* (magnitude algorithm) used to calculate the magnitude. E.g. 'trimag', 'SoCalMl', 'MungSoCalMl', etc. This string is case sensitive.  
6.  **minmag** : The *NetMag.magnitude* field of the mag must greater than or equal to this value for the magnitude to meet this test.  
7.  **maxmag** : The *NetMag.magnitude* field of the mag must less than or equal to this value for the magnitude to meet this test. *NetMag.magnitude* is constrained by the dbase to be between -10.0 and 10.0 inclusive.  
8.  **minreadings** : The *NetMag.nsta* field of the mag must be greater than or equal to this value for the magnitude to meet this test.  
9.  **maxuncertainty** : The *NetMag.uncertainty* field of the mag must be less than or equal to this value for the magnitude signature to match. Uncertainty is constrained in the dbase to >= 0.0.  
10.  **quality** : *NetMag.quality* value must be greater than or equal to this value for mag to be used. quality is constrained in the dbase to >= 0.0 and < = 1.0. (*The variance reduction of an Mw should be written into NetMag.Uncertainty but is not. As a work-around the plsql code looks it up in the Mec table.*)    
  

[NetMag table documentation](http://www.ncedc.org/db/Documents/NewSchemas/PI/v1.5.6/PI.1.5.6/Model2/Tbl22.htm)  
  
[Gazetteer documentation](???)  
  

#### Example magnitude priority definition  
  
```
#region             datetime_on           datetime_off    p type auth      src         algo minm maxm  msta   maxunc  qual
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00 1000    -   CI        -         HAND -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  920    w   US        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  910    w   CI     USER            -   -1   10     -     9999    .5
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  900    w   CI        -            - 3.45   10     -     9999    .5
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  820    s   CI        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  810    s   US        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  800    s NEIC        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  720    e   CI        -            - 5.95   10     2       .4     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  714    e   CI        -    trimag-SF 5.95   10     2       .4     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  712    e   CI        -    trimag-FF 5.95   10     2       .4     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  710    e   CI        -       trimag 5.95   10     2       .4     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  700    e   CI        -       sammag 5.95   10     2       .4     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  635   lr   CI   Jiggle      CISNml2 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  634    l   CI   Jiggle      CISNml2 -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  630   lr   CI  hypomag      CISNml2 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  625    l   CI  hypomag      CISNml2 -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  624   lr   CI        -    trimag-SF 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  623    l   CI        -    trimag-SF -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  622   lr   CI        -    trimag-SP 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  621    l   CI        -    trimag-SP -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  620   lr   CI        -    trimag-FF 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  619    l   CI        -    trimag-FF -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  618   lr   CI        -    trimag-FP 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  617    l   CI        -    trimag-FP -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  616   lr   CI        -       trimag 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  615    l   CI        -       trimag -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  610    l   CI        -       sammag -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  601   lr   CI        -            - 3.45    6     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  600    l   CI        -            - -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  599   lr   CI        -            - -1.0 7.95     2     9.99     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  550    w   CI        -            -   -1  3.5     -     9999    .5
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  500    c   CI        -            - -1.0   10     5     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  400    h   CI        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  306    d   CI        -    HypoinvMd -1.0   10    12      .25     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  304    d   CI        -    HypoinvMd -1.0   10     6      .45     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  302    d   CI        -    HypoinvMd -1.0   10     3      .75     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  300    d   CI        -            - -1.0   10     3     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  230    b   CI        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  220    b   US        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  210    b NEIC        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  200    b  RTP        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00   10    w   CI        -            - -1.0   10     -     9999     -
CI       1900-01-01 00:00:00.00 3000-01-01 00:00:00.00    3    e   CI        -            -  .95   10     3        -     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00 1000    -   CI        -         HAND -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  920    w   US        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  910    w   CI     USER            -   -1   10     -     9999    .5
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  900    w   CI        -            - 3.45   10     -     9999    .5
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  820    s   CI        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  810    s   US        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  800    s NEIC        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  720    e   CI        -            - 5.95   10     2       .4     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  714    e   CI        -    trimag-SF 5.95   10     2       .4     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  712    e   CI        -    trimag-FF 5.95   10     2       .4     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  710    e   CI        -       trimag 5.95   10     2       .4     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  700    e   CI        -       sammag 5.95   10     2       .4     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  635   lr   CI   Jiggle      CISNml2 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  634    l   CI   Jiggle      CISNml2 -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  630   lr   CI  hypomag      CISNml2 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  625    l   CI  hypomag      CISNml2 -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  624   lr   CI        -    trimag-SF 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  623    l   CI        -    trimag-SF -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  622   lr   CI        -    trimag-SP 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  621    l   CI        -    trimag-SP -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  620   lr   CI        -    trimag-FF 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  619    l   CI        -    trimag-FF -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  618   lr   CI        -    trimag-FP 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  617    l   CI        -    trimag-FP -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  616   lr   CI        -       trimag 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  615    l   CI        -       trimag -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  610    l   CI        -       sammag -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  601   lr   CI        -            - 3.45    6     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  600    l   CI        -            - -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  599   lr   CI        -            - -1.0 7.95     2     9.99     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  550    w   CI        -            -   -1  3.5     -     9999    .5
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  500    c   CI        -            - -1.0   10     5     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  400    h   CI        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  306    d   CI        -    HypoinvMd -1.0   10    12      .25     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  304    d   CI        -    HypoinvMd -1.0   10     6      .45     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  302    d   CI        -    HypoinvMd -1.0   10     3      .75     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  300    d   CI        -            - -1.0   10     3     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  230    b   CI        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  220    b   US        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  210    b NEIC        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00  200    b  RTP        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00   10    w   CI        -            - -1.0   10     -     9999     -
DEFAULT  1900-01-01 00:00:00.00 3000-01-01 00:00:00.00    3    e   CI        -            -  .95   10     3        -     -
DEFAULT  1899-12-31 20:53:20.00 3000-01-04 16:53:20.00    1    -    -        -            -    -    -     -        -     -
```
  
  

### Priority Values  
  
There is no absolute meaning to the priority values assigned to different magnitude types. What counts is the *relative* values. Higher values will be used over lower values. Any magnitude that meets none of the criteria defined in the table will be assigned a priority value of -1.  
  
In this example, an Ms from NEIC, regardless of its subsource or algorithm, would get a priority of 800. Setting the minmag/maxmag to -10/10 is equivalent to setting these fields null since any magnitude will always fall in this range and will never disqualify an magnitude.  
  
An Me from 'CI' with a magnitude of 6.2 but an uncertainty of 0.64 would get a priority of -1 because the uncertainty is higher than the maximum allowed; 0.4. Likewise, an Me with a magnitude of 5.8 would get a priority of -1 because it does not fall within the max/min magnitude range.  
  


### Deciding on the Preferred Mag  
  
When you call the function *MagPref.setPrefMagOfEventByPrefor(\<evid\>)* the priority of all the valid mags for the event will be determined and compared. The one with the highest value will be set as the event's preferred magnitude. If there is a tie, the latest in time will be used. An undefined mag, one that does not match any of the pre-defined criteria, will have a priority value of -1 and will not set as preferred.  
  
If an operator wants to allow use of otherwise undefined mags they can add a row to the MagPref table with all the criteria fields set permissively and string fields null. Therefore, any mag of any type from any source would satisfy those criteria. Such a row should have the lowest priority in the list.  
  
To force a particular magnitude to be preferred it may be tempting for an application to simple skip the rules and set the Event.prefmag directly. This would be a mistake. When the magpref rules engine is run ***all*** available magnitudes are compared and the best is set as preferred. Therefore, a subsequent application of the rules, say by some later process, may replace the improperly set prefmag, which is probably not the desired result.  
  
If you want to have some magnitude **always** be preferred over all others, then a “signature” should be created to accomplish this. For example, the MAGALGO = “HAND” in the example above.  
  


### Note on Mw Variance Reduction  
  
One quality measure of Mw is the Variance Reduction (VR) which is expressed as a percentage from 0-100%. This is a measure of the goodness of the fit of the synthetic waveforms to the observed waveforms. A larger value is better. The VR value is written to [Mec.pvr](http://www.ncedc.org/db/Documents/NewSchemas/PI/v1.5.6/PI.1.5.6/Model2/Tbl28.htm). To make it available to the MagPref logic this value is also written in decimal form to NetMag.Quality. A VR of 80% would be written as 0.8 to NetMag.Quality.  
  


### Listing magpref rules defined in the system  
  
The perl utility *magprefrules* can be used to print a summary listing of the magprefpriority table rules.  
  
https://aqms-swg.gitlab.io/aqms-docs/magprefrules.html  

  








<!--  
[Magnitude Priority](#mag-pref)
  *  [What Applications Should Do](#apps)  
  *  [Defining Priority Rules](#priority-rules)
  *  [Magnitude Preference Criteria](#mag-pref-criteria)
    *  [Example magnitude priority definition](#eg-mag-pref-def)


# Magnitude Priority <a name="mag-pref"></a>   
  
The determination of the "best" (preferred) magnitude from among several magnitudes calculated for an earthquake is accomplished by applying well considered, quantitative rules that are applied uniformly to all events. These rules are defined and applied by using the stored procedures of the [magpref](../../../aqms-db-ora/docsrc/stored-packages/magpref.md) package in the Oracle database. This package provides a rational, centralized way to implement magnitude rules that can be used by all applications. The stored procedures compare all available magnitudes associated with an event to defined metrics. The procedure will:   
  
*  Set the best magnitude of each type (magtype) in the (*EventPrefmag* table).
*  Set the best magnitude of all types as the preferred magnitude (prefmag) in the corresponding event and origin table rows.   
  
The "best" magnitude is the one whose data match the *MagPrefPriority* database table row having the highest priority value (see below).   
  
## What Applications Should Do <a name="apps"></a>  
  
Any application that calculates a magnitude can insert a ​http://www.ncedc.org/db/Documents/NewSchemas/PI/v1.5.6/PI.1.5.6/Model2/Tbl22.htm NetMag row into the database; however, the decision to make it the "preferred" magnitude (prefmag) should be handled by package stored procedures. Example step for committing and associating a magnitude are in the documentation of the [epref](../../../aqms-db-ora/docsrc/stored-packages/epref.md) and [magpref](../../../aqms-db-ora/docsrc/stored-packages/magpref.md) package stored procedures.   
  

## Defining Priority Rules <a name="priority-rules"></a>    
  
A magnitude preference rule assigns a "priority" value to a "filter signature" of magnitude attributes. Rule attributes are are stored in the *MagPrefPriority* database table. Rule attributes are compared to like attributes of an associated ​http://www.ncedc.org/db/Documents/NewSchemas/PI/v1.5.6/PI.1.5.6/Model2/Tbl22.htm NetMag table row to assign a priority value. The magnitude associated with the highest value is set as the preferred. If an attribute field (column) in the *MagPrefPriority* rule definition is left null, it is not used in the evaluation condition.  
    
Different magnitude priorities rules may be defined for different geographical regions. Named regions are specified in the Gazetteer_Region database table which is accessed using the [geo_region](../../../aqms-db-ora/docsrc/stored-packages/geo_region.md) stored package procedures.   
  

## Magnitude Preference Criteria <a name="mag-pref-criteria"></a>    
  
1.  **region** -- A named region as defined in the *Gazetteer_Region.name* table column. The event must be inside one or more regions defined in this table having priority constraints defined in the *MagPrefPriority* table. Constraints should always be defined in the *MagPrefPriority* table for a region named 'DEFAULT'.  
  
2.  **magtype** -- This is the subscript of the magnitude type and is constrained to the values allowed for the *NetMag.magtype* field:('a', 'b', 'e', 'l', 'l1', 'l2', 'lg', 'c', 's', 'w', 'z', 'B', 'un', 'd', 'h', 'n').  
  
3.  **auth** -- This is the "authority" for the magnitude. Usually an FDSN-style network code like 'CI', 'US", etc. This string is case sensitive.  
  
4.  **subsource** -- This is the source of the data. E.g. 'RT1', 'Jiggle', 'mung', etc. This string is case sensitive.  
  
5.  **magalgo** -- The *NetMag.magalgo* (magnitude algorithm) used to calculate the magnitude. E.g. 'trimag', 'CISNml2', '!RichterMl2', etc. This string is case sensitive.  
  
6.  **minmag** -- The *NetMag.magnitude* field of the mag must greater than or equal to this value for the magnitude to meet this test.  
  
7.  **maxmag** -- The *NetMag.magnitude* field of the mag must less than or equal to this value for the magnitude to meet this test. *NetMag.magnitude* is constrained by the database to be between -10.0 and 10.0 inclusive.  
  
8.  **minreadings** -- The *NetMag.nsta* field of the mag must be greater than or equal to this value for the magnitude to meet this test.  
  
9.  **maxuncertainty** -- The *NetMag.uncertainty* field of the mag must be less than or equal to this value for the magnitude signature to match. Uncertainty is constrained in the dbase to >= 0.0.  
  
10.  **quality** -- The *NetMag.quality* value must be greater than or equal to this value for mag to be used. Quality is constrained in the dbase to >= 0.0 and < = 1.0. (The percent variance reduction of an Mw is mapped to *NetMag.uncertainty* values 0.0-1.0).   
  

### Example magnitude priority definition: <a name="eg-mag-pref-def"></a>     
  
    ```
    REGION_NAME     PRIORITY     MAGTYPE     AUTH     SUBSOURCE     MAGALGO     MINMAG     MAXMAG     MINREADINGS     MAXUNCERTAINTY     QUALITY     DATETIME_ON          DATETIME_OFF         LDDATE                
    --------------  -----------  ----------  -------  ------------  ----------  ---------  ---------  --------------  -----------------  ----------  -------------------  -------------------  --------------------- 
    CI              1000         (null)      CI       (null)        HAND        -1         10         0               9999               (null)      1900/01/01 00:00:00  3000/01/01 00:00:00  8/12/2008 10:58:19 PM 
    CI              910          w           CI       (null)        (null)      5          10         0               9999               0.6         1900/01/01 00:00:00  3000/01/01 00:00:00  8/12/2008 10:58:19 PM 
    CI              900          w           US       (null)        (null)      -1         10         0               9999               (null)      1900/01/01 00:00:00  3000/01/01 00:00:00  8/12/2008 10:58:19 PM 
    CI              820          s           CI       (null)        (null)      -1         10         0               9999               (null)      1900/01/01 00:00:00  3000/01/01 00:00:00  8/12/2008 10:58:19 PM 
    CI              810          s           US       (null)        (null)      -1         10         0               9999               (null)      1900/01/01 00:00:00  3000/01/01 00:00:00  8/12/2008 10:58:19 PM 
    CI              800          s           NEIC     (null)        (null)      -1         10         0               9999               (null)      1900/01/01 00:00:00  3000/01/01 00:00:00  8/12/2008 10:58:19 PM 
    ```
-->