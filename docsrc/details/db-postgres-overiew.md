# Postgres database overview {#db-postgres-overview}  
  
Everything needed to setup an AQMS Postgres database.  
  
- \subpage db-pg-create