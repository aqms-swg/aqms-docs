One-page transition plan AQMS source code from SVN to GIT {#detail-git-plan}
=========================================================
Revisions:
* Version 1, draft by Renate Hartog, circulated by email to AQMS SWG on
September 25, 2018
* Version 2, draft by Renate Hartog, circulated by email to AQMS SWG on
September 27, 2018→ alternative break up of repositories on second page.
* Version 3, draft by Renate Hartog, circulated by email AQMS SWG on
October 4, 2018 → AQMS SWG in favor of smaller repositories. GitLab or
Github, TBD.
* Version 4, May 1 2019, draft by Renate Hartog, circulated by email to AQMS
SWG Sub-group on May 1, 2019 →removed references to github, we decided on
gitlab.  To have in hand for conference call May 15
* Version 5, May 30, 2019, AQMS SWG Sub-group decided on more repo-names,
shown underlined and bold.
* Version 6, November 16, 2019, clean up to be able to include this in the
gitlab aqms-docs repo.

Goals:
* Make AQMS available to anyone via GitLab →aqms.swg group (aqms is the user
that owns the group, groups are better for collaborating: 
https://gitlab.com/aqms.swg )
* Improve AQMS by following open source project style development, i.e. use gitlab issue, pull requests, versioning, etc.
* Clear build and installation  instructions for different platforms
(linux+PostgreSQL, linux+Oracle, solaris+Oracle, maybe even Windows in the
future if anyone cares to make that happen).
* Documentation of AQMS and how to configure it (from high-level overview to
details for each component).

Transition:
* Do not transfer the SVN repository, make a clean break instead. --all
agreed.
* Break the single large repository up into smaller git repositories
(probably not a comprehensive list and subject to change):
* Core AQMS: Open question: can we create sub-projects in GitLab? → decided
to go with submodules to enable easy builds and tests.

Projects:
* **aqms-docs** : source files for overview documentation only (Explanations, Tutorials), links to the other repos for details. E.g. how-to-configure-rt-aqms.md vs how-to-configure-pws.md (the latter would go into aqms-proxy-waveserver/docsrc).
* **aqms-db-pg** : everything needed to create an AQMS schema/stored procs in PostgreSQL
* **aqms-db-utilities**: admin scripts for database status, etc. pg,ora,either
* **aqms-db-ora**: everything needed to create an AQMS schema/stored procs in Oracle plus database admin scripts
* **aqms-lib**: libsrc C++ source code for shared libraries.
* **aqms-thirdparty**: third party libraries (not all have a general use license)
* **aqms-gcda**: makeada, deleteada, makewda, deletewda, scang, accmon, adadup (generic continuous data area)
* **aqms-alarming**: alarmdec, alarmact, alarmdist, alarmevent, sendcancel
* **aqms-amplitude-generator**: ampgen
* **aqms-conlog**: console log
* **aqms-duty-review**: drp
* **aqms-event-coordinator**: ec+ hyps2ps
* **aqms-proxy-waveserver**: pws
* **aqms-rapid-amplitude-data**: rad2
* **aqms-solution-server**: (solserver)
* **aqms-subnet-triggers**: tc + trig2db + trig2ps + ec2 + ntrgc2
* **aqms-rapid-magnitude**: trimag
* **aqms-wavearchiver**: wa
* **aqms-cms** : source code from ISTI that goes into QWServer.jar
* **aqms-cms-libs** : AQMS CMS library code.  
* **aqms-jiggle** : Java source code, from Allan Walter (i.e. Jiggle, RCG, Gmp2Db, SnapTriggerGif etc), can be broken up later.
* **aqms-utilities** : perl-utils and startup scripts etc. forks might diverge as different networks have different preferences. Sub-directories per language?
* **aqms-tools**: tools that are compiled executable code, rather than interpreted/compiled-in-place.
* **aqms-dbselect** : query tool to retrieve catalogs from the database in plain text. In preprocessor languages ProC and ecpg, needs to be pre-compiled to C.
* **aqms-latency** : EW client to calculate waveform latency values.  
* **aqms-comserv2aqms** : ComServ clients that read data from a comserv memory area and write to various locations.  
* **aqms-ewhypo2aqms** : Module to move EarlyBird to AQMS from EW.  
* **aqms-multicast** : Modules for ingesting data from a multicast address.  
* **aqms-ewmag2cism** : Module to insert localmag results into AQMS database.  
* **aqms-waveformclient** : Client program used to query waveform data in miniSEED.  
* **aqms-pdl** : Tools/libs for inserting PDL messages into AQMS.  
* **aqms-pdl-ora** : Oracle port of aqms-pdl.  

Add-ons:
* aqms-FP : fpfit wrapper
* aqms-HASH: HASH wrapper
* aqms-TMTS2 : tmts2
* docker-playground : Various docker files including postgres and oracle.  


Guidelines:
1. Only transfer code that is still in use, obsolete code will remain
accessible in (eventually) read-only SVN.
2. Create the new git repositories one by one in “private” ; release (i.e.
make public) when everything in it is versioned, has tests, and is documented.
3. Once a piece of the SVN repository is available on github/gitlab, it should
be understood that the equivalent  on SVN is now static.
4. To avoid living with two repository systems for years, there needs to be an
organized push to make this transition by a dedicated group of people,
ideally a representative per RSN and Paul Friberg. Volunteers: Paul Friberg
(ISTI), Renate Hartog (PNSN), Victor Kress (PNSN) Aparna Bhaskaran (SCSN),
Andrew Good (SCSN), Clara Yoon (USGS), Ben Baker (Utah), Stephane Zuzlewski
(UCB), Paul Milligan (UCB).
5. Contributor guidelines need to be written and agreed upon for each of the
new git repos (or one to be used for all?)

Other:
* Agreed to make aqms.swg gitlab group public before Hartog et al., 2019 SRL
paper about AQMS is published (early release is planned for Nov 20, 2019),
even though our work is far from complete, breaking rule #2 above..
* We’re using Doxygen for documentation.

\htmlonly
<a href="detail-overview.html">Next</a> or back to
<a href="index.html">Starting Page</a>.
\endhtmlonly

