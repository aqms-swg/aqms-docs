# Glossary {#aqms-glossary}  
  
* [A](#a)
* [C](#c)
* [D](#d)
* [E](#e)  
* [F](#f)  
* [G](#g)  
* [H](#h)  
* [I](#i)
* [J](#j)
* [N](#n)  
* [P](#p)  
* [Q](#q)
* [R](#r)  
* [S](#s)  
* [T](#t)
* [W](#w)
  
  
Glossary of AQMS terms.  
  
**A** <a name="a"></a>  
  
*  **ADA** - %Amplitude Data Area
*  **[ANSS](https://www.usgs.gov/programs/earthquake-hazards/anss-advanced-national-seismic-system)** - Advanced National Seismic System
*  **AQMS** - ANSS Quake Monitoring System
  

**C** <a name="c"></a>   

*  **CISN** - California Integrated Seismic Network   
   
   
**D** <a name="d"></a> 
  
*  **DRP** - Duty Review Page  
  

**E**  
  
*  **EEW** - Earthquake Early Warning  
*  **[EW](https://www.isti.com/products-offerings/earthworm)** - EarthWorm  

     
**F** <a name="f"></a>   

*  **[FDSN](https://www.fdsn.org/)** - International Federation of Digital Seismograph Networks.  
*  **FMAR** - First Motion/Amplitude Ratio (FMAR) focal mechanism. Also, known as HASH.    
*  **FP** - Fault Plane focal mechanism. Also, known as FPFIT.   
*  **FPFIT** - Fault Plane focal mechanism. Also, known as FP.

**G** <a name="g"></a>   
  
*  **[GMT](https://www.generic-mapping-tools.org/)** - Generic Mapping Tools  


**H** <a name="h"></a>  
  
*  **HASH** - First Motion/Amplitude Ratio (FMAR) focal mechanism. Also, known as HASH.  
  

**I** <a name="i"></i>  
  
*  **[ISTI](https://www.isti.com/)** - Instrumental Software Technologies, Inc.  
  

**J** <a name="j"></a>  
  
*  **[Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs)** -A Graphical User Interface (GUI) software application used to analyze earthquake waveform data and calculate accurate earthquake (event) parameters.  
  

**L** <a name="l"></a>  
  
*  **LATENCY_RING** - Shared memory area storing latency values calculated from waveform data.  


**M** <a name="m"></a>
  
*  **mcast** - Short for multicast.  
  

**N** <a name="n"></a>  

*  **[NCSN](https://www.ncedc.org)** - Northern California Seismic Network.    
*  **[NEIC](https://www.usgs.gov/programs/earthquake-hazards/national-earthquake-information-center-neic)** - National Earthquake Information Center  
    

**P** <a name="p"></a> 
  
*  **[PCS](https://aqms-swg.gitlab.io/aqms-docs/pcs.html)** - Process Control System
*  **PNSN** - Pacific Northwest Seismic Network    
*  **PP** - Post Processing  
  

**Q** <a name="q"></a>  
  
*  **[QuakeML](https://quake.ethz.ch/quakeml)** - A XML data format to exchange event parametric data.   
    

**R** <a name="r"></a> 
  
*  **RAD** - Reduced %Amplitude Data. Also, known as Rapid Amplitude Data  
*  **RT** - Real-time  
*  **RWP** - Rapid Wave Pool. A memory area holding waveform data with a depth of a few days.  
  

**S** <a name="s"></a>  
  
*  **[SCSN](https://www.scsn.org)** - Southern California Seismic Network   
*  **[SCEDC](https://scedc.caltech.edu)** - Sourther California Earthquake Data Center. Data archival arm of SCSN.  
*  **SNCL** - Station code, Network code, Channel code, Location code. An abbreviation to refer to a channel.  


**T** <a name="t"></a> 
  
*  **[TMTS](detail-tmts2.html)** - Trinet Moment Tensor System.
*  **TPP** - Trinet Post Process. It is a defunt acronym, PP (Post Processing) used now to refer to any tasks in post processing.  
*  **TRP** - Trigger Review Page    
  

**W** <a name="w"></a> 
  
*  **wa** - Waveform Archiver.  
*  **WDA** - Waveform Data Area.  
*  **WAVE_RING** - Shared memory area storing waveform data.  