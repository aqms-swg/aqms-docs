# %Origin {#aqms-origin-product}  
  
    
%Hypocenter (origin) computation can be done as part of AQMS' automated or manual event detection, or refined from an existing origin. The values are calculated and stored in the AQMS database. The most common program used by AQMS networks to determine origins is [hypoinverse](https://www.usgs.gov/node/279394). 

## Hypocenter Calculation from Real Time Data Acquisition

Currently all networks using AQMS use earthworm data flows to present picks to hypoinverse. The origin calculated by hypoinverse is then sent to the [ec] (https://gitlab.com/aqms-swg/aqms-event-coordinator) module to insert into the AQMS database.

earthworm sausage --> hypoinverse --> hyps2ps --> ec (ec writes to AQMS database) (need to update with agood entry?)

Subnet triggers not associated by the ec process also insert an origin into the AQMS database, but it will have a latitude and longitude of 0,0.  For more information on AQMS subnet triggers, see https://gitlab.com/aqms-swg/aqms-subnet-triggers

earthworm carlsubtrig (subnet trigger) --> trig2db (writes triggers to AQMS database) --> tc (writes event row etc).
    
## Hypocenter Calculation from Post Processing

In the AQMS postprocessing environment, access to hypoinverse is available a service called [solution server ("solserver")](https://gitlab.com/aqms-swg/aqms-solution-server). Solserver can then return back the origin in a hypoinverse format.
Analysts can make picks in [Jiggle](https://gitlab.com/aqms-swg/aqms-jiggle), which will submit them to the solserver and then insert the returned origin into the database.
An automated process such as hypomag (insert link) similarly can submit refined picks of an event to the solserver and insert the returned origin into the database.

## Importing Origins

### From QuakeML
Users can use [PDL2AQMS](https://gitlab.com/aqms-swg/aqms-pdl (pg port)) (https://gitlab.com/aqms-swg/aqms-pdl-ora for ora port)to insert an origin into the AQMS database.  It will do so under a new event id.

### From Hypoinverse
Users can use the [Jiggle.jar](https://gitlab.com/aqms-swg/aqms-jiggle) file to import hypoinverse files for an existing event id.

### From EarlyBird (Earlybird2AQMS)

EarlyBird is an Earthworm derivative tool used by the Tsunami Warning Centers to produce rapid solutions of local and teleseismic earthquakes. It provide a location solution repeatedly as more P phases are added so the solution evolves with time. The AQMS module ewhypo2aqms listens on an EW ring for origin solutions from EarlyBird and writes them progressively to an AQMS event at first and then updates the origin with new versions of the solution. The EarlyBird event TYPE_HYPOTWC message contains numerous magnitudes and these are also written to the netmag table and updated as the solution progresses. Unfortunately, the solution message does not contain the phase arrival information and that comes as a file on the EB system. That can be sent to ewhypo2aqms using the file2ew module and ewhypo2aqms can associate the phases with the solution as those files are injected as TYPE_TWC_PHASEFILE message type in Earthworm.

## Exporting and Distributing Origins

Users retrieve origins in a variety of ways from the AQMS database. AQMS also distributes its origins to ComCat via PDL.

* qml: perl script - QuakeML
* FDSN webservice: QuakeML
* dbselect: a compiled client QuakeML, various delimited text file formats
* STP: a compiled compiled client - various delimited text file formats
* cattail: perl script - delimited text file

  
### Data formats   
    
* **Hypoinverse** : https://pubs.usgs.gov/of/2002/0171/pdf/of02-171.pdf
* **QuakeML** : https://quake.ethz.ch/quakeml
   

## Source code  

* **AQMS2PDL** : https://gitlab.com/aqms-swg/aqms-pdl (pg port)  https://gitlab.com/aqms-swg/aqms-pdl-ora (ora port)
* **cattail** : https://gitlab.com/aqms-swg/aqms-utilities-ora/-/blob/master/perl_utils/bin/cattail.pl  
* **dbselect** : https://gitlab.com/aqms-swg/aqms-dbselect  
* **ec** : https://gitlab.com/aqms-swg/aqms-event-coordinator 
* **FDSN webservice** : (need link)
* **hypoinverse** : https://www.usgs.gov/node/279394 
* **jiggle**   : https://gitlab.com/aqms-swg/aqms-jiggle
* **solution server**  : https://gitlab.com/aqms-swg/aqms-solution-server
* **qml** : https://gitlab.com/aqms-swg/aqms-utilities-ora/-/tree/master/qml  
* **STP** : (need link)


