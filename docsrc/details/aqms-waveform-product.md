# Waveforms {#aqms-waveform-product}  
  
AQMS produces waveforms associated with a detected event in [miniSEED](???) format. Each event will have several waveforms associated with it, one for each channel (SNCL) that contributed to the event. %Event waveform generation is an automated, post procsessing, activity achived via the \subpage wavearchiver.   

The archived event waveforms are used by [Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs), [DRP](drp.html) and [TRP](trp.html).  

%Event waveforms can be re-archived using the [redo waveform](man-redo_wf.html) perl utility.       
    

## Source code  
  
*  **wavearchiver** : https://gitlab.com/aqms-swg/aqms-wavearchiver   
*  **%Database stored procedures - (Oracle)**   
    *  **wavefile** : https://gitlab.com/aqms-swg/aqms-db-ora/-/blob/master/storedprocedures/wave_pkg.sql
    *  **wave** : https://gitlab.com/aqms-swg/aqms-db-ora/-/blob/master/storedprocedures/wavefile_pkg.sql
*  **redo waveforms** : https://gitlab.com/aqms-swg/aqms-utilities-ora/-/tree/master/postprocessing_tasks/redo_wfs  
  

  
