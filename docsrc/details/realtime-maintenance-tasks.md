# Real Time Maintenance Tasks {#real-time-maintenance}    
  
  
  *  [Log file cleanup (EW and RT)](#log-cleanup)  
  *  [RT DB periodic clean up](#rtdb-periodic-cleanup)  
  *  [RT DB tablespace truncation](#rtdb-tblspc-trunc)  
  *  [Role switching](#role-switch)  
  *  [RT Health monitoring](#rt-health-mon)  
  *  [DB replication restart](#rep-restart)  
    *  [Further replication restart details](#rep-restart-det)  
  *  [Configuration push](#config-push)  
  *  [EW configuration files created from DB](#ew-config)  
    *  [WDA ADA and Channel Lists](#chan-lists)  
  *  [Leap Seconds Table in file system](#leap-seconds)  
  *  [Adding a station/channel to the system](#add-chan)  
  
  

  
## Log file cleanup (EW and RT) <a name="log-cleanup"></a>   
  
For this task, a simple cron job is configured to run the UNIX find command to remove logs older than a certain age. Here is how we set it up for most instances:   
  
```
    0 1 * * * /usr/bin/find /app/rtem/rt/logs -name '*.log' -mtime +30 -exec /usr/bin/rm {} \;
    30 1 * * * /usr/bin/find /app/rtem/ew/run/logs -name '*.log' -mtime +30 -exec /usr/bin/rm {} \;
```
  

## RT DB periodic clean up <a name="rtdb-periodic-cleanup"></a>  
  
The Oracle database creates logs that constantly fill up the /oralog/rtdb directory and need removal (which is automatically done by the rman backup system). However, since these rtdb's (rtdb and rtdb2) are backed up to the archdb via replication, there is no need to save these logs and they can safely be removed using cron job run from the oracle users account:     
  
```
    0 1 * * * /usr/bin/find /oralog/rtdb -name '*.dbf' -mtime +2 -exec /usr/bin/rm {} \;
```
  
## RT DB tablespace truncation <a name="rtdb-tblspc-trunc"></a>     
  
This section describes how to free up Oracle tablespace when it starts to fill on an RT db. The filling of the tablespace can be detected by the pctused script or from ???SeisNetWatch???. To free up space in the rtdb, first make sure that you are on the SHADOW RT host and not the primary. Then stop all processes that are writing to the database. To do this you must shutdown the rtem processes (??? what/where is this rtemctl.sh stop???). Use UNIX ps to confirm that all ???rtem procs??? are stopped. Once you are sure all of rtem is stopped, then you can do the following steps as the user rtem (you will need the password for the trinetdb user):   
  
```
    cd ~rtem/db
    sqlplus trinetdb@rtdb < /app/rtem/db/truncate_tabs.sql
```
  

## Role switching <a name="role-switch"></a>    
    
The purpose of role switching is to change which RT host is PRIMARY and which is designated SHADOW. The PRIMARY host is the one configured to alarm based on automated processes (i.e., [alarmdist](https://aqms-swg.gitlab.io/aqms-docs/man-alarmdist.html) will fire alarms only on the primary host). Further, the PRIMARY host is used to define which events are authoritative for both the subnet triggers and the hypo2000 (hypoinverse) event solutions.  
  
There are two simple bash shell scripts to make a host primary or shadow and they are make_dbase_primary and make_dbase_shadow. The executables for each can be found in the distribution under /app/rtem/utils/takeover and should be in the PATH of the rtem user. Note that each of these take an argument which is the rtdb to be switched. A user can change the roles of both rtdb systems from either host. If an attempt is made to change a role that is already set, then the script will notify the user that the role is already set.  
  
In the example below the host running rtdb2 is primary and rtdb is shadow:  
  
```
    make_dbase_primary rtdb
    make_dbase_shadow rtdb2
```

would effectively make the rtdb host primary now and the rtdb2 shadow. The only modules effected by this are alarmdist, ec, and tc.   
  

## RT Health monitoring <a name="rt-health-mon"></a> 
  
The RT systems monitor their own health via shell scripts that end-users should provide (e.g., monitoring that some critical number of channels are alive and feeding data are some common tests). If the scripts are successful, then the peer_system_health table of the peer rtdb status field should be marked as UP and if there are problems detected, it should write DOWN into the status field. This is detected by alarmdist and used to decide if a shadow box should automatically start sending alarms.  
  

## DB replication restart <a name="rep-restart"></a>  

If post processing reboots and takes too long to start or the network connection from an RT db to the archdb fails, then replication will not be running after about 16 tries. Replication copies key tables from the *rtdb* to *archdb* every 4 seconds and this is a critical component of the system. The ???SeisNetWatch??? display will show if the rtdb or rtdb2 replication is not functioning. One way to quickly detect that replication is not functioning is to note that no new events are appearing in the archdb but they are being created on the rtdbs. If it is not running, then the restart_replication bash script should be executed. This application can be run at any time and it will report if replication is already running and ONLY if it is not running will it attempt to restart it. The **restart_replication** script can be found in the ~rtem/utils/replication directory. It can be safely run at any time.  
  

### Further replication restart details <a name="rep-restart-det"></a>    
  
The repadmin user should be used to see if replication is failing by issuing the SQL below:  
  
```
    select job from dba_jobs where broken='Y' and what like '%push%';
```
  
And if no rows are selected then the replication process is running fine. Otherwise a job number will be returned and that should be used in the SQL query below to restart replication:   
  
```
    exec dbms_job.run(22);
```
  
after which you will see the response "PL/SQL procedure successfully completed." if replication was successfully restarted. Alternatively to detect failure you could look in the /app/oracle/admin/rtdb/bdump/alert_rtdb.log and at the end of the log you will see failures of this nature:  
  
```
    ORA-12012: error on auto execute of job 22
    ORA-23324: error ORA-12541: TNS:no listener, while creating deferror entry at "ARCHDB.WORLD" with error -12541
```
  

## Configuration push <a name="config-push"></a>    

The active stations and channels list used by the RT boxes comes from the archdb as a snapshot view (Oracle speak for a copy of the tables from one db system to another). The tables involved are station_data, channel_data, stacorrections, and simple_response. When a station list is updated on the archdb, it ultimately needs to be passed to a RT box and this push of station/channel data to the RT box is known as a "configuration push". A configuration push is typically only done on the shadow host so that it can be tested for correctness. Once the shadow configuration has been tested and the new stations/channels are observed (or if some were deleted, that they are gone), then the config push can happen on the primary box, but typically a role-switch is performed before this upgrade of the primary.

Running the **station_refresh.pl** script to update the view of the stations tables in the RT db from archdb (archival DB on post processing host). This is located in the config_channel/scripts or the db/ directory of the rtem user.  
  

## EW configuration files created from DB <a name="ew-config"></a>  
  
*Note: At the PNSN we have adopted the Caltech strategy described below and would recommend it. We use subversion rather than CVS to keep track of different config versions. In fact, we have a svn repository for a whole Real-Time box (/app/rtem) and a whole Post-Proc box (/app/postproc).*  
    
Along with configuration pushes (which update only the database tables) a number of scripts can be written to generate the Earthworm configuration files to be used. This is done at Caltech and the scripts are provided in the /app/rtem/chan-config/scripts directory as an example. The listing of the chan-configs directory shows the following:   
  
```
    {sunfire:~} ls -F chan-configs
    CVS/                           channels_cs2ew_fj.cfg          channels_rad_ae.cfg            pick_ew.sta
    README                         channels_cs2ew_ko.cfg          channels_rad_fj.cfg            rwp.cfg
    ada.cfg                        channels_cs2ew_ps.cfg          channels_rad_ko.cfg            scripts/
    all.stations                   channels_cs2ew_tz.cfg          channels_rad_ps.cfg            sql_chanloader/
    caltech.d                      channels_lg.cfg                channels_rad_tz.cfg            subnets/
    carlstatrig.sta                channels_mcast2ew_ae.cfg       channels_rwp_al.cfg            tridefs.cfg
    carlsubtrig.d                  channels_mcast2ew_analog.cfg   channels_rwp_analog.cfg        wda_ae.cfg
    carlsubtrig.sta                channels_mcast2ew_fj.cfg       channels_rwp_mz.cfg            wda_fj.cfg
    chan-configs/                  channels_mcast2ew_ko.cfg       channels_rws.cfg               wda_ko.cfg
    channels_cs2ew_ae.cfg          channels_mcast2ew_ps.cfg       email/                         wda_ps.cfg
    channels_cs2ew_analog.cfg      channels_mcast2ew_tz.cfg       generate-configs.sh*           wda_tz.cfg
    {sunfire:~} 
```

The way this is used at Caltech is that all of the configuration files are built from the database using a series of shell and Perl scripts. Furthermore, all of the configurations are stored in a ???is it still CVS or is it git???CVS repository so that configuration changes from time to time are tracked (note the CVS/ directory in the listing). Of importance in this listing are the sql_chanloader/ directory where SQL that governs how some RT programs know what channels to use and also so that the perl scripts that create configs know which stations and channels to include for a given program. The scripts/ directory is listed below and consists of Perl DBD usage to query the database view created by the sql_chanloader:    

```
    {sunfire:~} cd chan-configs/
    {sunfire:chan-configs} ls -F scripts/
    CVS/                        create_carlsubtrig.pl*      create_rad.pl*              create_wda.pl*              station_refresh.pl*
    cg_hinv_parser.py           create_carlsubtrig_sta.pl*  create_rwp.pl*              hypostalist_short*          test.pl*
    clean_cg_net.sql            create_cs2ew.pl*            create_rwp_base.pl*         run_all*
    coso.hinv.sta               create_lg.pl*               create_rws.pl*              static_carlsubtrig.cfg*
    create_ada.pl*              create_mcast2ew.pl*         create_station_list.pl*     static_pick_ew_sta.cfg*
    create_carlstatrig_sta.pl*  create_pick_ew_sta.pl*      create_tridefs.pl*          station.mcast.data
    {sunfire:chan-configs} 
```

This is just an example of how one could use the Oracle database to maintain channel lists that in turn are used to create the configurations used by Earthworm. We think that any users with large networks could use a version control system like git if you have many config changes taking place to your network. By using a version control system, you can more quickly back track to a working configuration if problems arise on the shadow system.  

  
## WDA ADA and Channel Lists <a name="chan-lists"></a>  
  
The WDA and ADA "Data Areas" are used to generate the rapid magnitude and shakemap values (refer to the [gcda_utils](https://aqms-swg.gitlab.io/aqms-docs/man-gcda.html), ???ew2wda???, [rad2](https://aqms-swg.gitlab.io/aqms-docs/man-rad2.html), and [trimag](https://aqms-swg.gitlab.io/aqms-docs/man-trimag.html) manual pages). The configuration of these Data Areas are via shared memory (much like Earthworm) and the Oracle database. The Oracle database is used for a list of channels that are assigned to a KEY value that references the shared memory data area. At Caltech all of this configuration is handled with the configuration push scripts, however it can be done by hand as well. This section details the steps needed in gory detail, but note that ALL OF THE RT side can be simply done by doing a station_refresh and then stopping the RT system and restarting it (rtemctl.sh stop; sleep 60; rtemctl.sh start):  
  
1.  On the post processing host archdb add in the channels you want to add and also update the stacorrections table for trimag (see the file ???db/magnitude_corrections.sql???).  
  
2.  All of the next steps happen on the RT host, run the stations_refresh.pl script with the argument of the database to have the new stations and channels loaded locally.  
  
3.  Next stop all RT programs and EW modules using the WDA and ADA (ew2wda, rad2, trimag, and ampgen).  
  
4.  cd into the wda directory and wipe and recreate the WDA (note you may need to edit the wda_config.sql script if the station/channels are different from other stations in your network). To delete and rebuild the WDA, simply run the start_wda script.   
  
5.  Restart the ew2wda module in EW to start populating the WDA.  
  
6.  Confirm that you have data in the WDA (use the command line "scang WDA_KEY") and that it is not too latent. Also confirm your new channels are there.  
  
7.  Then delete the wda (cd ~rtem/rad; deleteada ADA_KEY)  
  
8.  Restart rad2 (radctl.sh start)  
  
9.  Confirm that you have data in the ADA (use the command line "scang ADA_KEY" or the accmon program to look at a particular SCNL).  
  
10.  Restart trimag   
  

## Leap Seconds Table in file system <a name="leap-seconds"></a>  
  
The RT software relies on an updated leapseconds file. This defaults to being in the /usr/local/lib/leapseconds. The last leap second was added at the end of 2016 on Dec 31 at 23:59:59 making 27 leapseconds since this was introduced. This file must be kept up to date an the database leap_seconds table must be too. You will hear from Brian Pardini at NCEDC if there is a new one being introduced. Subscribe to the AQMS-ANSS google group (anss-aqms@googlegroups.com) to get the update. The database leap_second table update procedure is described in <a href="db-maintenance.html">here</a> .   
  

## Adding a station/channel to the system <a name="add-chan"></a>  
  
This section details the steps needed to add a station to the system. We recommend you do this on the SHADOW system first, and then compare the results with the production (primary) RT server.  
  
1.  [Get the station metadata loaded into the archdb]??? (station_data, channel_data, simple_response, stacorrections, etc)...  
  
2.  Push the updated metadata to the shadow RT system's database (cd db; ./station_refresh.pl *rtdbN*).  
  
3.  Stop the RT shadow system (rtemctl.sh stop)  
  
4.  Run your Earthworm configuration generation scripts and review that changes are as you expect.  
  
5.  If you are using the trimag and/or ampgen programs, then edit the /app/rtem/wda/wda_config.sql and make sure the SQL will capture the new station's channels of interest for strong-motion and broadband instruments only.  
  
6.  Restart the system (rtemctl.sh start)   
  

















