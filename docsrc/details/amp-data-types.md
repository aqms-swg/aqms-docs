# Amplitude Data types {#amp-data-types}
  
The following amplitude data types are defined within the CISN system.   
  
| **Code** | **Description** |
---------- | -----------------
| PGD | Peak Ground Displacement |
| PGV | Peak Ground Velocity |
| PGA |	Peak Ground Accelaration |
| SP.3 | Spectral Peak Amplitude at 0.3 second period |
| SP1.0 | Spectral Peak Amplitude at 1.0 second period |
| SP3.0 | Spectral Peak Amplitude at 3.0 second period |
| WA | Wood-Anderson, photographic |
| WAC | Wood-Anderson, corrected |
| WAU | Wood-Anderson, uncorrected |
| WAS |	Wood-Anderson, synthetic |
| WASF | Wood-Anderson, synthetic, filtered |
| ML100 | Peak Wood-Anderson amp in 5 sec window (for ML) |
| ME100 | Integral of velocity squared, 5 sec window (for ME, energy magnitude) |
| EGY | Energy (not used) |
| HEL | Hand measured from helicorder or photo record |
  

## ME100  
  
Note from Hiroo Kanimori   
  
```
It should be (velocity)**2*time.

Then the question is what unit is used for velocity.  As I remember, we were using cm/s.  
So, the unit would be  cm^2 sec, but we need to check the unit of velocity.

One comment:  As is written in BSSA, it is actually an integral of velocity squared, and 
not "energy".  It is an essential part of the expression used to compute the radiated 
energy in seismology.

As long as it is understood that way, that is fine.
```
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/iovs.gif">

Reference: Continuous Monitoring of Ground-Motion Parameters, Bulletin of the Seismological Society of America; February 1999; v. 89; no. 1; p. 311-316 

