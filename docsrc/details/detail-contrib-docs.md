AQMS Documentation Structure {#detail-contrib-docs}
=========================================
This page explains how the AQMS documentation pages are build from the aqms-
docs repository and all the other AQMS repositories hosted by the aqms.swg
group on gitlab.com.

This repository contains source files for documentation as well as
configuration files for Doxygen. By specifying the other aqms.swg repositories
as git submodules in the .gitmodule file, it allows all the documentation to be
created from this "superproject".

The Short Version
-----------------
If you just want to know what you have to do to add some documentation, see the
how-to page:

\ref how-to-contrib-aqms-docs

Automatic Gitlab pages
-------------
The HTML output from doxygen is served to the public using GitLab pages at
https://aqms.swg.gitlab.io/aqms-docs . The pages are automatically built and
published by the GitLab.com pipeline. GitLab.com provides gitlab-runners on
their servers and maintains the web-servers that publish the pages, requiring
no maintenance from the AQMS SWG. The pipeline is enabled by the presence of
the .gitlab-ci.yml file at the root of the aqms-docs git repository. Every time
someone pushes a change to the aqms-docs master branch, the documentation will
get re-built and re-published.

Git Submodules
--------------
We chose to break up the old AQMS subversion repository into lots of smaller
repositories, and to pull them all together using the concept of submodules.
Submodules allow you to keep a Git repository as a subdirectory of another Git
repository. This lets you clone *(RH: don't use the command clone however, use the git submodule commands)* another repository into your project and keep
your commits separate.

If you run `git help submodules`, this is listed as the second reason why one
might want to use submodules:

           Splitting a (logically single) project into multiple repositories
           and tying them back together. This can be used to overcome current
           limitations of Git’s implementation to have finer grained access:

           ·   Size of the Git repository: In its current form Git scales up poorly for large repositories containing
               content that is not compressed by delta computation between trees. For example, you can use submodules
               to hold large binary assets and these repositories can be shallowly cloned such that you do not have a
               large history locally.

           ·   Transfer size: In its current form Git requires the whole working tree present. It does not allow
               partial trees to be transferred in fetch or clone. If the project you work on consists of multiple
               repositories tied together as submodules in a superproject, you can avoid fetching the working trees
               of the repositories you are not interested in.

           ·   Access control: By restricting user access to submodules, this can be used to implement read/write
               policies for different users.

Specifically to our aqms.swg group on gitlab.com, where we have all the repos
under one group AND we want to use the gitlab.com pipeline, we have to add
submodules using relative paths:

```
renate@gaia:~/Programs/gitlab/aqms-docs$ git submodule add ../aqms-db-pg
Cloning into '/home/renate/Programs/gitlab/aqms-docs/aqms-db-pg'...
Username for 'https://gitlab.com': jrhartog
Password for 'https://jrhartog@gitlab.com':
warning: redirecting to https://gitlab.com/aqms.swg/aqms-db-pg.git/
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 6 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (6/6), done.

renate@gaia:~/Programs/gitlab/aqms-docs$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	modified:   .gitmodules
	new file:   aqms-db-pg

renate@gaia:~/Programs/gitlab/aqms-docs$ ls
aqms-db-pg  aqms-dbselect  aqms-libs  docsrc  README.md  templates
```
If I now navigate into the aqms-db-pg, you'll see that git knows it is a
completely separate repo with a different remote:

```
renate@gaia:~/Programs/gitlab/aqms-docs$ cd aqms-db-pg/
renate@gaia:~/Programs/gitlab/aqms-docs/aqms-db-pg$ ls
CHANGELOG  README.md
renate@gaia:~/Programs/gitlab/aqms-docs/aqms-db-pg$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
renate@gaia:~/Programs/gitlab/aqms-docs/aqms-db-pg$ git remote -v
origin	https://gitlab.com/aqms.swg/aqms-db-pg (fetch)
origin	https://gitlab.com/aqms.swg/aqms-db-pg (push)
```
Now, let's commit this change to aqms-docs.
```
renate@gaia:~/Programs/gitlab/aqms-docs$ git commit -am "added the aqms-db-pg
submodule"
[master f2f4cd5] added the aqms-db-pg submodule
 2 files changed, 4 insertions(+)
 create mode 160000 aqms-db-pg
renate@gaia:~/Programs/gitlab/aqms-docs$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
```
Alright, so now my local repo knows about the submodule, but the remote one on
gitlab.com does not yet. Let's push the change there:

```
renate@gaia:~/Programs/gitlab/aqms-docs$ git push origin master
Username for 'https://gitlab.com': jrhartog
Password for 'https://jrhartog@gitlab.com':
Counting objects: 3, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 375 bytes | 375.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0)
To https://gitlab.com/aqms.swg/aqms-docs.git
   12dceb2..f2f4cd5  master -> master
```
Now it is complete on the remote repo as well. See:

![Screenshot of gitlab.com/aqms.swg/aqms-docs page showing the newly added
submodule aqms-db-pg](docsrc/images/new-submodule-at-remote.png)


Doxygen
-------
Doxygen allows for quite a bit of flexibility, but it is important to understand
following behaviors:

* Markdown files are automatically interpreted as "pages".
* Documentation for each AQMS component will be listed under the correct Module
if a group is defined for the module
and files that are part of the module include the `@ingroup group_modulename`
doxygen tag.
* doxygen uses relative paths, relative to the directory from which it is run.

More notes, to be organized later.
-------------------------------------
A POSSIBLE WORKFLOW FOR AN ARTIFICIALLY SPLIT REPO

           # Enable recursion for relevant commands, such that
           # regular commands recurse into submodules by default
           git config --global submodule.recurse true

           # Unlike the other commands below clone still needs
           # its own recurse flag:
           git clone --recurse <URL> <directory>
           cd <directory>

           # Get to know the code:
           git grep foo
           git ls-files

           # Get new code
           git fetch
           git pull --rebase

           # change worktree
           git checkout
           git reset
