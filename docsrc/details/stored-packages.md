# AQMS Stored Packages  {#aqms-stored-procs}  
    
  
*  [Names](#sp-names)  
*  [Percent of PL/SQL and Java Use](#percent-used)
*  [Dependencies](#dependencies)
  

 <a name="aqms-sp"></a>  
  
All packages contain function *getPkgId* which returns the version of last package checkin. Some production RT (real-time) and postprocessing code modules call functions/procedures in some of these packages.  
  
Source is at: source: [https://gitlab.com/aqms-swg/aqms-db-ora/-/tree/master/storedprocedures](https://gitlab.com/aqms-swg/aqms-db-ora/-/tree/master/storedprocedures)​   
  
  
## Names <a name="sp-names"></a>    
   

[ampsetmaker](stored-packages/ampsetmaker.md)   
Functions to create new Strong Ground Motion (sm) ampset and assocevampset table rows for those events that pre-date production use of the ampgen/gmp2db/AmpGenPp code versions that populate these tables.  
  
[assocamp](stored-packages/assocamp.md)  
Functions called by java code module Gmp2Db for creating event associated sets of amplitudes derived the from strong-ground motion packets exchanged between networks.   
  
[copydata](stored-packages/copydata.md)  
Functions to copy an event's parametric table data from a specified database link and from schema to the user's login schema.   
  
[datum](stored-packages/datum.md)  
Functions to manage origin datum and depth (geoidal versus model).  
  
[deltaz](stored-packages/deltaz.md)  
Functions to compute distance and azimuth for all the arrivals associated with an origin and update the AssocArO entries accordingly.    
  
[db_cleanup](stored-packages/db_cleanup.md)  
Must be installed in same schema as database parametric tables (i.e. TRINETDB not CODE). Functions used by DBA to completely remove all table rows associated with a specified evid or date range for those events whose selectFlag=0.   
  
[epref](stored-packages/epref.md)  
Functions used by code modules to insert event,origin, preferred magnitude and other event association data.   
  
[eventpriority](stored-packages/eventpriority.md)  
Not used in production. Functions to determine a priority value from an event's age, its magnitude, and its origin quality   
  
[file_util](stored-packages/file_util.md)    
Not used in production. Functions to create/delete file at specified pathname. User must have privileges.   
  
[formats](stored-packages/formats.md)  
Functions to format CUBE and aftershock probability report strings for email or other transport.   
  
[geo_region](stored-packages/geo_region.md)  
Functions to determine whether point is inside polygon of geographic region defined in the gazetteer_region table.   
  
[importevent](stored-packages/importevent.md)  
Functions similar to copydata, except that the imported parametric table data rows are resequenced using user's schema sequences.   
  
[irinfo](stored-packages/irinfo.md)  
Functions used by TMTS to retrieve channel response gain poles and zeros.  
  
[magpref](stored-packages/magpref.md)  
Functions to obtain the priority (rank) value of a particular event magnitude. Functions to set the event/origin preferred magnitude to the associated magnitude with the highest priority value. Utilizes rules defined by the MagPrefPriority table rows determine event magnitude ranking.  
  
[orgpref](stored-packages/orgpref.md)  
Functions for determining the priority rank of a Origin.  
      
[match](stored-packages/match.md)  
Functions used by code to determine if duplicate event exists in database archive.   
  
[pcs](stored-packages/pcs.md)  
Functions invoked by code modules using id state posting to determine for which ids to process event related data.   
  
[quarry](stored-packages/quarry.md)  
Functions to determine closest quarry to an event location.   
  
[requestcard](stored-packages/requestcard.md)  
Functions related to request_cards.  
  
[sequence](stored-packages/sequence.md)  
Functions to get a range of sequence numbers key ids of rows to be inserted into the database.   
  
[schema_util](stored-packages/schema_util.md)  
Functions to grant/revoke privileges declared in table PROD_TAB_PRIVS to schema user roles.   
  
[truetime](stored-packages/truetime.md)  
Functions to convert between nominal, true, and Strings date times.   
  
[util](stored-packages/util.md)  
Miscellaneous read-only functions used by other code modules, database lookup and date conversion.   
  
[wave](stored-packages/wave.md)  
Functions to retrieve archived miniseed timeseries from database as BLOB objects   
  
[wavearc](stored-packages/wavearch.md)  
Not used in Production. Function to generate waveform request cards for an event.   
  
[wavefile](stored-packages/wavefile.md)  
Functions to obtain event waveform file path locations from the WaveRoots table.   
  
[wheres](stored-packages/wheres.md)  
Functions to calculate distance/azimuth between points, to report the closest station, town, quarry, fault to known point, such as an event origin.   

``Init_Leap_Secs.sql`` is a PL/SQL script for initialization of the *leap_seconds* table.     
  
``dbclasses16UTCg-20160318_1331.jar`` and ``dbjavasrc-gUTC-20160318_1332.zip`` are the latest versions of stored procedures written in Java.  Instructions to compile and install them are [here](stored-packages/javastoredprocedures.md)   
  
``stateTransition_trigger_pkg.sql`` runs a test transition in the PCS system.  
     


## Percent of PL/SQL and Java Use <a name="percent-used"></a>    
  
The following is a synopsis of how the packages above are built. (circa August 2011)  
  
The pl/sql packages currently installed and used here that reference at least one stored java class in the body implementation are:  
  
**assocamp_pkg** - Uses java classes for association of Strong ground motion packet (GMP) amps imported with events in catalog. Only used by GMP import app.  
  
**db_cleanup_pkg** - If deleting waveform files, does a call to a delete_file function which uses a java class to delete waveform file from os disk.  
  
**event_priority_pkg** - Single lookup function uses by java class but could be deprecated and easily configured in pl/sql to give event a relative urgency rank (which I don't believe anybody pays any attention to).  
  
**formats_pkg** - Called for formatting strings needed by some postproc perl scripts like cubeMessage and QDDSsend, and DRP generating email text for earthquake summary This could be converted but would require some work.  
  
**wave_pkg** - Uses a java class to put waveform timeseries into BLOB, used by Jiggle when reading BLOBS of miniseed from database. Uses the java SeedHeader reader to figure out packet times.  
  
**wheres_pkg** - References many java classes that access the db gazetteer tables info, used by Jiggle, and some postproc perl scripts, beltPager. This package would be the one with the most work to convert from java implementation.  
  
The packages below reference a stored java class, but are not used (as far as I know) by other stored procedures, java apps, or perl modules in use here:  
  
**file_util_pkg** - A few function that reference Java classes for local disk file operations,  
  
**util_pkg** - Has a few date epoch time conversion functions that call a java class, but again, I don't believe these functions are used by any java apps or perl production scripts   
  

The packages listed below are used by Java apps and/or perl modules in production and do not have a JAVA classes in body implementation:  
  
*  epref_pkg.sql
*  geo_region_pkg.sql
*  magpref_pkg.sql
*  match_pkg.sql
*  quarry_pkg.sql
*  sequence_pkg.sql
*  truetime_pkg.sql
*  wavefile_pkg.sql 
  
I see a stand alone function "irInfo" that referencesa java class, Stephane or Ellen may know what this function is used for, probably some script. No idea about who is the author of java class irInfo.   
  
  
## Dependencies <a name="dependencies"></a>    
  
These are the database object dependencies for the stored procedures  
  
| Stored Procedure | Dependent Object | Comments |
------------------ | ---------------- | ----------
| [assocamp](stored-packages/assocamp.md) | AMP, AMPSET, AMPSETSEQ, AMPSETS, ASSOCAMO, ASSOCEVAMPSET, EVENT, ORIGIN, UNASSOCAMP | |
| [copydata](stored-packages/copydata.md) | AMP, ARRIVAL, ASSOCAMM, ASSOCAMO, ASSOCARO, ASSOCCOM, ASSOCCOO, ASSOCWAE, CODA, CREDIT, EVENT, EVENTPREFMAG, FILENAME, importevent, MEC, NETMAG, ORIGIN, ORIGIN_ERROR, REMARK , WAVEFORM | | 	
| [db_cleanup](stored-packages/db_cleanup.md) | AMP, AMPSET, ARRIVAL, ASSOCAMM, ASSOCAMO, ASSOCARO, ASSOCCOM, ASSOCCOO, ASSOCEVAMPSET, ASSOCNTE, ASSOCWAE, CODA, CREDIT, EVENT, EVENTPREFMAG, FILENAME, MEC, MECCHANNEL, MECDATA, MECFREQ, MECFREQDATA, MECOBJECT, NETMAG, NETTRIG, ORIGIN, ORIGIN_ERROR, pcs, REMARK, SUBDIR, TRIG_CHANNEL, truetime, WAVEFORM, WAVEROOTS | | 	
| [epref](stored-packages/epref.md) | ALARM_ACTION, AMP, ARRIVAL, ASSOCAMM, ASSOCAMO, ASSOCARO, ASSOCCOM, ASSOCCOO, ASSOCWAE, CODA, CREDIT, CREDIT_ALIAS, EVENT, EVENTPREFMAG, EVENTPREFMEC, EVENTPREFOR, geo_region, magpref, MAGSEQ, MEC, NETMAG, ORIGIN, pcs, REMARK, truetime, WAVEFORM | |	
| [eventpriority](stored-packages/eventpriority.md) | EVENTPRIORITYPARAMS | |
| [geo_region](stored-packages/geo_region.md) | ASSOC_REGION_GROUP, COORDINATES, GAZETTEERPT, GAZETTEER_REGION, LATLON | | 	
| [importevent](stored-packages/importevent.md) | AMP, AMPSEQ, ARRIVAL, ARSEQ, ASSOCAMM, ASSOCAMO, ASSOCARO, ASSOCCOM, ASSOCCOO, ASSOCWAE, CODA, COMMSEQ, COSEQ, EVENT, EVENTPREFMAG, EVSEQ, FILENAME, MAGSEQ, MEC, MECSEQ, NETMAG, ORIGIN, ORIGIN_ERROR, ORSEQ, REMARK, WASEQ, WAVEFORM | | 	
| [irinfo](stored-packages/irinfo.md) | T_ARRAY_COMPLEX, T_COMPLEX, T_IRINFO | | 	
| [magpref](stored-packages/magpref.md) | EVENT, EVENTPREFMAG, GAZETTEER_REGION, geo_region, MAGPREFPRIORITY, NETMAG, ORIGIN, truetime, util | |
| [match](stored-packages/match.md) | ALL_TABLES, ARRIVAL, ASSOCARO, EVENT, ORIGIN, truetime, wheres | | 	
| [pcs](stored-packages/pcs.md) | AUTOPOSTER, ID_TABLE, PCS_SIGNAL, PCS_STATE, PCS_TRANSITION, RT_ROLE, SIGSEQ | | 	
| [quarry](stored-packages/quarry.md) | EVENT, GAZETTEERPT, GAZETTEERQUARRY, NETMAG, ORIGIN, QB_SCHEDULE, QB_TIME, truetime, wheres | |
| [schema_util](stored-packages/schema_util.md) | ALL_TAB_PRIVS, DBMS_JAVA, DBMS_OUTPUT, PROD_TAB_PRIVS, ID_TABLE | | 	
| [truetime](stored-packages/truetime.md) | EPOCHTIMEBASE TABLE, LEAP_SECONDS | |
| [util](stored-packages/util.md) | AMP, AMPSET, ASSOCAMM, ASSOCAMO, ASSOCEVAMPSET, ASSOCWAE, CREDIT, DBMS_OUTPUT, EVENT, EVENTPREFMAG, NETMAG, ORIGIN, REMARK, STATION_DATA, truetime, WAVEFORM, wheres | | 	
| [wave](stored-packages/wave.md) |	ASSOCWAE, DBMS_LOB, wavefile, WAVEFORM | |
| [wavefile](stored-packages/wavefile.md) | ASSOCWAE, EVENT, FILENAME, ID_TABLE, SUBDIR, truetime, WAVEFORM, WAVEROOTS | |
| [wheres](stored-packages/wheres.md) | GAZETTEERPT, ORIGIN | | 
  

## Stored procedure maintainers  
  
| Object Type | Object Name | Contact | 
------------- | ----------- | ---------
| FUNCTION | BEST_MAG | Caltech |
| PACKAGE | STPDATETIME | Caltech | 
| | | |
| PACKAGE | TRUETIME | NCEDC | 
| FUNCTION | NOMINAL2STRING | |                   
| FUNCTION | NOMINAL2TRUE | |                    
| FUNCTION | STRING2NOMINAL | |                   
| FUNCTION | STRING2TRUE | |                      
| FUNCTION | TRUE2NOMINAL | |                     
| FUNCTION | TRUE2STRING | |
| | | |
| PACKAGE | WAVEFILE | Caltech |
| FUNCTION | GETHOTEVID | |                       
| FUNCTION | WAVEFILEDFILE | |                   
| FUNCTION | WAVEFILENAME | |                     
| FUNCTION | WAVEFILEPATH | |                     
| FUNCTION | WAVEFILEPATHBYEVID | |               
| FUNCTION | WAVEFILEROOT | |                     
| FUNCTION | WAVEFILEROOTBYTIME | | 
| | | |
| PACKAGE | COPYDATA | Caltech | 
| PACKAGE | DB_CLEANUP | Caltech | 
| PACKAGE | GEO_REGION | Caltech | 
| PACKAGE | WAVE | Caltech |
| PACKAGE | WHERES | Caltech | 
| | | |
| PACKAGE | PCS | Caltech | 
| PROCEDURE | DOTRANSITIONS | Caltech | 
| | | |
| PACKAGE | EVENTPRIORITY | Caltech | 
| PACKAGE | MATCH | Caltech | 
| PACKAGE | UTIL | Caltech |
| PACKAGE | FILE_UTIL | Caltech | 
| | | |
| PACKAGE | ASSOCAMP | Caltech | 
| PACKAGE | FORMATS | Caltech | 
| PACKAGE | MAGPREF | Caltech | 
| PACKAGE | WAVEARC | Caltech |  
  
  

- \subpage ampsetmaker-stored-package
- \subpage assocamp-stored-package
- \subpage copydata-stored-package
- \subpage datum-stored-package
- \subpage db_cleanup-stored-package
- \subpage deltaz-stored-package
- \subpage epref-stored-package
- \subpage eventpriority-stored-package
- \subpage file_util-stored-package
- \subpage format-stored-package
- \subpage geo_region-stored-package
- \subpage importevent-stored-package
- \subpage irinfo-stored-package
- \subpage magpref-stored-package
- \subpage match-stored-package
- \subpage orgpref-stored-package
- \subpage pcs-stored-package
- \subpage quarry-stored-package
- \subpage requestcard-stored-package  
- \subpage sequence-stored-package 
- \subpage schema_util-stored-package
- \subpage truetime-stored-package
- \subpage util-stored-package
- \subpage wave-stored-package
- \subpage wavearc-stored-package
- \subpage wavefile-stored-package
- \subpage wheres-stored-package  









  

  
