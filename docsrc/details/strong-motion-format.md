# Earthworm Strong Motion II format {#strong-motion-format}  
  
The Earthworm StrongMotionII format is used for exchange of amplitude data among AQMS (and other) system.   
    
[StrongMotionII format documentation](http://folkworm.ceri.memphis.edu/ew-doc/PROGRAMMER/strongmotionII_format.html)  
  
  
### Description     
  
```
"SNCL:" sta.comp.net.loc
"TIME:" date-time
"ALT:" alternate date-time "CODE:" altcode
"PGA:" pga "TPGA:" tpga
"PGV:" pgv "TPGV:" tpgv
"PGD:" pgd "TPGD:" tpdg
"RSA:" nrsa"/"pdrsa rsa"/"pdrsa rsa...
"QID:" qid qauthor
```
  

### Example  
  
```
SCNL: 5444.HLZ.NP.--
TIME: 2010/03/31 09:21:16.880
PGA: 0.0101 TPGA: 2010/03/31 09:21:23.170
PGV: 0.0017 TPGV: 2010/03/31 09:21:22.870
PGD: 0.0024 TPGD: 2010/03/31 09:21:23.130
RSA: 3/0.30 0.0286/1.00 0.0244/3.00 0.0034/
QID: 14606268 004014100:CI

SCNL: 5444.HLN.NP.--
TIME: 2010/03/31 09:21:20.030
PGA: 0.0106 TPGA: 2010/03/31 09:21:40.510
PGV: 0.0016 TPGV: 2010/03/31 09:21:40.800
PGD: 0.0015 TPGD: 2010/03/31 09:21:38.040
RSA: 3/0.30 0.0210/1.00 0.0266/3.00 0.0057/
QID: 14606268 004014100:CI

SCNL: 5444.HLE.NP.--
TIME: 2010/03/31 09:20:46.010
PGA: 0.0107 TPGA: 2010/03/31 09:21:34.680
PGV: 0.0013 TPGV: 2010/03/31 09:21:41.000
PGD: 0.0020 TPGD: 2010/03/31 09:20:46.010
RSA: 3/0.30 0.0246/1.00 0.0147/3.00 0.0013/
QID: 14606268 004014100:CI
```
  
The format allows up to six different amplitude readings for a single channel. Any number of channels may be included in a single message file. Some of the format lines are nonsense outside of an Earthworm context and are not populated. Other lines are optional. For example, a well-formed message may contain only one of the six possible amplitude readings.   
