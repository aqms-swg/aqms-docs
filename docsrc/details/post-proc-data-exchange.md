# Post Processing Data Import And Export {#post-proc-data-exc}  
  

## Amplitude Import And Export   
  
Amplitude data can be automatically and rapidly exchanged among AQMS systems. The primary consumer of these amplitudes is ShakeMap, the Internet Quick Report (IQR), and data archives. The ground motion amplitude data types includes PGA, PGV, PGD, SP.3, SP1.0 & SP3.0.   

In order to build a complete data set, each center must export the data they produce and import the data they do not. The message transport mechanism is EarthWorm modules [sendfile2/getfile2](ttp://folkworm.ceri.memphis.edu/ew-doc/ovr/getfileII_sendfileII.html). The message format is [CISN Ground Motion Message format](https://aqms-swg.gitlab.io/aqms-docs/cisn-ground-motion-format.html).

 

- \subpage ampimport
- \subpage ampexport

  
  
## Waveform Import and Export  
  
Waveform data can be automatically and rapidly exchanged among AQMS systems. Currently this is done only for strong motion waveforms to and from the CGS CSMIP program which does not run an AQMS and cannot do "native" realtime waveform exchange.   
  
  
- \subpage waveformimport
- \subpage waveformexport
