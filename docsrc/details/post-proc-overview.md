# Post Processing %System Overview {#post-proc-overview}  
  
The goal of the post processing system is to 
The post processing system for C.. is a database centric set of processes that run to collect subnet triggers, hypocenters, and waveforms and provide analysis support to the analysts. See diagram below:   
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/AQMS_postproc_diagram.png">

Describe the key components: archival DB, waveform archive store, solution servers, duty review page and image generation.

​[Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs/) is the post processing tool the analyst uses to pick phase arrivals and redo event locations and Ml and/or Md magntitudes. Events can also be reprocessed in batch via [PCS](pcs.html) using the Java application [hypomag](hypomag).  
  
- \subpage perl-utils-overview

<ul>
    <li><b>Post Processing Tasks and Man pages</b></li>
    - \subpage hypomag  
    - \subpage pcs
    - \subpage post-proc-autoposter
    - \subpage rcg
    - \subpage newtrig
    - \subpage drp
    - \subpage trp
    - \subpage webicorder
    - \subpage swarmon
    - \subpage post-proc-data-exc
</ul>
- \subpage post-proc-maintenance