# %Amplitude Association {#amp-association}  
  
## Overview  
  
[Amplitudes imported](https://aqms-swg.gitlab.io/aqms-docs/ampexc.html) from other networks are inserted into the UnassocAmp table. It is identical in structure to the [CISN schema](https://www.ncedc.org/db/Documents/NewSchemas/schema.html) Amp table. An Oracle stored procedure, [AssocAmp](https://aqms-swg.gitlab.io/aqms-docs/assocamp-stored-package.html) scans the UnassocAmp table and attempts to associate imported amps with events in the Event/Origin tables of the dbase. The association is based on a time-window algorithm. An amplitude will only be associated with one event. If multiple events are candidates the amp will be associate with the ‘best’ fitting event. The association is consummated in the dbase by moving the amp form the UnassocAmp table to the Amp table and writing an AssocAmO row. The stored procedure does housekeeping on the UnassocAmp table by deleting rows with load dated (LDDATE) older than seven days and deleting all rows, even new ones, that have Net/Sta/Seedchan values that do not appear in Channel_Data.   
  
## The Code  
  
%Amplitude association is done by Java code running as a stored procedure, AssocAmp, in the database. The main code and its supporting functions are in a package called AssocAmp. The authoritative source code is part of the Java package org.trinet.storedprocs.association. The class files are loaded into the database.  
  
Functions in the AssocAmp package:   
  
*  AssocAmps - Attempt to associate all Amps in the UnassocAmp table. This is the ‘main’ that does association. It takes no arguments.  
*  GetAssocEvent - return the EVID of the best fitting event for an amplitude  
*  MoveAndAssoc - Move an amp from UnassocAmp table to Amp table and write an AssocAmO row. Return 1 on success, 0 on failure.  
*  MoveAndAssoc2 - Move an amp from UnassocAmp table to Amp table and write an AssocAmO row. No return values.  
*  CleanUp – Delete aged-out amps and amps that have no matching Net/Sta/Seedchan in Channel_Data.  
  

## The Association Algorithm  
  
The code extracts amps from the UnassocAmp table that are grouped by time. Amp groups are delimited by intervals of 300sec or more with no amps.  
  
A list of candidate events is then retrieved from the dbase. These are events that might possibly have produced energy during the time of the amps in the group.  
  
For each amp/event pair the code then calculates the distance and P & S travel-time. The amp will be associate if it lies within a window where seismic energy is likely for a given station/event pair (see Figure). The window is defined by the S–P interval itself plus the coda duration for the event magnitude. If no magnitude is available for an event, the window is defined by the S–P interval plus the S-P duration after the S arrival. A small “slop” time (1sec) is added before the window.  
  
If an amp time falls in this window it will be associated. If the amp is within the window of more than one event, it will be associated with the event with the largest magnitude.  
  
Amps that fail to associate remain in the UnassocAmp table for a period of time (7 days) so they can be associated with late-arriving or relocated events.   

<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/assocamp.gif" />
