AQMS Overview {#aqms-overview}
=================================
  
**AQMS** stands for <b>A</b>NSS <b>Q</b>uake <b>M</b>onitoring <b>S</b>ystem   
  
AQMS can be used to detect events (earthquakes) in real-time. AQMS can further refine the parameters of already detected events in both [automated](???) and [manual](???) processing. 

AQMS is using gitlab.com and doxygen to couple source browsing, documentation and discussing software issues related to the AQMS package and ticketing tasks. CHEETAH was the code name for the first release of the AQMS software originally known as the CISN Real Time and Post Processing Software. AQMS is based on standard earthquake location algorithms using ​Earthworm 7.X and an Oracle 12 or Postgres 10/11/12 %Database with C++ and Java applications for support and processing.

In recent years, the software has been ported from SPARC to Linux and a recent project at the University of Washington (Seattle) has ported the database to ​PostgreSQL (an open source database). The software working group is currently working on cleaning up the SVN repository and assigning a unified version number to both the database and the software packages. A group at Caltech is unifying the distribution of the AQMS software for Linux.  
  
As mentioned above, AQMS is built on top of earthworm. See [http://www.earthwormcentral.org/documentation4/index.html](http://www.earthwormcentral.org/documentation4/index.html).   
  
**Source code repo** : https://gitlab.com/aqms-swg  
  

- \subpage aqms-software-overview  
- \subpage aqms-requirements  
- \subpage aqms-installation          
- \subpage aqms-products   
    - [Origin](aqms-origin-product.html)              
    - [Magnitude](aqms-magnitude-product.html)    
    - [Amplitude](aqms-amplitude-product.html)        
    - [Focal Mechanism](aqms-focmec-product.html)    
    - [Moment Tensor](detail-tmts2.html)     
    - [Waveforms](aqms-waveform-product.html)  
- \subpage aqms-processing  
    - [Automated](aqms-automated-processing.html)  
        - [Real-time processing overview](real-time-overview.html)  
        - [Real-time maintenance](real-time-maintenance.html)  
        - [Post processing overview](post-proc-overview.html)    
    - [Manual](aqms-manual-processing.html)  
        - [Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs)
    - [Messaging](aqms-messaging.html)  
        - [CMS](man-cms.html)       
        - [PCS](pcs.html)    
        - [Alarming](alarming.html)  
    - [Database](db-overview.html)   
        - [Schema documentation](http://www.ncedc.org/db/Documents/NewSchemas/schema.html)    
        - [Stored procedures](aqms-stored-procs.html)    
        - [Stored procedure usage](db-map.html)  
        - [Oracle database overview](ora-db.html)    
        - [Postgres database overview](db-postgres-overview.html)   
    - [Utilities](aqms-utilities.html)  
- \subpage aqms-gui  
    - [Jiggle](https://aqms-swg.gitlab.io/jiggle-userdocs)    
    - [DRP](drp.html)  
    - [TRP](trp.html)    
    - [Web TMTS](web-tmts.html) 
- [Man Pages](manpages.html)    
- [How To](how-to.html)      
- \subpage aqms-glossary  
- \subpage aqms-coding-conventions  
- \subpage aqms-eula   

<!--
<ul>
    <li>\subpage aqms-software-overview</li>  
    <li>\subpage aqms-requirements</li>  
    <li>\subpage aqms-installation</li>        
    <li>\subpage aqms-products
      \htmlonly
      <ul>  
            <li>Origin</li>              
            <li>Magnitude</li>  
            <li><a href="aqms-amplitude-product.html">Amplitudes</a></li>        
            <li>Focal Mechanism</li>  
            <li><a href="https://aqms-swg.gitlab.io/aqms-docs/detail-tmts2.html">Moment Tensor</a></li>  
            <li>Waveforms</li> 
        </ul>  
      \endhtmlonly
    </li>   
    <li>\subpage aqms-processing    
        \htmlonly    
        <ul>                        
            <li>\subpage aqms-automated-processing         
                <ul>  
                    <li>\subpage real-time-overview</li>  
                    <li>\subpage real-time-maintenance</li>  
                    <li>\subpage post-proc-overview</li>  
                </ul>              
            </li>      
            <li>\subpage aqms-manual-processing                 
                <ul>  
                    <li><a href="https://aqms-swg.gitlab.io/jiggle-userdocs">Jiggle</a></li>      
                </ul>  
            </li>  
            <li>\subpage aqms-messaging              
                <ul>  
                    <li><a href="cmd.html">CMS</a></li>     
                    <li><a href="pcs.html">PCS</a></li>  
                    <li><a href="alarming.html">Alarming</a></li>  
                </ul>                                 
            </li>
            <li>\subpage db-overview                  
                <ul>  
                    <li><a href="http://www.ncedc.org/db/Documents/NewSchemas/schema.html">Schema documentation</a></li>  
                    <li><a href="aqms-stored-procs.html">Stored procedures</a></li>  
                    <li><a href="db-map.html">Stored procedure usage</a></li>  
                    <li><a href="ora-db.html">Oracle database overview</a></li>  
                    <li><a href="db-postgres-overview.html">Postgres database overview</a></li>  
                </ul>                  
            </li>   
            <li>\subpage aqms-utilities</li>   
        </ul>  
        \endhtmlonly      
    </li>
    <li>\subpage aqms-gui        
        \htmlonly   
        <ul>  
              <li><a href="https://aqms-swg.gitlab.io/jiggle-userdocs">Jiggle</a></li>  
              <li><a href="drp.html">DRP</a></li>  
              <li><a href="trp.html">TRP</a></li>  
              <li><a href="web-tmts.html">Web TMTS</a></li>  
        </ul>            
        \endhtmlonly 
    </li>  
    <li>[Man Pages](manpages.html)</li>  
    <li><a href="how-to.html">How To</a></li>    
    <li>\subpage aqms-glossary</li>  
    <li>\subpage aqms-coding-conventions</li>  
    <li>AQMS End User License Agreement</li>  
</ul>
-->  

<!--
-  \subpage aqms-software-overview
-  Installation
-  Products ?
-  Processing  
    -  Automated  
    -  Manual
-  Messaging  
    -  CMS  
    -  \subpage pcs  
    -  Alarming  
-  Database
-  GUI
    -  Jiggle        
    -  \subpage drp
    -  \subpage trp
-  Utilities and Tools
    -  cattail
    -  ampdump
    -  \subpage qml
    -  \subpage siggen  
    -  \subpage sigswitch
-  Man pages
-  How-Tos  
-  Glossary
-  AQMS End User License Agreement
-->

<!--
   
  
- \subpage aqms-software-overview
  
- \subpage amp-data-types
- \subpage cisn-ground-motion-format
- \subpage strong-motion-format


### Real-time Systems  
  
- \subpage real-time-maintenance  
  
  
### Post Processing Systems  
  
<ul>
    <li><a href="post-proc-overview.html">Post Processing Software Overview</a>
        <ul>
            <li><a href="perl-utils-overview.html">Perl Utilities (ORACLE)</a>
        </ul>
    </li>
</ul>  
- \subpage post-proc-overview

  
    

### %Database Schema  
-->
<!--\ref db-overview    -->
<!--
\subpage db-overview
\htmlonly
<ul>
    <li><a href="http://ncedc.org/db/Documents/NewSchemas/schema.html">Schema documentation</a></li>    
    <li><a href="schema-change-notes.html">Schema change notes</a></li>
    <li><a href="default-db-users.html">Default AQMS Database Users</a></li>
    <li><a href="aqms-stored-procs.html">AQMS Stored Package Specs</a></li>  
    <li><a href="db-map.html">AQMS Application Stored Procedure Usage</a></li>
    <li><a href="db-maintenance.html">Database Maintenance Tasks</a></li>  
</ul>
\endhtmlonly  
- \subpage amp-association  
- \subpage db-postgres-overview


### TMTS2
\ref detail-tmts2
-->
\htmlonly
<a href="detail-overview.html">Next</a> or back to
<a href="index.html">Starting Page</a>.
\endhtmlonly
