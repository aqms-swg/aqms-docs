# Focal Mechanisms {#aqms-focmec-product}  
  
AQMS produces focal mechanisms as part of it's computation to help further describe a detected event. There are two options to calculate focal mechanisms.  
  
1.  **FMAR** : First Motion/Amplitude Ratio (FMAR) focal mechanism. The algorithm used for calculation is also known as HaSH and FMAR and HASH are often use interchangeably.  
2.  **FP or FPFIT** : Fault Plane focal mechanism  
  

## FMAR    
  
-  \subpage focmec-hash    
-  \subpage man-hash    
  

## FP    
  
-  \subpage man-fp_cont
  

## Source code  
  
*  **FMAR** : https://gitlab.com/aqms-swg/aqms-HASH  
*  **FP** : https://gitlab.com/aqms-swg/aqms-fp  