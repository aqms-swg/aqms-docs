# Manual Processing {#aqms-manual-processing}  
  
AQMS can further refine the parameters of already detected events via both [automated](???) and [manual](???) processing. For manual processing,  this is achieved via Jiggle.  
  

### Jiggle  
  
Jiggle is a Java GUI application used for the analysis of the earthquake waveform time-series data stored in databases that have implemented the AQMS database schema. 
  
Click [here](https://aqms-swg.gitlab.io/jiggle-userdocs/) for detailed information about jiggle.   
  

#### Current capabilities  
  
*  Reading/writing of seismic data from/to a database source.  
*  Display of an earthquake catalog in a scrollable table.  
*  Display of the waveform timeseries, phase picks, and amplitudes of a selected catalog event.  
*  Manipulation of displayed waveforms (scaling, zooming, filtering, etc.).  
*  Editing and/or creation of waveform phase picks, amplitudes, and coda.  
*  Earthquake locations from phase picks (via a remote location service).  
*  Local magnitude calculations from amplitudes and/or coda (Ml, Md).  


#### Requirements  
  
https://aqms-swg.gitlab.io/jiggle-userdocs/install/
  
#### Installation  
  
https://aqms-swg.gitlab.io/jiggle-userdocs/install/  
  

#### %Configuration  
  
- [Property files](https://aqms-swg.gitlab.io/jiggle-userdocs/property_files/)  
- [Minimal customization](https://aqms-swg.gitlab.io/jiggle-userdocs/minimal_setup/)

  
#### Getting started with Jiggle  
  
https://aqms-swg.gitlab.io/jiggle-userdocs/tabviews/
  

#### %User Docs  
  
https://aqms-swg.gitlab.io/jiggle-userdocs  
  

#### Source Code
  
https://gitlab.com/aqms-swg/aqms-jiggle
