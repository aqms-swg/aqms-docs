# Waveform Import {#waveformimport}  

Waveforms associated with events are imported from other metworks using EarthWorm modules. As this is a continuously running system allows for event waveforms to be automatically ingested into the AQMS system. It should be kept in mind that there is an inherent latency in getting these waveforms. 
  
*  Event waveforms from [NetQuakes](https://earthquake.usgs.gov/monitoring/netquakes) and NetWorms are received via EW modules ([sendfileII/getfileII](http://folkworm.ceri.memphis.edu/ew-doc/ovr/getfileII_sendfileII.html)).  
*  The waveforms are loaded into a Winston Wave Server (WWS). Alternatively, they could be loaded into a CWB.  
*  A proxy wave server (PWS) is setup to retrieve waveforms from the WWS.  
*  The waveform achiver uses the PWS to retrieve the waveforms and archive them.  

