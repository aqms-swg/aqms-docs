# Post Processing Maintenance Tasks {#post-proc-maintenance}  
  
  *  [Database related tasks](#db-related-tasks)  
    *  [Backup the database](#backup)  
    *  [Restore the archdb database](#restore)
    *  [Updating Event Types](#update-event-types)  
    *  [Magprefpriority configuration](#magprefpriority)  
    *  [Replication checking](#rep-check)  
    *  [Fixing Replication](#fix-rep)  
    *  [Increase Table space in the database](#increase-tabspace)   
    *  [Updating Stored procedures](#update-sp)  
  *  [log file filling and review](#logs)  
  *  [Maintenance and archiving of the waveform repository](#wf-repo)  
  *  [Adding/Deleting a station/channel or syncing metadata (changing configs)](#sta-metadata)  
    *  [Problems with channel updates](#channel-update-probs)   
  *  [What are request cards?](#req-cards)   
  *  [rcg - request cards are not working](#rcg)   
    *  [rcg reposting for subnet or event triggers](#rcg-repost)  
  *  [Triggering to get Waveforms by Hand](#trigger-by-hand)   
  *  [Archiving Waveforms and the Waveroots table](#arch-wfs)  
  *  [clients of PWS have problems connecting](#pws-prob)  
    
  
  
## Database related tasks <a name="db-related-tasks"></a>    
  
  
### Backup the database <a name="backup"></a>   
  
*  Backup is done automatically from an Oracle script that runs nightly at 1AM by the oracle user on the postprocessing host as a crontab job:  
``` 
    /app/oracle/backup/backup_lev_0.sh
```

    the backup data are stored in */app/oracle/backup/ARCHDB* directory. *backup_lev0.sh* contains:     
```
    #! /bin/bash

    dir=/app/oracle/backup/
    source ~/.oraenv

    rman target / nocatalog cmdfile $dir/archdb_bak_0.rcv > $dir/backup.log 2>&1

```
  
*  The file archdb_bak_0.rcv is a script for the Oracle backup utility *rman* and it contains:   
```
    run {
        backup
            as compressed backupset
            incremental level 0
            tag backup_db_level_0_compress
            database filesperset 1
            format '/app/oracle/backup/ARCHDB/df_t%t_s%s_p%p'
        plus archivelog delete input;

        configure retention policy to recovery window of 7 days;

        delete force obsolete;
    }
```
  
*  if you try and run a backup but some redo backup data has been deleted (.dbf files found /oralog/archdb), then you must flush the rman cache of ones that are not there. In this case run the following:  
```
    rman target / nocatalog


    # then at the RMAN> prompt type: 

    crosscheck archivelog all;
```
  
*  more on RMAN can be found at this very helpful link ​RMAN by Burleson http://www.dba-oracle.com/concepts/rman.htm  


### Restore the archdb database <a name="restore"></a>   
  
The postprocessing database can be restored from the regular backups above. *Note this procedure was written and tested at HVO in a live failure*  
  
*  This first step is to stop replication from the real time systems:     
```
    sqlplus repadmin/passwd@rtdb

    SQL > select job,what from dba_jobs
```

*  you are looking for the job number for the job that contains: sys.dbms_defer_sys.push(destination....   
  
*  once you have the job number (22 in this case):  
```
    SQL > execute dbms_job.broken(22,true,'4000/01/01');

    SQL>  select broken, job from dba_jobs;
```   

*  the result set should show a 'Y' next to your job number  
  
*  you will need to repeat this for the other real time host (rtdb2)  

*  then, you will need to shut down the database, so login as the oracle UNIX user and type:  
```
    sqlplus "/ as sysdba"

    BE VERY CAREFUL as this user since you now are the SUPER USER equivalent in Oracle. 

    SQL> shutdown immediate;

    SQL> quit
```  
  
*  Now, also as the oracle user, run     
```
    rman target / nocatalog 
    RMAN> restore database
    (this should produce a bunch of messages while rebuilding the database, telling you which of your backup files it is reading from)

    RMAN> recover database until time '2009-04-01:01:01:00'                  (or whenever your database dissappeared)

    RMAN> alter database open resetlogs
```
  
*  You should be able to quit, and then go into sqlplus and query your now refilled database tables!   
  
*  DO NOT forget to restart replication from the 2 RT databases   
  

### Updating Event Types <a name="update-event-types"></a>  
  
The <a href="schema-event-types.html">event types</a> fill in the event.etype database field and describe what the event is (blast, local, regional quake etc). Some times these need updating, but it should be infrequent. Renate H. has put together a nice wiki page on <a href="adding-event-types.html">Adding New Event Types</a>.  


### Magprefpriority configuration <a name="magprefpriority"></a> 

The magprefpriority table in the Oracle database governs how a given magnitude becomes PREFERRED. The detailed explanation of how this table is configured is available [here](mag-pref-priority.md).  

This can only be updated on the postproc database archdb and then pushed to each RT host for use by the modules that write magnitudes (localmag, ec, trimag). The steps to propagating the magprefpriority table is to simply run the purge_snap.pl snapshot sync'ing script on the specific database. This script can be found in the db directory and an example run for updating the snapshot view that contains the magprefpriority table on the rtdb2 database is shown below:  
```
    purge_snap.pl QC_01_MASTER rtdb2
```       
  

### Replication checking <a name="rep-check"></a> 
  
*  Use the SNW system to check on replication by looking at the DB-rt? where rt? is the database name of either RT host. To get replication started again there is a procedure described in the <a href="real-time-maintenance.html">RT maintenance tasks</a>.   
  

### Fixing Replication <a name="fix-rep"></a> 

Consult the [ReplicationFailure](../db-maintenance/replication-failure.md) page now.  


### Increase Table space in the database <a name="increase-tabspace"></a> 

*  Eventually the tablespace, the files on disk where the oracle tables are written, will fill up. When this happens, the user needs to use sqlplus to find the files related to a tablespace and enlarge it. Here is how:   
  
*  First find the tablespace that is problematic from SNW from the DB-archdb detail pages. In our example below, lets assume the tablespace name is ASSOC_SPACE that is nearing FULL.   
  
*  Then you need to find the filename associated with that table space as the oracle user sysdba:  
```
    sudo su - oracle

    then as user oracle:

    make sure the ENV ORACLE_SID is set to "archdb" for your shell

    csh: setenv ORACLE_SID archdb
    bash: export ORACLE_SID=archdb

    sqlplus "/ as sysdba"

    BE VERY CAREFUL as this user since you now are the SUPER USER equivalent in Oracle.

    find the filename to be resized:

    select tablespace_name, bytes, user_bytes, file_name from dba_data_files where tablespace_name='ASSOC_SPACE';

    will return:


    TABLESPACE_NAME                     BYTES USER_BYTES
    ------------------------------ ---------- ----------
    FILE_NAME
    --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    ASSOC_SPACE                     104857600  104792064
    /database/archdb/assoc_space01.dbf


    SQL> 
```   
  
*  finally, you need to increase the tablespace now that you know the data file by issuing this command as system oracle user:     
```
    sqlplus "/ as sysdba"

    alter database datafile '/database/archdb/assoc_space01.dbf' resize 500M;
```
  
*  event deletion from db - Ellen Yu has a script that will truncate the database for events that are not authoritative. This script should also remove the waveform files associated with the event. NAME OF SCRIPT HERE.   
  

### Updating Stored procedures <a name="update-sp"></a> 
  
First obtain the latest stored procedure files from https://gitlab.com/aqms-swg/aqms-db-ora/-/tree/master/storedprocedures. In the example below we will upgrade the magpref and epref (event preferences) stored procedures. For stored procedures, they must be run as the SQL user named "code":    
```
    sqlplus code/codepasswd@rtdb < epref_pkg_installer.sql

    sqlplus code/codepasswd@rtdb < magpref_pkg_installer.sql
```
  
To update a Java stored procedures, you must run the loadjava oracle program as the **UNIX oracle user**. The repo contains .zip files that have some stored procedures coded in java. TO load these, you must also use the **code Oracle user**. Grab the latest dbclasses jar file from the repo in the *stored_procedures* directory.   
```
    loadjava -user code/passwd@archdb -verbose -synonym -force -resolve dbclasses13UTC.DATESTRING.jar 1>loadjava.txt 2>&1
```
  
In general any loadjava or storedprocs updates via .sql should be run on all of an RSN's AQMS hosts.  
  
Use the https://gitlab.com/aqms-swg/aqms-db-ora/-/tree/master/storedprocedures and monitor the Jiggle web page for changes.  
  
**IMPORTANT NOTE:** if you upgrade the PCS package, this will cause the autoposter job to die and it will need restarting (see steps in RCG section below).   
  

## log file filling and review <a name="logs"></a> 

Check the post processing logs for the AQMS modules. For e.g. at the SCSN the post processing logs of most modules are found in */app/aqms/rt/logs*. Some of the Java modules keep their own logs in their directories (RCG and solserver for instance). Also, the solserver keeps a history of any solutions that were run for each server so that you can do a post mortem on them as well.  


## Maintenance and archiving of the waveform repository <a name="wf-repo"></a> 

(what happens when 1 Terabyte is used up)....Go buy another terabyte !!! Duh.   


## Adding/Deleting a station/channel or syncing metadata (changing configs) <a name="sta-metadata"></a> 

**Note that this procedure will change to use the PopulateRdseed and SimpleResponse scripts instead (TBD) . The Pro-C programs by Stephane Z are the preferred method now for loading metadata in a dataless seed volume into AQMS.**    
  
*  loading an updated stations view into the system. If you are using INV, then the command line below can be used to sync stations and channels into the CISN schema. Fill in your passwords as necessary:    
```
    java -jar InvToCISN.jar -v  -invUserName paulf -invPassword myINVpass \
            -cisnHost someRSN.isti.com -cisnPort 1521 -cisnUserName trinetdb -cisnPassword myPass -cisnSID archdb
```
  
*  Likewise for installing a dataless, the **InvToCISN.jar** program can be used, but the INV db commands are left off and **-datalessSeed** option should be used to point to the dataless file to be loaded.  
*  for the InvToCISN.jar program, if you are *reloading the entire dataless*, be sure and use the **-deleteTables** option to clear the station_data, channel_data, and simple_response tables of any older configurations.  
*  rcg station list needs updating by modifying the *rcg.sql*. For e.g. /app/aqms/rcg/rcg.sql . Note you must restart RCG's if your properties for auto refreshing channel lists is not often enough (see the RCG property **autoRefreshChannelListInterval**)  
*  pws (proxy wave server) station list needs updating. for e.g. /app/aqms/pws/pws.sql, and then restart pws (pwsctl.sh stop; pwsctl.sh start)  
*  any Jiggle configured channels (store these in /app/aqms/db directory).  
*  any magnitude station corrections that fill in the stacorrections table for trimag (ML and Me).  
*  hyp2000 configuration up to date in solution server directory (need a script for this, rsync?, much like refresh stations script)  
  *  solserver's do not need restarting if configurations of the hyp2000 files (vel models, station lists, calibrations are done). The reason is that the solserver simply invokes hyp2000 for each request as a separate instance, thus it finds the fresh configs if they are updated.   
*  Once the data are loaded into the archiving database, then proceed to adding the station to the shadow RT system.   
  

### Problems with channel updates <a name="channel-update-probs"></a> 
  
Note that sometimes you will hit a snag, where a dataless loads fine, but when you run some app_channels configuration scripts to load channels for RCG or PWS, it will fail because there already is a station loaded for the same time span. This will be indicated by the warning:     
```
    {aqms@helen:pws} ./update_channel_list

    SQL*Plus: Release 10.2.0.1.0 - Production on Thu Jul 17 14:14:50 2014

    Copyright (c) 1982, 2005, Oracle.  All rights reserved.


    Connected to:
    Oracle Database 10g Enterprise Edition Release 10.2.0.1.0 - 64bit Production
    With the Partitioning, OLAP and Data Mining options

    SQL> SQL> SQL> SQL> 0 rows deleted.

    SQL> 1 row deleted.

    SQL> SQL> 1 row created.

    SQL> SQL>   2    3    4  insert into appchannels (progid, net, sta, seedchan, location, config, ondate, offdate)
    *
    ERROR at line 1:
    ORA-00001: unique constraint (TRINETDB.CONFIG_CHANKEY01_1) violated
```
  
NOTE that the CONFIG_CHANKEY01 is described below:    
```
    CONSTRAINT config_chankey01 PRIMARY KEY (progid, net, sta, seedchan, location
```
  
You can find the suspect SCNL and probably the bad dataless using the example query below:   
```
    select net,sta,seedchan,location from channel_data where ondate < sysdate and offdate>sysdate group by net,sta,seedchan,location having count(*) >1;

    10:05:31 TPP@archdb:helen SQL>  /

    NET      STA    SEE LO
    -------- ------ --- --
    AO       OSAR   HHZ 00
    AO       OSAR   HHN 00
    AO       OSAR   HHE 00

    3 rows selected.


    The culprit can easily be seen when you pull all of the data for that Network and Station, note the dups of the HH* channels:

    NET      STA    SEE LO ONDATE              OFFDATE
    -------- ------ --- -- ------------------- -------------------
    AO       OSAR   HHE 00 2011-01-01 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HHE 00 2011-09-06 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HHN 00 2011-01-01 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HHN 00 2011-09-06 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HHZ 00 2011-01-01 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HHZ 00 2011-09-06 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HNE 10 2011-09-06 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HNN 10 2011-09-06 00:00:00 2099-01-01 00:00:00
    AO       OSAR   HNZ 10 2011-09-06 00:00:00 2099-01-01 00:00:00

    9 rows selected. 
```
  
## What are request cards? <a name="req-cards"></a>  
  
Request cards are rows in the request_card Oracle database table that describe requests for waveforms. Request cards get created by 2 variants of programs, either in C++ or in Java. The earlier request card generators (rcg and ntrcg) were written in C++ and are described in the ???RT manual pages???. The later variants were written in Java and are what all RSNs were configured to use and they are written up in the ???Post Processing manual pages???. The <a href="man-wavearchiver.html">wave archiver</a> will contact wave servers to pull data and fullfill request cards. Once the wave archiver has fulfilled a request for waveforms, it deletes the row in the request_card table. To check that you have request cards being generated, look at the request_card.lddate field of the most recent entries and see that the event id (evid) matches up with your latest events in the event table. This requires a little SQL to do. First find the most recent events evid's; this next query gets you the last 4 hours of events in your database. The query after that finds those same events that have unfulfilled request cards in the database still (note, the reason they are unfulfilled is a separate matter entirely ):   

```
    SQL> select lddate, evid from event where selectflag=1 and lddate >sysdate-.16 order by lddate
    2  ;

    LDDATE                    EVID
    ------------------- ----------
    2010/11/11 21:46:26   60000801
    2010/11/11 21:49:42   60000806
    2010/11/11 22:11:55   60000811
    2010/11/11 22:20:57   60000816
    2010/11/11 22:30:15   60000821
    2010/11/11 22:31:48   60000826
    2010/11/11 22:38:42   60000831
    2010/11/11 22:39:45   60000836
    2010/11/11 22:51:23   60000841
    2010/11/11 22:57:14   60000846

    10 rows selected.

    SQL> 


    SQL> select e.evid, count(*) from request_card r, event e where e.selectflag=1 and e.lddate>sysdate-.16 and e.evid=r.evid group by e.evid;

        EVID   COUNT(*)
    ---------- ----------
    60000836         11
    60000841         14
    60000831         13
    60000846         11

    SQL> 
```
  
For reference, here is the description of the request_card table at the time of this trac wiki entry:   

```
    SQL> desc request_card
    Name                                      Null?    Type
    ----------------------------------------- -------- ----------------------------
    EVID                                               NUMBER(15)
    AUTH                                      NOT NULL VARCHAR2(15)
    SUBSOURCE                                 NOT NULL VARCHAR2(8)
    NET                                       NOT NULL VARCHAR2(8)
    STA                                       NOT NULL VARCHAR2(8)
    SEEDCHAN                                  NOT NULL VARCHAR2(8)
    STAAUTH                                   NOT NULL VARCHAR2(15)
    CHANNEL                                            VARCHAR2(8)
    DATETIME_ON                               NOT NULL NUMBER(25,10)
    DATETIME_OFF                              NOT NULL NUMBER(25,10)
    REQUEST_TYPE                              NOT NULL VARCHAR2(1)
    LDDATE                                             DATE
    RCID                                      NOT NULL NUMBER(15)
    LOCATION                                  NOT NULL VARCHAR2(2)
    RETRY                                              NUMBER(38)
    LASTRETRY                                          DATE
    PRIORITY                                           NUMBER
    WFID                                               NUMBER
    PCSTORED                                           NUMBER

    SQL> 
```  
      

## rcg - request cards are not working <a name="rcg"></a> 

Okay, so request cards are not working (you have no waveforms being archived due to no request cards) and you have established that none of the latest events in your database are getting request cards created for them. Here are some steps to perform based on a problem found in a configuration of one RSN.  

1.  First make sure replication is working and that you are getting events coming in. To do this, check the event table on archdb and see that some events are later than the lastlddate in the autoposter entries.  
  
2.  MAKE ABSOLUTELY sure, that the hosts sending in data are configured so that one is in primary mode and one is in shadow mode. If both hosts are accidentally in shadow mode, you will get no autoposting since the autoposter only sets events to autopost that have event.selectflag=1!!!!  
  
3.  Then check that the Java request card generators (RCG's) are running; you should see two java procs running that look like this:     
```
    {aqms@helen:db} ps -ef |grep RCG |grep -v grep
    aqms  1864     1   0 22:00:05 pts/2       0:52 java -mx512m -DRCG_RT.props -DRCG_CONT_USER_HOMEDIR=/app/aqms/rcg/cfg -cp /
    aqms  1867     1   0 22:00:05 pts/2       0:55 java -mx512m -DRCG_Trigger.props -DRCG_CONT_USER_HOMEDIR=/app/aqms/rcg/cfg 
    {aqms@helen:db} 
```
  
4.  next determine that you have autoposter rows in the database (select * from autoposter) since the PCS system relies on autoposter rows to fire state transitions that the RCG's look for. You should see rows that look like this when you issue this query as the trinetdb oracle user:   
```
    SQL> select * from autoposter
    2  ;

    INSTANCENAME                   CONTROLGROUP         SOURCETABLE
    ------------------------------ -------------------- --------------------
    STATE                      RANK     RESULT ACTON  EV     LASTID
    -------------------- ---------- ---------- ------ -- ----------
    LASTLDDATE          LDDATE
    ------------------- -------------------
    RESULTTEXT
    --------------------------------------------------------------------------------
    NewEventPoster                 EventStream          archdb
    NewEvent                    100          1 EVENT  le
    2010/11/11 21:58:53 2010/11/11 21:58:54



    INSTANCENAME                   CONTROLGROUP         SOURCETABLE
    ------------------------------ -------------------- --------------------
    STATE                      RANK     RESULT ACTON  EV     LASTID
    -------------------- ---------- ---------- ------ -- ----------
    LASTLDDATE          LDDATE
    ------------------- -------------------
    RESULTTEXT
    --------------------------------------------------------------------------------
    NewTriggerPoster               EventStream          archdb
    NewTrigger                  100          1 EVENT  st   60000846
    2010/11/11 22:57:14 2010/11/11 22:58:07



    SQL> 
```
  
5.  next, make sure that your autoposter process is running by issuing this query as the oracle **trinetdb** user:   
```
    SQL> select what, job, broken from dba_jobs where what like '%PCS.DoAuto%';

    WHAT
    --------------------------------------------------------------------------------
        JOB B
    ---------- -
    PCS.DoAutoPostJobs();
            21 N


    SQL> 


    If the job had returned with a Y, that means it was broken and needs restarting:

    If it is returned with broken = 'Y' - then we need to resume this job. 

    then restart the job using this SQL as trinetdb:

    exec dbms_job.run(21);
```


### rcg reposting for subnet or event triggers <a name="rcg-repost"></a> 

So, you haven't had RCG running or you lost RCG's being created (see above) for some time and you want to get those RCG's posted so that waveforms are archived. How do you do that. Well, if you have subnet triggers and have a list of event id's then you issue a command as follows on the post processing host:   

```
    post -d archdb 60000731  EventStream archdb NewTrigger 100 1
```

where 60000731 is an example event id (event.evid) for a event.etype='st' (for subnet trigger). For actual earthquake events, (of etype='le' or other as the case may be), do this command:   

```
    post -d archdb 60000731  EventStream archdb NewEvent 100 1
```


## Triggering to get Waveforms by Hand <a name="trigger-by-hand"></a> 

The AQMS system can be triggered to store waveforms for a given time span using the newtrig.pl script (if properly configured on your host). The perl script is merely a wrapper around the Java ???NewTrig??? function built into later jiggle.jar bundles. To run it on the command line is a bit tricky since you need to escape things just right. Here is an example of it being used to trigger for an event that happened while EW (earthworm) was down:  

```
    newtrig.pl -s "2010-11-16 10:16:00.000" -l 360
```   
  
**IMPORTANT: A prior version of this script required complicated quote strings...that was fixed in 2013-14.**  
  
Note that running this requires that you have a ???NewTrig.props??? file created and placed in /app/aqms/.newtrig/NewTrig.props. Below is an example:   
  
```
    {aqms@helen:bin} more ~/.newtrig/NewTrig.props 
    
    #--- NewTrig properties ---*
    # Create a trigger event in the dbase and generate waveform requests
    # for a fixed time slice for all elegable stations in a named list.
    # 
    debug=true
    verbose=true
    #
    jasiObjectType=TRINET
    localNetCode=NM

    # ##
    # Database connection props (may differ from solution server host):
    # 
    dbTimeBase=TRUETIME
    dbWriteBackEnabled=true

    #dbaseDomain=gps.caltech.edu
    dbaseDriver=oracle.jdbc.driver.OracleDriver
    dbaseHost=helen
    dbaseName=archdb
    dbasePasswd=xxxxxxxxxxxxx
    dbasePort=1521
    dbaseUser=rtem 

    # set =false for testing
    writeReqs=true

    auth=NM
    staAuth=NMPP 
    subsource=NewTrig

    #
    # Model specific properties
    #
    model=org.trinet.jasi.NewTriggerChannelTimeWindowModel
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.debug=false
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.includeAllComponents=false
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.maxDistance=10000
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.maxChannels=9999
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.candidateListName=RCG-NM
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.requiresLocation=false
    org.trinet.jasi.NewTriggerChannelTimeWindowModel.synchListBySolution=true

    {aqms@helen:bin} 
```
  

## Archiving Waveforms and the Waveroots table <a name="arch-wfs"></a> 
  
The waveroots table in the Oracle DB is how the root directory is located for waveforms stored in the mseed archive. This is described how this is used by Allan Walter in this ???waveroots??? page.   
  
## clients of PWS have problems connecting <a name="pws-prob"></a> 
  
It is often seen in the log that the client is connecting from an invalid IP address. This is because the C++ code tries to map the IP address back to a hostname (this is known as Reverse DNS). To fix this problem, simply add any clients to the /etc/hosts table on the server running the PWS server.   
  
??? What to do with attachments on this page https://vault.gps.caltech.edu/trac/cisn/wiki/PPmaintenance#no1 ???
  



















  
