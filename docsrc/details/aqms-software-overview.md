# %System Overview {#aqms-software-overview}  
  
This section gives a broad overview of how the software works and all of the large scale components. In the AQMS system there are 2 fully redundant Real Time (RT) servers that perform automated event locations, subnet triggering, databasing and alarming. Each RT server replicates its database contents to the post processing server which contains an archival database. Because replication of databases is a key component, the Oracle database was chosen early on for its advanced capabilities in this regards. The post-processing server is also responsible for reaping the waveforms and storing them in a RAID device for later processing and archiving at a central facility. The figure below shows the basic layout of the system.  
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/cisn_core.gif">  
  

## %System Requirements  
  
The official release of AQMS that is installed at all of the Regional Seismic Networks (RSNs) is dependent on the following: 
  
*  Redhat Enterprise Linux 7  
*  Oracle 12.2.1 OR Postgres 10+  
*  Earthworm version >= 7.2   
  

## Earthworm  
  
Earthworm 7.2 (or greater) is the core of the AQMS RT system for data acquisition, locating earthquakes and performing subnet triggers. The Earthworm system is connected to the CHEETAH system by a handful of modules that send messages into CMS (hyps2ps and trig2ps) or write data from Earthworm directly to the database (ewmag2cisn). It helps if a seismic network is already configured and running Earthworm. Medium term storage (days to weeks) of waveforms in Earthworm is done via a Winston waveserver or via a wave_serverV process and this is connected to AQMS by means of the proxy wave server (pws) module.   
  
  
## %Database  
  
In the AQMS system, the database is the central repository for all station/channel configurations and the parametric results from the processing, both automated and human driven. The database for the first edition of this system is Oracle 12c. Stored procedures are used to centralize logic in the database itself and many of these are written in Java.  
  
The main reason for using Oracle is the replication capability. Each RT host has its own self-contained database snapshot of its view of the system and it is replicated to a centralized repository for post-processing and further archiving. So, the ideal system has two RT databases (rtdb and rtdb2) that provide a redundant system and each of these is replicated to a master archive database (archdb) that is backed up on a regular basis. The archdb is thus referred to as the post processing database.   
  
**Due to the prohibitive cost of Oracle license, AQMS is in the process of transitioning to using PostgreSQl. Replication is achieved using pg_logical. The database has been migrated to Postgres. Most of the software has been ported to work with both Oracle and Postgres. RSNs are actively in the process of transitioning to using Postgres.**    

  
## Real Time  
  
The RealTime (RT) hosts each have a role as the primary or shadow server and this section briefly describes the activities that take place on a RT host. For a more detailed discussion of the modules running on each RT host, refer to the RT Overview???  
  
The RT host designated as PRIMARY will be the one from which alarms are sent and event solutions and subnet triggers are managed. Each RT host is an exact copy of its sibling, so that if the PRIMARY host falls over, the SHADOW host can take over its role. Each RT host runs Earthworm, a number of RT processes for computing %Magnitude and Amplitudes, and massaging the data into the database from Earthworm. Most importantly, each RT host runs an alarming thread that can activate scripts on the PRIMARY host to start actions for each event (e.g., QDDS or EIDS notification, email alarms, ShakeMap initiation etc).   
  
### Monitoring of the system  
  
[SeisNetWatch](https://www.isti.com/products-offerings/seisnetwatch) (and Nagois, as some RSNs) are used to monitor each RT system, the EW modules, the RT modules, and the Post Processing computer and systems. Furthermore, the system can be configured to provide automatic fail-over of the RT hosts.   
  

## Post Processing  
  
One of the principal features of the AQMS system is its post processing capabilities. That is, the ability to re-pick an event, locate an event using a subnet trigger, and do further alarming and database queries. The post processing system has two major components to aid in reviewing and alarming on events, the Duty Review Pages and the Jiggle Java application.  
  
The Duty Review Pages <a href="drp.html">DRP</a> show a summary web-based view of recent events and subnet triggers and allow an operator to confirm or cancel an earthquake or make minor changes to the solution or magnitude.  
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/drp_1.gif">
  
The Jiggle application allows analysts to re-pick, pick, compute magnitudes, locations, and finalize an event by sending out new alarms to indicate the event has changed in some way.   
  
<img src="https://gitlab.com/aqms-swg/aqms-docs/-/raw/master/docsrc/images/jiggle3.gif">
  



 