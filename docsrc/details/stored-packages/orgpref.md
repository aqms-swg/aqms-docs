# ORGPREF PACKAGE {#orgpref-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE ORGPREF AUTHID CURRENT_USER AS   
--
-- Functions for determining the priority rank of a Origin.
--
-- Used in production: no, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  Java class org.trinet.jiggle.Jiggle.class (references Solution abstract method)
--  Java class org.trinet.jasi.TN.Solution.class (references abstract method)
--  Java class org.trinet.jasi.TN.SolutionTN.class (method prepared statement ref)
--  Stored Package EPREF 
--  tpp/bin/perlmodules/Event.pm ?
-- <DEPENDS ON>
--   Parametric schema tables Event, Origin, Origin, EventPrefor
--   and tables Gazetteer_Region and OrgPrefPriority
--   Stored Package EPREF installed
--     
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
    --
    -- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns max priority value for a origin described by the input values using the rule
    -- constraints found in the OrgPrefPriority table for the input region name key and datetime.
    -- Rules applied are those enabled for the specied input datetime; input datetime is the event's
    -- origin time in epoch seconds.
    -- Returns -1 if no matching priority constraints are found for the input.
    -- Example: call ORGPREF.getOriginPriority('CI',93291992392.,'H','CI','Jiggle','HYP2000',10,104.,13.,9.3,2.1,0.2,.75) into :v_priority;
    FUNCTION getOriginPriority( 
                             p_region      Gazetteer_Region.name%TYPE,
                             p_datetime    Origin.datetime%TYPE,
                             p_otype       Origin.type%TYPE,
                             p_auth        Origin.auth%TYPE,
                             p_subsource   Origin.subsource%TYPE,
                             p_algo        Origin.algorithm%TYPE,
                             p_ndef        Origin.ndef%TYPE,
                             p_gap         Origin.gap%TYPE,
                             p_dist        Origin.distance%TYPE,
                             p_depth       Origin.depth%TYPE,
                             p_erhor       Origin.erhor%TYPE,
                             p_wrms        Origin.wrms%TYPE,
                             p_quality     Origin.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- As above, except only use the origin priority rules enabled for the current system time.
    FUNCTION getOriginPriority( 
                             p_region      Gazetteer_Region.name%TYPE,
                             p_otype       Origin.type%TYPE,
                             p_auth        Origin.auth%TYPE,
                             p_subsource   Origin.subsource%TYPE,
                             p_algo        Origin.algorithm%TYPE,
                             p_ndef        Origin.ndef%TYPE,
                             p_gap         Origin.gap%TYPE,
                             p_dist        Origin.distance%TYPE,
                             p_depth       Origin.depth%TYPE,
                             p_erhor       Origin.erhor%TYPE,
                             p_wrms        Origin.wrms%TYPE,
                             p_quality     Origin.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns max priority value for the origin described by input values.
    -- Input datetime is the event's origin time in epoch seconds; rules used are
    -- those enabled for this datetime.  Input lat, lon should be inside one
    -- or more regions defined in Gazetteer_Region table that also have priority
    -- constraints declared in the OrgPrefPriority table, otherwise rules matching
    -- OrgPrefPriority.region_name = 'DEFAULT' are used.
    -- Returns -1 if lat/lon are NULL or no matching priority is found in table.
    -- Example: call ORGPREF.getOriginPriority('CI',93291992392.,36.,-117.9,'H','CI','Jiggle','HYP2000',10,104.,13.,9.3,2.1,0.2,.75) into :v_priority;
    FUNCTION getOriginPriority(
                             p_datetime    Origin.datetime%TYPE,
                             p_lat         Origin.lat%TYPE,
                             p_lon         Origin.lat%TYPE,
                             p_otype       Origin.type%TYPE,
                             p_auth        Origin.auth%TYPE,
                             p_subsource   Origin.subsource%TYPE,
                             p_algo        Origin.algorithm%TYPE,
                             p_ndef        Origin.ndef%TYPE,
                             p_gap         Origin.gap%TYPE,
                             p_dist        Origin.distance%TYPE,
                             p_depth       Origin.depth%TYPE,
                             p_erhor       Origin.erhor%TYPE,
                             p_wrms        Origin.wrms%TYPE,
                             p_quality     Origin.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- As above, except only use the origin priority rules enabled for the current system time.
    FUNCTION getOriginPriority(
                             p_lat         Origin.lat%TYPE,
                             p_lon         Origin.lat%TYPE,
                             p_otype       Origin.type%TYPE,
                             p_auth        Origin.auth%TYPE,
                             p_subsource   Origin.subsource%TYPE,
                             p_algo        Origin.algorithm%TYPE,
                             p_ndef        Origin.ndef%TYPE,
                             p_gap         Origin.gap%TYPE,
                             p_dist        Origin.distance%TYPE,
                             p_depth       Origin.depth%TYPE,
                             p_erhor       Origin.erhor%TYPE,
                             p_wrms        Origin.wrms%TYPE,
                             p_quality     Origin.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Wrapper around above. Does database query lookup for additional function parameters.
    -- Returns max priority value for the origin matching Origin.orid.
    -- Input datetime is the event's origin time in epoch seconds; rules used are
    -- those enabled for this datetime. Input lat, lon should be inside one
    -- or more regions defined in Gazetteer_Region table that also have priority
    -- constraints declared in the OrgPrefPriority table, otherwise rules matching
    -- OrgPrefPriority.region_name = 'DEFAULT' are used.
    -- Returns -1 if lat/lon are NULL or no matching priority is found in table.
    FUNCTION getOriginPriority(p_orid Origin.orid%TYPE, p_datetime Origin.datetime%TYPE,
                            p_lat Origin.lat%TYPE, p_lon Origin.lon%TYPE) RETURN NUMBER;
    --
    -- Wrapper around above, instead uses origin priority rules enabled for current system time.
    FUNCTION getOriginPriority(p_orid Origin.orid%TYPE, p_lat Origin.lat%TYPE, p_lon Origin.lon%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return max priority value of the origin matching input Origin.orid.
    -- Convenience wrapper around above getOriginPriority function, uses origin's 
    -- datetime, lat, and lon for the input arguments.
    -- Returns -1 if no matching priority row constraints are found.
    FUNCTION getOriginPriority(p_orid Origin.orid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the max priority value for the origin whose Origin.orid=Event.prefor
    -- for the Event row matching the input evid value.
    -- Convenience wrapper around above getOriginPriority function, uses prefor's 
    -- datetime, lat, and lon for the input arguments.
    -- Returns -1 if no matching priority row constraints are found.
    -- Example: call ORGPREF.getOriginPriorityOfEvent(1111) into :v_priority;
    FUNCTION getOriginPriorityOfEvent(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the max priority value for the origin described by the Origin
    -- row whose key equals the orid value from the EventPrefor table row 
    -- whose key matches both the input evid value and origin type.
    -- Convenience wrapper around above getOriginPriority function, uses origin's 
    -- datetime, lat, and lon for the input arguments.
    -- This origin may or may not be the 'event' preferred origin (Event.prefor)
    -- Returns -1 if no matching priority row constraints are found.
    -- Example: call ORGPREF.getOriginPriorityOfPreforType(1111,'H') into :v_priority;
    FUNCTION getOriginPriorityOfPreforType(p_evid Event.evid%TYPE, p_otype Origin.type%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    --
    -- Return the orid of the highest priority origin. If equal priorities,
    -- return new orid (2nd argument), when both priority's are < 0 
    -- (both failed rules) return the old orid (1st argument).
    -- Example: call ORGPREF.highest_priority(1111, 2222) into :v_orid;
    FUNCTION highest_priority(p_old_orid Origin.orid%TYPE, p_new_orid Origin.orid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Calculates a maximum priority for every origin whose orid is associated
    -- with the input p_evid in the EventPrefor table.
    -- Set the row's 'Event.prefor' to the orid having the highest calculated priority 
    -- Commits the transaction if p_commit =1.
    -- Returns:
    --    > 0 updated, prefor value changed to the returned number
    --    < 0 updated, prefor value did NOT change, return is -prefor
    --      0 prefor could not be updated; no origins, bad evid, etc.
    -- Example: call ORGPREF.setPreforOfEvent(1111, 1) into :v_orid;
    FUNCTION setPreforOfEvent(p_evid Event.evid%TYPE, p_commit PLS_INTEGER) RETURN NUMBER;
    --
    -- Wrapper around above, with p_commit=1, commit transaction if update is successful.
    FUNCTION setPreforOfEvent(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns Event.prefor value of the Event table row whose key matches the input evid.
    -- NOTE: the event prefmag's orid DOES NOT HAVE TO EQUAL this prefor.
    -- Returns -1 if Event.prefor is NULL.
    FUNCTION getPreforOfEvent(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the Origin.orid of the origin with the highest priority
    -- associated with Event in the EventPrefor table.
    -- Returns 0 if no origin is found in table that this satisfies priority rules.
    -- Example: call ORGPREF.bestOridOfEventPrefor(1111) into :v_orid;
    FUNCTION bestOridOfEventPrefor(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    --
    -- Returns the Origin.orid of the origin with the highest priority
    -- associated with Event in the Origin table, regardless of origin type.
    -- Returns 0 if no origin satisfies priority rules.
    -- Note: Does not query EventPrefor table.
    -- Example: call ORGPREF.bestOridForOriginPref(1111) into :v_orid;
    FUNCTION bestOridForOriginPref(p_evid Event.evid%TYPE) RETURN NUMBER;
    --
    -- Returns the Origin.orid of the origin with the highest priority
    -- associated with Event in the Origin table and having the input origin type.
    -- Returns 0 if no matching origin for specified type satisfies priority rules.
    -- Note: Does not query EventPrefor table.
    -- Example: call ORGPREF.bestOridForOriginPref(1111, 'H') into :v_orid;
    FUNCTION bestOridForOriginPref(p_evid Event.evid%TYPE, p_otype Origin.type%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    --
    -- Return spaced delimited list of the region names in GAZETTEER_REGION table that are 
    -- possible for OrgPrefPriority table row key elements.
    FUNCTION get_region_names RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
    -- For debugging
    -- Return string describing the data from the Origin row whose key matches the input orid.
    -- that are used to determine origin priority when compared to the rule criteria
    -- of the OrgPrefPriority table.
    --  Returns NULL if no matching Origin row key exists.
    FUNCTION describeOrigin(p_orid Origin.orid%TYPE) RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- For debugging
    -- Write to server output a list of region names initialized from GAZETTEER_REGION table.
    PROCEDURE dump_region_names;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
END orgpref;
```