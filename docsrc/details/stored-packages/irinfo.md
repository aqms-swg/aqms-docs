# IRINFO PACKAGE {#irinfo-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE irInfo AS
-- NOTE:
--
-- Contact: Stephane Zuzlewski - UCB
--
-- Currently used in production: yes
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   TMTS2
--
-- <DEPENDS ON>
--   - NCEDC instrument responses schema Channel_Data, D_Unit, Netmag, Sensitivity,
--     Poles_Zeros & PZ_Data tables to get db data for a given SNCL & time
--   - Java class irInfo.getSimpleIR.class (loaded onto server)
--   - User defined types:
--       -- Complex object
--       CREATE OR REPLACE TYPE T_COMPLEX AS OBJECT
--       (
--        real           NUMBER,
--        imag           NUMBER
--       );
--
--       -- Array of complex objects
--       CREATE OR REPLACE TYPE T_ARRAY_COMPLEX IS VARRAY(32) OF T_COMPLEX;
--
--       -- Simple response object
--       CREATE OR REPLACE TYPE T_IRINFO AS OBJECT
--       (
--        ondate         DATE,
--        offdate        DATE,
--        units          VARCHAR2(32),
--        gain           NUMBER,
--        nb_poles       INTEGER,
--        nb_zeros       INTEGER,
--        poles          T_ARRAY_COMPLEX,
--        zeros          T_ARRAY_COMPLEX
--       );
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Table of numbers
    TYPE T_TABLE_NUMBER IS TABLE OF NUMBER INDEX BY BINARY_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return T_IRINFO structure containing poles & zeros, gain & units information
    FUNCTION getSimpleIR (net VARCHAR2, sta VARCHAR2, cha VARCHAR2, loc VARCHAR2,
                          stime DATE, etime DATE, debug NUMBER)
    RETURN T_IRINFO;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return poles & zeros, gain & units information
    FUNCTION getIR (net IN VARCHAR2, sta IN VARCHAR2, cha IN VARCHAR2, loc IN VARCHAR2,
                    stime IN OUT DATE, etime IN OUT DATE, units OUT VARCHAR2, gain OUT NUMBER,
                    nb_poles OUT NUMBER, nb_zeros OUT NUMBER, poles_real OUT T_TABLE_NUMBER,
                    poles_imag OUT T_TABLE_NUMBER, zeros_real OUT T_TABLE_NUMBER,
                    zeros_imag OUT T_TABLE_NUMBER)
    RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------

END irInfo;
```
  
## Examples  

```


SET SERVEROUTPUT ON;
DECLARE x T_IRINFO;
BEGIN
        x := irinfo.getSimpleIR('CI','BAK','BHZ','  ',TO_DATE('2008/01/01 00:00:00','yyyy/mm/dd hh24:mi:ss'),
                                TO_DATE('2008/01/02 00:00:00','yyyy/mm/dd hh24:mi:ss'),1);

        DBMS_OUTPUT.PUT_LINE('Ondate  : ' || TO_CHAR(x.ondate,'yyyy/mm/dd hh24:mi:ss'));
        DBMS_OUTPUT.PUT_LINE('Offdate : ' || TO_CHAR(x.offdate,'yyyy/mm/dd hh24:mi:ss'));
        DBMS_OUTPUT.PUT_LINE('Units   : ' || x.units);
        DBMS_OUTPUT.PUT_LINE('Gain    : ' || x.gain);

        DBMS_OUTPUT.PUT_LINE('# Poles : ' || x.nb_poles);
        FOR i IN 1..x.nb_poles LOOP
          DBMS_OUTPUT.PUT_LINE('Pole #' || i || ' : ' || x.poles(i).real || '  ' || x.poles(i).imag);
        END LOOP;

        DBMS_OUTPUT.PUT_LINE('# Zeros : ' || x.nb_zeros);
        FOR i IN 1..x.nb_zeros LOOP
          DBMS_OUTPUT.PUT_LINE('Zero #' || i || ' : ' || x.zeros(i).real || '  ' || x.zeros(i).imag);
        END LOOP;
END;

SET SERVEROUTPUT ON;
DECLARE
        x NUMBER;
        stime DATE := TO_DATE('2008/01/01 00:00:00','yyyy/mm/dd hh24:mi:ss');
        etime DATE := TO_DATE('2008/01/02 00:00:00','yyyy/mm/dd hh24:mi:ss');
        units VARCHAR2(10);
        gain NUMBER;
        nb_poles NUMBER;
        nb_zeros NUMBER;
        poles_real IRINFO.T_TABLE_NUMBER;
        poles_imag IRINFO.T_TABLE_NUMBER;
        zeros_real IRINFO.T_TABLE_NUMBER;
        zeros_imag IRINFO.T_TABLE_NUMBER;
BEGIN
        x := irinfo.getIR('CI','BAK','BHZ','  ', stime, etime, units, gain, nb_poles, nb_zeros,
                          poles_real, poles_imag, zeros_real, zeros_imag);

        if x = 0 THEN
                DBMS_OUTPUT.PUT_LINE('No response available.');
        ELSE
                DBMS_OUTPUT.PUT_LINE('Ondate  : ' || TO_CHAR(stime,'yyyy/mm/dd hh24:mi:ss'));
                DBMS_OUTPUT.PUT_LINE('Offdate : ' || TO_CHAR(etime,'yyyy/mm/dd hh24:mi:ss'));
                DBMS_OUTPUT.PUT_LINE('Units   : ' || units);
                DBMS_OUTPUT.PUT_LINE('Gain    : ' || gain);

                DBMS_OUTPUT.PUT_LINE('# Poles : ' || nb_poles);
                FOR i IN 1..nb_poles LOOP
                        DBMS_OUTPUT.PUT_LINE('Pole #' || i || ' : ' || poles_real(i) || '  ' || poles_imag(i));
                END LOOP;

                DBMS_OUTPUT.PUT_LINE('# Zeros : ' || nb_zeros);
                FOR i IN 1..nb_zeros LOOP
                        DBMS_OUTPUT.PUT_LINE('Zero #' || i || ' : ' || zeros_real(i) || '  ' || zeros_imag(i));
                END LOOP;
        END IF;
END;
```