# GEO_REGION PACKAGE {#geo_region-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE geo_region AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: geo_region_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Used in production: yes, UTC version
--
-- Function/Procedures for testing whether geographic point lies
-- inside or outside a polygonal region specified by lat,lon coordinates.
--
-- <LIST DEPENDENT APPLICATIONS HERE>:
--   Java org.trinet.jasi.TN.SolutionTN using EventSelectionProperties
--
-- <DEPENDS ON>
--   refs GAZETTEER_REGION, GAZETTEERPT tables objects 
--   Oracle user defined datatype LATLON, COORDINATES in PUBLIC schema (synonym ok)
--
-- Example: SELECT evid, orid, lat,lon FROM event,origin
-- Example:  WHERE orid=prefor and datetime > truetime.putEpoch(truetime.string2true('2004/01/30 00:00:00'),'UTC')
-- Example:        and GEO_REGION.inside_border('AirportLake', lat, lon);
--
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
  -- Sets the coordinates bounding the region used by function inside_border(lat,lon)
  -- The default coordinates are also set to the values stored in db for a named border
  -- by invocation of the function: inside_border(name, lat, lon).
  -- By default border is a rectangle (SoCal) defined by the coordinate values of:
  --   LATLON(30.,-122.),LATLON(38.,-122.),LATLON(38.,-114.),LATLON(30.,-114.)
  -- Input COORDINATES count must define a polygon, i.e. at least 3 LATLON points.
  PROCEDURE set_default_coord(p_coord COORDINATES);
  --
  -- Print the current values of the border coordinates used by function inside_border(lat,lon)
  PROCEDURE print_default_coord;
  --
  -- Print coordinates for a named region stored in GAZETTER_REGION table.
  PROCEDURE print_coord(p_coord COORDINATES);
  --
  -- Return border coordinates for a named region stored in GAZETTER_REGION table.
  FUNCTION get_coord(p_name VARCHAR2) RETURN COORDINATES;
  --
  -- Returns 0 if outside, 1 if inside or on, the defined region border.
  -- The default coordinates are set to the values stored in db for a named border
  -- Example: call GEO_REGION.inside_border('NorthridgeAftershocks', 34., -118.) into :v_status;
  FUNCTION inside_border(p_name VARCHAR2, p_lat NUMBER, p_lon NUMBER) RETURN NUMBER ;
  PRAGMA RESTRICT_REFERENCES (inside_border, WNDS, WNPS, RNPS, TRUST);
  --
  -- Returns 0 if outside, 1 if inside, 2 if on the defined region border.
  -- The default coordinates are not set to the input values.
  FUNCTION inside_border(p_coord COORDINATES, p_lat NUMBER, p_lon NUMBER) RETURN NUMBER DETERMINISTIC;
  PRAGMA RESTRICT_REFERENCES (inside_border, WNDS, RNDS, WNPS, RNPS, TRUST);
  --
  -- Returns 0 if outside, 1 if inside or on, the defined region border.
  -- Use the border coordinates set by either set_default_coord(coord) or inside_border(name,lat,lon).
  FUNCTION inside_border(p_lat NUMBER, p_lon NUMBER) RETURN NUMBER DETERMINISTIC;
  PRAGMA RESTRICT_REFERENCES (inside_border, WNDS, RNDS, WNPS, RNPS, TRUST);
  --
  -- Creates a named region in the gazetteer_region table
  -- Name is primary key to table, so must not already exist or exception is thrown
  -- Returns 1 on success, else -1.
  FUNCTION create_region(p_name VARCHAR2, p_coord COORDINATES) RETURN PLS_INTEGER;
--
--
-- Return a LATLON object containing the latitude and longitude of an entry in gazetteerpt table
-- whose name matches the input string.
  FUNCTION get_location_of(p_name VARCHAR2) RETURN LATLON;
  PRAGMA RESTRICT_REFERENCES (get_location_of, WNDS, WNPS, RNPS, TRUST);
--
-- Returns name of the gazetteer_region associated with the specified p_grpid in the 
-- assoc_region_group table in which the input location is located, and in the
-- case of multiple overlapping region boundaries within the same group, the one with
-- the the lowest rg_order value.
-- Returns NULL if input lat,lon point is outside of all regions associated with
-- input group id.
  FUNCTION nameOfRegionInGroup(p_lat NUMBER, p_lon NUMBER, p_grpid VARCHAR2) RETURN VARCHAR2;
--
-- Returns auth of the gazetteer_region associated with the specified p_grpid in the 
-- assoc_region_group table in which the input location is located, and in the
-- case of multiple overlapping region boundaries within the same group, the one with
-- the the lowest rg_order value.
-- Returns NULL if input lat,lon point is outside of all regions associated with
-- input group id.
  FUNCTION authOfRegionInGroup(p_lat NUMBER, p_lon NUMBER, p_grpid VARCHAR2) RETURN VARCHAR2;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END geo_region;
```