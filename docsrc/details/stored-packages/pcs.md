# PCS PACKAGE {#pcs-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE TYPE id_table AS TABLE OF NUMBER(15)
/ 
--
CREATE OR REPLACE PUBLIC SYNONYM id_table FOR id_table;
GRANT EXECUTE ON id_table TO PUBLIC;
--
-- Admin test table for id posting
-- DROP TABLE ids2post;
-- CREATE TABLE ids2post (id NUMBER(15));
-- CREATE OR REPLACE PUBLIC SYNONYM ids2post FOR ids2post;
--
-- NOTE DEFINER rights why?
--  Centralizes PCS (tables) reference to those of DEFINER's 
--
CREATE OR REPLACE PACKAGE pcs AUTHID DEFINER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: pcs_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- Functions/Procedures in this package for applications that utilize 
-- process event state processing tables PCS_SIGNAL, PCS_STATE, PCS_TRANSITION 
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  PACKAGE DB_CLEANUP
--  PACKAGE EPREF
--  Java org.trinet.pcs.AbstractProcessingController.class and subclasses like:
--  Java org.trinet.pcs.HypoMagProcessingController.class 
--  Java org.trinet.pcs.RcgProcessingModule.class
--  Java org.trinet.jasi.TN.SolutionTN.class
--
--  /tpp/bin/next
--  /tpp/bin/post
--  /tpp/bin/postFromList
--  /tpp/bin/result
--  /tpp/bin/resultAll
--  /tpp/bin/resultByRank
--  /tpp/bin/unpost
--
--  /tpp/bin/perlmodules/PCS.pm
--
-- <DEPENDS ON>
--  PCS_SIGNAL, PCS_STATE, PCS_TRANSITION, and AUTOPOSTER tables 
-- ------------------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------------------
-- Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ------------------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------------------
-- Return 1 if RT_ROLE table primary_system value is true, else 0
   FUNCTION amPrimary RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------------------
--
-- Schedule an event for state processing.
-- Creates or updates a row in the PCS_STATE table whose key matches the input parameters.
-- If input result is non-zero, all state transitions for input state result will occur
-- after row insert. Does a database commit.
-- Returns 1 on success, <= 0 failure.
-- Example: call PCS.putState('TTP', 'TEST', 1, 'fubar', 100, 1) into :v_istatus;
    FUNCTION putState(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_id NUMBER,
        p_state VARCHAR2,
        p_rank NUMBER,
        p_result NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- DEPRECATED function is an alias for function post_id with same parameters.
    FUNCTION putState(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_id NUMBER,
        p_state VARCHAR2,
        p_rank NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Schedule an event for state processing with result code 0.
-- Inserts new row in the PCS_STATE table whose key matches the input parameters, only
-- if row matching key does not exist, otherwise a no-op, but the matching existing
-- row could have non-zero result and different rank.
-- Since result code is 0, no row state transitions will occur.
-- Returns 1 for success, 0 for a no-op, -1 for error exception.
-- Does a database commit. 
-- Example: call PCS.post_id('TTP', 'TEST', 1, 'fubar', 100) into :v_istatus;
  FUNCTION post_id(p_group VARCHAR2,
                   p_source VARCHAR2,
                   p_id NUMBER,
                   p_state VARCHAR2,
                   p_rank NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Schedule an event for state processing with result code 0.
-- Inserts new row in the PCS_STATE table whose key matches the input parameters, only
-- if row matching key does not exist, otherwise a no-op, but the matching existing
-- row could have non-zero result and different rank.
-- Commits DML if successful when input p_commit > 0. This function is
-- No row state transition logic since row result code set to 0.
-- Returns 1 for success, 0 for a no-op, -1 for error exception.
-- Example: call PCS.post_id('TTP', 'TEST', 1, 'fubar', 100) into :v_istatus;
  FUNCTION post_id(p_group VARCHAR2,
                   p_source VARCHAR2,
                   p_id NUMBER,
                   p_state VARCHAR2,
                   p_rank NUMBER,
                   p_commit NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Loop over array invoking post_id(...) function for each id
-- found in input array. Commits each id row transaction.
-- Result code is set 0 so no state transitions occur.
  FUNCTION post_ids(p_ids   ID_TABLE,
                    p_group VARCHAR2,
                    p_source VARCHAR2,
                    p_state VARCHAR2,
                    p_rank NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Schedules all ids in input table for state processing by doing a 
-- FORALL bulk insert without a check for existance of rows that
-- matching inputs, throws exception if inputs match row in table.
-- Each row result code is set to 0, so no state transition logic.
-- Commits all rows after successful bulk insert.
  FUNCTION bulk_post(p_ids   ID_TABLE,
                     p_group VARCHAR2,
                     p_source VARCHAR2,
                     p_state VARCHAR2,
                     p_rank NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Ids are selected from named input id table which should contain a
-- column named "id" (number(15)).
-- Ids from this column are scheduled for state processing by doing a 
-- FORALL bulk insert without a check for existance of rows that
-- matching inputs, throws exception if inputs match row in table.
-- Each row result code is set to 0, so no state transition logic.
-- Commits all rows after successful bulk insert.
  FUNCTION bulk_post_id_table( p_table_name VARCHAR2,
                          p_group VARCHAR2,
                          p_source VARCHAR2,
                          p_state VARCHAR2,
                          p_rank NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Ids are selected from named input id table and column.
-- Ids from this column are scheduled for state processing by doing a 
-- FORALL bulk insert without a check for existance of rows that
-- matching inputs, throws exception if inputs match row in table.
-- Each row result code is set to 0, so no state transition logic.
-- Commits all rows after successful bulk insert.
  FUNCTION bulk_post_id_table( p_table_name VARCHAR2,
                          p_table_column VARCHAR2,
                          p_group VARCHAR2,
                          p_source VARCHAR2,
                          p_state VARCHAR2,
                          p_rank NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Delete all PCS_STATE table entries whose id is the input id.
-- Returns number of rows deleted on success, < 0 failure.
-- Example: call PCS.unpost(1) into :v_istatus;
    FUNCTION unpost( p_id NUMBER ) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Delete all PCS_STATE table entries matching input.
-- Returns number of rows deleted on success, < 0 failure.
-- Example: call PCS.unpost(1) into :v_istatus;
    FUNCTION unpost( p_group VARCHAR2,
                     p_source VARCHAR2,
                     p_state VARCHAR2 ) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Updates result column of the PCS_STATE table row whose key matches input parameters
-- if its existing value is NULL or is not equal to the input result.
-- If the id is posted for multiple ranks, results ONLY row with highest posted rank.
-- UNLIKE putResult this FUNCTION DOES NOT DO STATE ROW TRANSITIONS. 
-- The appropriate transitions(...) function can be subsequently
-- invoked to to force state transitions, if any.
-- Returns 1 if a matching row is found whose result is different from input,
--  2 if matching row is found have the input result code (a no-op) and
--  0 if no matching row is found. Returns a value < 0 for an error exception.
-- Example: call PCS.result_id('TTP', 'TEST', 1, 'fubar', 1) into :v_istatus; 
  FUNCTION result_id(p_group VARCHAR2,
                  p_source VARCHAR2,
                  p_id NUMBER,
                  p_state VARCHAR2,
                  p_result NUMBER) RETURN PLS_INTEGER;
--
-- As above, except matching row's rank must MATCH input rank.
  FUNCTION result_id(p_group VARCHAR2,
                  p_source VARCHAR2,
                  p_id NUMBER,
                  p_state VARCHAR2,
                  p_rank NUMBER,
                  p_result NUMBER) RETURN PLS_INTEGER;
--
-- As above, but results ALL posted ranks for same id.
  FUNCTION result_id_all(p_group VARCHAR2,
                  p_source VARCHAR2,
                  p_id NUMBER,
                  p_state VARCHAR2,
                  p_result NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Updates result column of the PCS_STATE table row whose key matches input parameters
-- when its existing value is NULL or not equal to the input result.
-- If an id is posted for multiple ranks, results ONLY the row with highest posted rank.
-- Does transitions to new states if an update is done.
-- Returns 1 if matching row is found whose result is different from input,
--  2 if all matching row is found have input result code (a no-op) and
--  0 if no matching row is found. Returns a value < 0 for an error exception.
-- Example: call PCS.putResult('TTP', 'TEST', 1, 'fubar', 1) into :v_istatus; 
    FUNCTION putResult(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_id NUMBER,
        p_state VARCHAR2,
        p_result NUMBER) RETURN PLS_INTEGER;
--
-- As above, except matching row's rank must MATCH input rank.
    FUNCTION putResult(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_id NUMBER,
        p_state VARCHAR2,
        p_rank NUMBER,
        p_result NUMBER) RETURN PLS_INTEGER;
--
-- As above, but results ALL posted ranks for same id.
    FUNCTION putResultAll(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_id NUMBER,
        p_state VARCHAR2,
        p_result NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Useful for forcing transitions for all postings that match input posting description.
-- Only updates row whose existing result is NULL or not equal to the input.
-- Returns count of all id entries effected, or value < 0 for error.
-- Example: call PCS.putResultAll('TPP','TEST', 'fubar', 1) into :v_istatus;
    FUNCTION putResultAll(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_state VARCHAR2,
        p_result NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
--
-- Function DEPRECATED, instead use function next_id with same parameters.
    FUNCTION getNext(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_state VARCHAR2) RETURN NUMBER;
-- ------------------------------------------------------------------------------------------
--
-- Return the next event id scheduled for the state processing described by input.
-- Returns 0 if no id is scheduled; returns -1 for error exception. 
-- Example: call PCS.next_id('TTP', 'TEST', 'fubar') into :v_id; 
  FUNCTION next_id(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2) RETURN NUMBER;
--
-- Return the next event id scheduled for the state processing described by input.
-- Returns 0 if no id is scheduled; returns -1 for error exception. 
-- Returns only evids that have been posted 'p_delay' seconds or longer.
-- Example: call PCS.next_id('TTP', 'TEST', 'fubar') into :v_id; 
  FUNCTION next_id(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2, p_delay NUMBER) RETURN NUMBER;
-- ------------------------------------------------------------------------------------------
--
-- Return status of row entry in PCS_STATE table, if any, that matches input parameters.
--         -1  id is not posted for state,
--          0  id is processed with result,
--          1  id is posted but unprocessed,
--         >1  id is rank blocked can't process for input state
-- Example: call PCS.run_status('TTP', 'TEST', 'fubar', 1) into :v_istatus;
    FUNCTION run_status(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2, p_id NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Return result value found in PCS_STATE row whose key matches input row key, return NULL if no matching row.
-- Example: call PCS.result_of('TTP', 'TEST', 'fubar', 1) into :v_istatus;
    FUNCTION result_of(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2, p_id NUMBER) RETURN NUMBER;
-- ------------------------------------------------------------------------------------------
--
-- Deletes all PCS_STATE rows matching "old" state values whatever rank, and "insert" new state entries for same ids into table.
-- Returns count of entries effected, or value < 0 for error.
-- Example: call PCS.recycle('TPP','TEST','fubar', 1, 'TPP','TEST', 'mung', 100) into :v_istatus;
    FUNCTION recycle(p_groupOld VARCHAR2, p_sourceOld VARCHAR2, p_stateOld VARCHAR2, p_result NUMBER,
                     p_groupNew VARCHAR2, p_sourceNew VARCHAR2, p_stateNew VARCHAR2, p_rank NUMBER) RETURN PLS_INTEGER;
--
-- Like above except processes only those old rows whose old rank matches non-null input p_rankOld.
    FUNCTION recycle(p_groupOld VARCHAR2, p_sourceOld VARCHAR2, p_stateOld VARCHAR2, p_result NUMBER, p_rankOld NUMBER, 
                     p_groupNew VARCHAR2, p_sourceNew VARCHAR2, p_stateNew VARCHAR2, p_rank NUMBER) RETURN PLS_INTEGER;
--
-- Delete PCS_STATE rows matching "old" state values and result, whatever rank, but without any new state to post.
-- Returns count of entries effected, or value < 0 for error.
-- Example: call PCS.recycle('TPP','TEST','fubar', 0) into :v_istatus;
    FUNCTION recycle(p_groupOld VARCHAR2, p_sourceOld VARCHAR2, p_stateOld VARCHAR2, p_result NUMBER) RETURN PLS_INTEGER;
--
-- Like above except deletes only those old rows whose old rank matches non-null input p_rankOld.
    FUNCTION recycle(p_groupOld VARCHAR2, p_sourceOld VARCHAR2, p_stateOld VARCHAR2, p_result NUMBER, p_rankOld NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
--
-- Add a PCS_TRANSITION table row for the processing result to automatically recycle state processing result to the new specification.
    FUNCTION put_trans(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2, p_result NUMBER,
                       p_groupNew VARCHAR2, p_sourceNew VARCHAR2, p_stateNew VARCHAR2, p_rankNew NUMBER,
                       p_resultNew NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Delete the PCS_TRANSITION table row(s) matching the old state result and new state specification.
-- Using a NULL value in the new specification short-circuits the matching of new state rows, that is,
-- an input of p_groupNew=NULL delete all PCS_TRANSITION table rows for old result. 
    FUNCTION del_trans(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2, p_result NUMBER,
                       p_groupNew VARCHAR2 DEFAULT NULL, p_sourceNew VARCHAR2 DEFAULT NULL,
                       p_stateNew VARCHAR2 DEFAULT NULL, p_resultNew NUMBER DEFAULT NULL) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
--
-- Posts ids to new states as mapped by PCS_TRANSITION rows
-- Returns count of entries effected, or value < 0 for error.
-- Example: call PCS.transition() into :v_istatus;
-- Usually invoked by installing a trigger on PCS_STATE table.
    FUNCTION transition RETURN PLS_INTEGER;
--
-- Posts ids to new states as mapped by PCS_TRANSITION rows
-- Only those postings with matching group and source attributes.
-- Returns count of entries effected, or value < 0 for error.
-- Example: call PCS.transition('TPP', 'TEST') into :v_istatus;
    FUNCTION transition(p_group VARCHAR2, p_source VARCHAR2) RETURN PLS_INTEGER;
--
-- Posts ids to new states as mapped by PCS_TRANSITION rows
-- Only those postings with matching group, source, and state attributes.
-- Returns count of entries effected, or value < 0 for error.
-- Example: call PCS.transition('TPP', 'TEST', 'fubar') into :v_istatus;
    FUNCTION transition(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2) RETURN PLS_INTEGER;
--
-- Posts input id to new states as mapped by PCS_TRANSITION rows
-- that match input group, source, and state attributes.
-- Returns count of entries effected, or value < 0 for error.
-- Example: call PCS.transition('TPP', 'TEST', 'fubar') into :v_istatus;
    FUNCTION transition(p_group VARCHAR2, p_source VARCHAR2, p_state VARCHAR2, p_id NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Run all defined autopost jobs one time. Autoposter jobs are defined int he Autoposter
-- Table. This procedure is typically run FROM a "job" that runs at short intervals.
-- See: /home/tpp/src/org/trinet/storedprocs/PCS/CreateProcessControlTables.sh
    PROCEDURE DoAutoPostJobs;
    PROCEDURE DoAutoPostShadowJobs;
-- ------------------------------------------------------------------------------------------
--
-- Posts all evids matching input instanceName from AutoPoster table
-- that have changed lddate of either event/origin row type since
-- last posting job was run.
    PROCEDURE AutoPostNew(p_instanceName VARCHAR2);
    PROCEDURE AutoPostShadow(p_instanceName VARCHAR2);
-- ------------------------------------------------------------------------------------------
--
--Post the Evids that result FROM the given SELECT statment
--The SELECT MUST have the form:
-- 'SELECT Event.evid, Event.lddate FROM Event... whatever'
--For examples, see above.
--This uses Dynamic DML.
    PROCEDURE postEvidsFromStatement(
        p_statement VARCHAR2,
        p_instanceName VARCHAR2);
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing to log the start of state processing 
-- Creates a row in PCS_SIGNAL table, returns a unique table row key id.
-- Example: call PCS.signal_start('TPP', 'TEST', 'hypomag', 'HYP2000', 'iron', 'kate') into :v_pid;
    FUNCTION signal_start(
        p_group VARCHAR2,
        p_source VARCHAR2,
        p_state VARCHAR2,
        p_algorithm VARCHAR2,
        p_userhost VARCHAR2,
        p_username VARCHAR2) RETURN NUMBER;
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing to log the end of state processing 
-- Updates existing row in PCS_SIGNAL table with SYS_EXTRACT_UTC(SYSTIMESTAMP) time and message "STOPPED".
-- User must have first invoked signal_start(...) to get the value of the input id.
-- Example: call PCS.signal_stop(101) into :v_istatus;
    FUNCTION signal_stop(p_id NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing to log the end of state processing 
-- Updates existing row in PCS_SIGNAL table with SYS_EXTRACT_UTC(SYSTIMESTAMP) time and message string.
-- User must have first invoked signal_start(...) to get the value of the input id.
-- Example: call PCS.signal_stop(101, 'TOTAL COUNT = 1023') into :v_istatus;
    FUNCTION signal_stop(p_id NUMBER, p_message VARCHAR2) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing to stop further state processing 
-- User must have first invoked signal_start(...) to get the value of the input id.
-- Stop signal is a non-null stopdate value in the PCS_SIGNAL table row found for p_id.
-- Return 0, stop is not signalled; return 1, stop is signalled.
-- Example: call PCS.has_stop_signal(101) into :v_istatus;
    FUNCTION has_stop_signal(p_id NUMBER) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing after invoking signal_start(...) and
-- signal_stop(...), returns total processing time elapsed between start and stop. 
-- User must have first invoked signal_start(...) to get the value of the input id.
-- Example: call PCS.run_time(101) into :v_time;
    FUNCTION run_time(p_id NUMBER) RETURN NUMBER;
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing to update an application's entry
-- in the PCS_SIGNAL table that was created by the invocation of signal_start(...).
-- Updates table row matching input row id with a text message indicating
-- application processing status or other info.
-- User must have first invoked signal_start(...) to get the value of the input id.
-- Example: call PCS.signal_message(101, 'calculating Md for evid') into :v_istatus;
    FUNCTION signal_message(
        p_id NUMBER,
        p_message VARCHAR2) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------------------
--
-- Used by applications doing state processing to delete an application's entry
-- in the PCS_SIGNAL table that was created by the invocation of signal_start(...).
-- User must have first invoked signal_start(...) to get the value of input id.
-- Example: call PCS.delete_signal(101) into :v_istatus;
    FUNCTION delete_signal(p_id NUMBER) RETURN PLS_INTEGER;
END pcs;
```