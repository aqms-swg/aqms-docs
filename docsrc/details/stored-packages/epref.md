# EPREF PACKAGE {#epref-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE epref AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: epref_pkg.sql 7906 2016-03-19 20:09:09Z renate $';
-- Functions/Procedures used to update event/trigger
-- selection states, prefor, prefmag, origin reading
-- associations and event waveform associations
-- in the NCEDC parametric schema tables.
--
-- Note Code here does not check for existance of table row keys that match input ids
--      however the FUNCTIONS return a value > 0 if the database was modified.
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
-- Java org.trinet.storedprocs.CatEntry
-- Java org.trinet.jasi.TN.WaveformTN
-- Java org.trinet.jasi.TN.SolutionTN
-- Java org.trinet.storedprocs.CatEntry
-- tpp/perlmodules/EventActions.pm
-- tpp/perlmodules/Magnitude.pm
--
-- <DEPENDS ON > (loaded on server)
-- NCEDC Parametric tables
-- refs package GEO_REGION installed
-- refs package MAGPREF installed
-- refs package ORGPREF installed
-- refs package PCS installed
-- refs package TRUETIME installed
--
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- Increment from its current value the version of the event row whose key is the input evid.
-- NOTE: A NULL event version becomes version 2.
-- Returns version, else -1, if update failed, or 0, if no matching event id is found.
-- Commits on success.
-- Example: call EPREF.bump_version(1111);
  FUNCTION bump_version(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ---------------------------------------------------------------------------
--
-- As above, except commits on success only if input p_commit > 0.
  FUNCTION bump_version(p_evid Event.evid%TYPE, p_commit PLS_INTEGER) RETURN NUMBER;
-- ---------------------------------------------------------------------------
--
-- Get value the version number of the event row whose key is the input evid.
-- Returns version, or -1 if no matching event id is found, or 0 if event version is NULL.
-- Example: call EPREF.get_version(1111);
  FUNCTION get_version(p_evid Event.evid%TYPE) RETURN NUMBER;
--
-- Updates or inserts a credit table row whose fields match input values.
-- Commits transaction if successful.
-- Returns attribution id on success, null otherwise.
  FUNCTION attribute(p_id NUMBER, p_tablename VARCHAR2, p_reference VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
--
-- Updates or inserts a credit table row whose fields match input values.
-- Commits transaction if successful and p_commit > 0.
-- Returns attribution id on success, null otherwise.
  FUNCTION attribute(p_id NUMBER, p_tablename VARCHAR2, p_reference VARCHAR2, p_commit PLS_INTEGER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Get string that identifies who created Origin row corresponding to input orid
-- Returns NULL if no such row or error.
   FUNCTION getWhoOrigin(p_orid Origin.orid%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- Get string that identifies who created NetMag table row corresponing to input magid.
-- Returns NULL if no such row or error.
   FUNCTION getWhoNetMag(p_magid Netmag.magid%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Set Origin.rflag 'H' for Origin row whose id is the prefor of the Event whose key equals input p_evid.
-- Bumps event version if new rflag value, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.accept_event(1111);
  FUNCTION accept_event(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Event type remains unchanged ('st'), set Event.selectFlag=1, Origin.rflag 'H'.
-- Doesn't change the Origin bogusFlag setting.
-- Bumps event version if new rflag value, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.accept_trigger(2222);
  FUNCTION accept_trigger(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Event type remains unchanged ('st'), set Event.selectFlag=1, Origin.rflag 'H',
-- and origin lat/lon/z to input values.
-- If p_bogus is non-zero, or both input p_lat and p_lon are zero set Origin.bogusflag=1 (bogus origin), 
-- otherwise set the Origin.bogusflag=0. 
-- Bumps event version if new rflag value, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.accept_trigger(1111, 36.0, -118.0, 6.0, 0);
  FUNCTION accept_trigger(p_evid Event.evid%TYPE,
                           p_lat Origin.lat%TYPE,
                           p_lon Origin.lon%TYPE,
                           p_z Origin.depth%TYPE,
                           p_bogus Origin.bogusflag%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Set event.selectFlag=0, commit changes.
-- Posts evid 'TPP.TPP.DELETED.100'.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.accept_trigger(1111, 36.0, -118.0, 6.0, 0);
-- Example: call EPREF.delete_event(1111);
  FUNCTION delete_event(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Posts evid 'TPP.TPP.DELETED.100' which causes cancellation messages to be sent.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cancel_event(1111);
  FUNCTION cancel_event(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
-- Update all ALARM_ACTION table rows associated with input evid whose ACTION_STATE string
-- matches 'CANCEL%' to '!CANCELLED', and if the event prefor rflag is in ('A','C') sets
-- its value to 'H'. Function DOES NOT post the evid for re-alarming.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.uncancel_event(1111);
  FUNCTION uncancel_event(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
-- ---------------------------------------------------------------------------
--
-- Deletes the eventprefmag row corresponding to inputs if it is not (Event.prefmag)
-- the event preferred magnitude. Upon success commits transaction.
-- Returns 1 if row was deleted, returns 0 if no such row, or input magid is the
-- event preferred magid. Returns -1 if other exception.
  FUNCTION delete_prefmag(p_evid Event.evid%TYPE, p_magid Netmag.magid%TYPE) RETURN PLS_INTEGER;
--
-- Deletes the eventprefmag row corresponding to inputs if it is not (Event.prefmag)
-- the event preferred magnitude. Upon success commits transaction.
-- Returns 1 if row was deleted, returns 0 if no such row, or input magtype is the
-- event preferred magtype. Returns -1 if other exception.
  FUNCTION delete_prefmag(p_evid Event.evid%TYPE, p_magtype NetMag.magtype%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Set Origin.rflag='F'.
-- Associate all amps associated with event's earliest origin with the current prefor. 
-- Commits the transaction.
-- Posts evid 'TPP.TPP.FINAL.100'.
-- Bumps event version if new rflag value, commits changes.
-- Return value = 1 rflag set to 'F', = 2 already posted, =3 successful new post, <= 0 an error.
-- Example: call EPREF.finalize_event(1111);
  FUNCTION finalize_event(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- Like above, but bumps the event version only if p_bump>0.
  FUNCTION finalize_event(p_evid Event.evid%TYPE, p_bump PLS_INTEGER) RETURN PLS_INTEGER;
--
-- Adds rows to PCS_STATE table for input event id having state description:
--  TPP TPP DISTRIBUTE 100 0
-- Does not change origin.rflag value, used to publish, or resend alarms etc.
-- Return value = 1 already posted, =2 successful new post, <= 0 an error.
  FUNCTION distribute_event(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Set Origin.rflag to input value where orid equals the prefor of input Event.evid. 
-- Bumps event version if new rflag value, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setRflag(1111, 'F');
  FUNCTION setRflag(p_evid Event.evid%TYPE, p_rflag Origin.rflag%TYPE) RETURN PLS_INTEGER;
-- Like above, but bumps the event version only if p_bump>0.
  FUNCTION setRflag(p_evid Event.evid%TYPE, p_rflag Origin.rflag%TYPE, p_bump PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Does NOT actually remove the record, just deletes all associations to all origins.
-- Commits the transaction.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.unassociate_arrival(1111);
  FUNCTION unassociate_arrival(p_arid Arrival.arid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Create AssocWaE record for input WaveForm and Event table key ids.
-- Commits the transaction.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.associateWaveform(2222, 1111);
  FUNCTION associateWaveform(p_wfid AssocWaE.wfid%TYPE, p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate new event id with the waveforms associated with the old evid. 
-- Commits the transaction.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocWaE(1111, 2222);
  FUNCTION cloneAssocWaE(p_evidOld Event.evid%TYPE, p_evidNew Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate new event id with the waveforms associated with the old evid. 
-- Will "commit" transaction only if the value of the input p_commit > 0.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocWaE(1111, 2222, 1);
  FUNCTION cloneAssocWaE(p_evidOld Event.evid%TYPE,
                         p_evidNew Event.evid%TYPE,
                         p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate new origin id with the amps associated with the old origin id. 
-- Commits the transaction.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocAmO(1111, 2222);
  FUNCTION cloneAssocAmO(p_oridOld AssocAmO.orid%TYPE, p_oridNew AssocAmO.orid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate new origin id with the arrivals associated with the old origin id. 
-- Commits the transaction.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocArO(1111, 2222);
  FUNCTION cloneAssocArO(p_oridOld AssocArO.orid%TYPE, p_oridNew AssocArO.orid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate new origin id with the amps associated with the old origin id. 
-- Will "commit" transaction only if the value of the input p_commit > 0.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocAmO(1111, 2222, 1);
  FUNCTION cloneAssocAmO(p_oridOld AssocAmO.orid%TYPE,
                          p_oridNew AssocAmO.orid%TYPE,
                          p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate new origin id with the arrivals associated with the old origin id. 
-- Will "commit" transaction only if the value of the input p_commit > 0.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocArO(1111, 2222, 1);
  FUNCTION cloneAssocArO(p_oridOld AssocArO.orid%TYPE,
                          p_oridNew AssocArO.orid%TYPE,
                          p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Associate Event.prefor of input id with all amps associated with the earliest origin for event. 
-- Commits the transaction.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.cloneAssocAmO(1111);
-- DEPRECATED - DO NOT USE
-- FUNCTION cloneAssocAmO(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Set the event type for the Event table row whose id equals p_evid to the input value.
-- Bumps event version if new etype value, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setEventType(1111, 'le');
  FUNCTION setEventType(p_evid Event.evid%TYPE, p_type Event.etype%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Example: call EPREF.setTypeLocal(1111);
-- Commits change.
  FUNCTION setTypeLocal(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
-- Example: call EPREF.setTypeQuarry(1111);
-- Commits change.
  FUNCTION setTypeQuarry(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
-- Example: call EPREF.setTypeTele(1111);
-- Commits change.
  FUNCTION setTypeTele(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
-- Example: call EPREF.setTypeRegional(1111);
-- Commits change.
  FUNCTION setTypeRegional(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
-- Example: call EPREF.setTypeSonic(1111);
-- Commits change.
  FUNCTION setTypeSonic(p_evid Event.evid%TYPE) RETURN PLS_INTEGER;
--
--
-- Set the gtype for the prefor of the event table row whose id equals p_evid to the input value.
-- DOES not bumps event version, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setPreforGType(1111, 'le');
  FUNCTION setPreforGType(p_evid Event.evid%TYPE, p_gtype Origin.gtype%TYPE) RETURN PLS_INTEGER;
--
-- Set the gtype for the origin table row whose id equals p_orid to the input value.
-- DOES not bumps event version, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setOriginGtype(1111, 'le');
  FUNCTION setOriginGType(p_orid Origin.orid%TYPE, p_gtype Origin.gtype%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Set prefor and prefmag for Event row whose key equals p_evidm to the values
-- found for row whose key equals p_evids invokes delete_event(p_evids)
-- Returns value > 0 on success, <= 0 if not.
-- Commits transaction if successful.
-- Example: call EPREF.merge(1111, 2222);
  FUNCTION merge(p_evidm Event.evid%TYPE, p_evids Event.evid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Insert a new NetMag row as described by input values.
-- Return the new Netmag.magid taken from the id sequencer.
-- Use when values of nsta, uncertainty, gap, distance are unknown. 
-- Note this DOES NOT UPDATE event prefmag, just inserts a new row.
-- Returns value of new magid on success.
-- Commits transaction if successful.
-- Example: call EPREF.insertNetMag(1111, 6., 'e', 'CI', 'OES', 'Unknown', 1., 'H');
  FUNCTION insertNetMag(p_orid      NetMag.orid%TYPE,
                        p_magVal    NetMag.magnitude%TYPE,
                        p_magType   NetMag.magtype%TYPE,
                        p_auth      NetMag.auth%TYPE,
                        p_subsrc    NetMag.subsource%TYPE,
                        p_algo      NetMag.magalgo%TYPE,
                        p_qual      NetMag.quality%TYPE,
                        p_rflag     NetMag.rflag%TYPE) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Insert a NetMag row. Return magid which is taken from the sequencer.
-- Example: call EPREF.insertNetMag(1111, 3., 'l', 'CI', 'RT', 'SoCalMl', 11, .23, 33, 8.8, 1., 'H');
-- Note this DOES NOT UPDATE event prefmag, just inserts a new row.
-- Returns value of new magid on success.
-- Commits transaction if successful.
  FUNCTION insertNetMag(p_orid      NetMag.orid%TYPE,
                        p_magVal    NetMag.magnitude%TYPE,
                        p_magType   NetMag.magtype%TYPE,
                        p_auth      NetMag.auth%TYPE,
                        p_subsrc    NetMag.subsource%TYPE,
                        p_algo      NetMag.magalgo%TYPE,
                        p_nsta      NetMag.nsta%TYPE,
                        p_unc       NetMag.uncertainty%TYPE,
                        p_gap       NetMag.gap%TYPE,
                        p_dist      NetMag.distance%TYPE,
                        p_qual      NetMag.quality%TYPE,
                        p_rflag     NetMag.rflag%TYPE) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Insert a NetMag row. Return magid which is taken from the sequencer.
-- Example: call EPREF.insertNetMag(1111, 3., 'l', 'CI', 'RT', 'SoCalMl', 11, .23, 33, 8.8, 1., 'H', 1);
-- Note this DOES NOT UPDATE event prefmag, just inserts a new row.
-- Returns value of new magid on success.
-- Commits database transaction only if input parameter p_commit > 0.
  FUNCTION insertNetMag(p_orid      NetMag.orid%TYPE,
                        p_magVal    NetMag.magnitude%TYPE,
                        p_magType   NetMag.magtype%TYPE,
                        p_auth      NetMag.auth%TYPE,
                        p_subsrc    NetMag.subsource%TYPE,
                        p_algo      NetMag.magalgo%TYPE,
                        p_nsta      NetMag.nsta%TYPE,
                        p_unc       NetMag.uncertainty%TYPE,
                        p_gap       NetMag.gap%TYPE,
                        p_dist      NetMag.distance%TYPE,
                        p_qual      NetMag.quality%TYPE,
                        p_rflag     NetMag.rflag%TYPE,
                        p_commit    PLS_INTEGER) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Insert a NetMag row. Return magid which is taken from the sequencer.
-- Example: call EPREF.insertNetMag(1111, 3., 'l', 'CI', 'RT', 'SoCalMl', 11, 22, .23, 33, 8.8, 1., 'H', 1);
-- Note this DOES NOT UPDATE event prefmag, just inserts a new row.
-- Returns value of new magid on success.
-- Commits database transaction only if input parameter p_commit > 0.
  FUNCTION insertNetMag(p_orid      NetMag.orid%TYPE,
                        p_magVal    NetMag.magnitude%TYPE,
                        p_magType   NetMag.magtype%TYPE,
                        p_auth      NetMag.auth%TYPE,
                        p_subsrc    NetMag.subsource%TYPE,
                        p_algo      NetMag.magalgo%TYPE,
                        p_nsta      NetMag.nsta%TYPE,
                        p_nobs      NetMag.nobs%TYPE,
                        p_unc       NetMag.uncertainty%TYPE,
                        p_gap       NetMag.gap%TYPE,
                        p_dist      NetMag.distance%TYPE,
                        p_qual      NetMag.quality%TYPE,
                        p_rflag     NetMag.rflag%TYPE,
                        p_commit    PLS_INTEGER) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- If the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, 
-- the new magnitude must have the highest ranked priority >= 0.
-- If the ORGPREFPRIORITY table has rules that apply to the input origins's type, 
-- the new origin must have the highest ranked priority >= 0,
-- otherwise this function is a no-op, no row updates and no row insertions.
--
-- Set the prefor and prefmag of Event row whose key corresponds to input p_evid. 
-- Does NOT check for foreign key consistency, i.e. does not update Origin.evid or Netmag.orid.
-- Bumps event version if preferred id change, commit changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setPref(1111, 2222, 3333); 
  FUNCTION setPref(p_evid Event.evid%TYPE, p_orid Origin.orid%TYPE, p_magid Netmag.magid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- If the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, 
-- the new magnitude must have the highest ranked priority >= 0.
-- If the ORGPREFPRIORITY table has rules that apply to the input origins's type, 
-- the new origin must have the highest ranked priority >= 0,
-- otherwise this function is a no-op, no row updates and no row insertions.
--
-- Set the prefor, prefmag, and prefmec of Event row whose key corresponds to input p_evid. 
-- Does NOT check for foreign key consistency, i.e. does not update Origin.evid or Netmag.orid.
-- Bumps event version if preferred id change, commit changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setPref(1111, 2222, 3333, 4444); 
  FUNCTION setPref(p_evid Event.evid%TYPE,
                   p_orid Origin.orid%TYPE,
                   p_magid Netmag.magid%TYPE,
                   p_mecid Mec.mecid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- First creates a new Netmag row from the specified inputs setting its orid value to Event.prefor
--
-- Then if the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, 
-- and this newly inserted magnitude has the highest ranked priority >= 0, 
-- set Event.prefmag and Origin.prefmag table row values to input magid regardless of magtype.
-- Update or insert row in EventPrefmag table corresponding to the Netmag.magtype
-- of new magid and input evid.
-- Bump event version and commit changes.
-- Returns new magid > 0 on sucess, <= 0 if not.
-- Example: call EPREF.setPrefMag(1111, 3.,'l','CI','RT','SoCalMl',11,.23,33,8.8,1.,'H') INTO :magid;
  FUNCTION setPrefMag(p_evid      Event.evid%TYPE,
                       p_magVal    NetMag.magnitude%TYPE,
                       p_magType   NetMag.magtype%TYPE,
                       p_auth      NetMag.auth%TYPE,
                       p_subsrc    NetMag.subsource%TYPE,
                       p_algo      NetMag.magalgo%TYPE,
                       p_nsta      NetMag.nsta%TYPE,
                       p_unc       NetMag.uncertainty%TYPE,
                       p_gap       NetMag.gap%TYPE,
                       p_dist      NetMag.distance%TYPE,
                       p_qual      NetMag.quality%TYPE,
                       p_rflag     NetMag.rflag%TYPE) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- Same actions as signature above, but commit the transaction only if input p_commit > 0;
  FUNCTION setPrefMag(p_evid      Event.evid%TYPE,
                       p_magVal    NetMag.magnitude%TYPE,
                       p_magType   NetMag.magtype%TYPE,
                       p_auth      NetMag.auth%TYPE,
                       p_subsrc    NetMag.subsource%TYPE,
                       p_algo      NetMag.magalgo%TYPE,
                       p_nsta      NetMag.nsta%TYPE,
                       p_unc       NetMag.uncertainty%TYPE,
                       p_gap       NetMag.gap%TYPE,
                       p_dist      NetMag.distance%TYPE,
                       p_qual      NetMag.quality%TYPE,
                       p_rflag     NetMag.rflag%TYPE,
                       p_commit PLS_INTEGER) RETURN NUMBER;
--
-- Same actions as signature above, but commit the transaction only if input p_commit > 0;
  FUNCTION setPrefMag(p_evid      Event.evid%TYPE,
                       p_magVal    NetMag.magnitude%TYPE,
                       p_magType   NetMag.magtype%TYPE,
                       p_auth      NetMag.auth%TYPE,
                       p_subsrc    NetMag.subsource%TYPE,
                       p_algo      NetMag.magalgo%TYPE,
                       p_nsta      NetMag.nsta%TYPE,
                       p_nobs      NetMag.nobs%TYPE,
                       p_unc       NetMag.uncertainty%TYPE,
                       p_gap       NetMag.gap%TYPE,
                       p_dist      NetMag.distance%TYPE,
                       p_qual      NetMag.quality%TYPE,
                       p_rflag     NetMag.rflag%TYPE,
                       p_commit PLS_INTEGER) RETURN NUMBER;
-- ---------------------------------------------------------------------------
--
-- Create a new Netmag row from the specified inputs setting its orid value to Event.prefor
--
-- If the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, 
-- and the new magnitude has the highest ranked priority >= 0, then update or insert row
-- in EventPrefmag table corresponding to the Netmag.magtype of new magid and input evid.
-- Also set the Event.prefmag and Origin.prefmag values if they are null or are
-- of same magtype as the new magnitude.
-- Bumps event version if event prefmag changed, commits changes.
-- Returns new magid > 0 on sucess, <= 0 if not.
-- Example: call EPREF.setPrefMagOfType(1111,3.,'l','CI','RT','SoCalMl',11,.23,33,8.8,1.,'H') INTO :magid;
  FUNCTION setPrefMagOfType(p_evid      Event.evid%TYPE,
                             p_magVal    NetMag.magnitude%TYPE,
                             p_magType   NetMag.magtype%TYPE,
                             p_auth      NetMag.auth%TYPE,
                             p_subsrc    NetMag.subsource%TYPE,
                             p_algo      NetMag.magalgo%TYPE,
                             p_nsta      NetMag.nsta%TYPE,
                             p_unc       NetMag.uncertainty%TYPE,
                             p_gap       NetMag.gap%TYPE,
                             p_dist      NetMag.distance%TYPE,
                             p_qual      NetMag.quality%TYPE,
                             p_rflag     NetMag.rflag%TYPE) RETURN NUMBER;
-- ---------------------------------------------------------------------------
--
-- Same actions as signature above, but commit the transaction only if input p_commit > 0;
  FUNCTION setPrefMagOfType(p_evid      Event.evid%TYPE,
                             p_magVal    NetMag.magnitude%TYPE,
                             p_magType   NetMag.magtype%TYPE,
                             p_auth      NetMag.auth%TYPE,
                             p_subsrc    NetMag.subsource%TYPE,
                             p_algo      NetMag.magalgo%TYPE,
                             p_nsta      NetMag.nsta%TYPE,
                             p_unc       NetMag.uncertainty%TYPE,
                             p_gap       NetMag.gap%TYPE,
                             p_dist      NetMag.distance%TYPE,
                             p_qual      NetMag.quality%TYPE,
                             p_rflag     NetMag.rflag%TYPE,
                             p_commit PLS_INTEGER) RETURN NUMBER;
--
-- Same actions as signature above, but commit the transaction only if input p_commit > 0;
  FUNCTION setPrefMagOfType(p_evid      Event.evid%TYPE,
                             p_magVal    NetMag.magnitude%TYPE,
                             p_magType   NetMag.magtype%TYPE,
                             p_auth      NetMag.auth%TYPE,
                             p_subsrc    NetMag.subsource%TYPE,
                             p_algo      NetMag.magalgo%TYPE,
                             p_nsta      NetMag.nsta%TYPE,
                             p_nobs      NetMag.nobs%TYPE,
                             p_unc       NetMag.uncertainty%TYPE,
                             p_gap       NetMag.gap%TYPE,
                             p_dist      NetMag.distance%TYPE,
                             p_qual      NetMag.quality%TYPE,
                             p_rflag     NetMag.rflag%TYPE,
                             p_commit PLS_INTEGER) RETURN NUMBER;
--
-- DEPRECATED
-- Wrapper around setprefmag_event(p_evid, p_magid)
-- Set Event.prefmag and Origin.prefmag table row values to input magid regardless of its magtype.
-- Bumps event version if event preferred magnitude (Event.prefmag) changes, commit changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setPrefMag(1111, 2222); 
  FUNCTION setPrefMag(p_evid Event.evid%TYPE, p_magid Event.prefmag%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Set preferred magnitude for an event corresponding to the magtype of the input magid
-- if input magid <> event.prefmag and the event.prefmag is the same magtype as magid,
-- update the event table prefmag and eventprefmag table if preferred magid has changed.
-- Insert a new eventprefmag table row if none already exists for the magtype and evid.
-- If the prefmag of the Origin row associated with the Netmag is of the same magtype,
-- set the prefmag of this Origin row to the input magid.
--
-- If the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, 
-- the new magnitude must have the highest ranked priority >= 0, otherwise this function
-- is a no-op, row values are not changed: no row updates and no row insertions.
--
-- A row must already exist in the Event table whose key is input evid.
-- A row must already exist in the Netmag table whose key is the input magid.
-- A row must already exist in the Origin table whose key is orid value of the Netmag row.
-- If either the Event or Netmag table row is missing, or the Origin row 
-- corresponding to the Netmag.orid has an evid value different from the input p_evid
-- the transaction fails.
-- Bumps version if event preferred magnitude changes, commits changes.
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmag was updated.
-- Example: call EPREF.setprefmag_magtype(1111, 2222) into :v_status;
  FUNCTION setprefmag_magtype(p_evid EVENT.EVID%TYPE, p_magid NETMAG.MAGID%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- As above checks priority rule, with added control flags:
-- p_evtpref  > 0, set p_magid event preferred regardless of magtype.
--
-- p_bump     > 0, bump event version if Event.prefmag changed.
--              NOTE: if event.prefmag is NULL or existing event.prefmag has same magtype
--              as input magid you must set p_bump=1 for event.version to be incremented. 
--
-- p_commit   > 0, commit transaction changes. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmag was updated.
  FUNCTION setprefmag_magtype(p_evid    EVENT.EVID%TYPE,
                              p_magid   NETMAG.MAGID%TYPE,
                              p_evtpref PLS_INTEGER,
                              p_bump    PLS_INTEGER,
                              p_commit  PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- As above, plus added control flag for the magnitude priority rule application:
-- p_check_priority > 0, set prefmag only if input magnitude satisfies magnitude priority rules. 
--                  <=0, ignore magnitude priority rules. 
-- NOTE: if event.prefmag is NULL or existing event.prefmag has same magtype
--       as input magid you must set p_bump=1 for event.version to be incremented. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmag was updated.
  FUNCTION setprefmag_magtype(p_evid    EVENT.EVID%TYPE,
                              p_magid   NETMAG.MAGID%TYPE,
                              p_magtype NETMAG.MAGTYPE%TYPE,
                              p_evtpref PLS_INTEGER,
                              p_bump PLS_INTEGER,
                              p_commit PLS_INTEGER,
                              p_check_priority PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- If the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, the new
-- magnitude must have the highest ranked priority >= 0, otherwise this function is a no-op,
-- row values are not changed: no row updates and no row insertions.
--
-- Set Event.prefmag and Origin.prefmag table row values to input magid regardless of its magtype.
-- Updates or insert row in EventPrefmag table corresponding to the Netmag.magtype
-- of input magid and input evid.
-- Bumps version if event preferred magnitude changed, commits changes.
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmag was updated.
-- Example: call EPREF.setprefmag_event(1111, 2222) into :v_status;
  FUNCTION setprefmag_event(p_evid EVENT.EVID%TYPE, p_magid NETMAG.MAGID%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- As above, update event prefmag, with additional programming control flags:
-- p_bump     > 0, bump event version if Event.prefmag changed.
-- p_commit   > 0, commit transaction with changes. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmag was updated.
  FUNCTION setprefmag_event(p_evid EVENT.EVID%TYPE, p_magid NETMAG.MAGID%TYPE,
                            p_bump PLS_INTEGER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- If the MAGPREFPRIORITY table has rules that apply to the input magnitude's magtype, the new
-- magnitude must have the highest ranked priority >= 0, otherwise this function is a no-op,
-- row values are not changed: no row updates and no row insertions.
--
-- Set the prefmag value of the Origin table row whose row key is the value of Netmag.orid.
-- for the Netmag table row whose key value is the input p_magid.
-- Does not update Event.prefmag so no change to EventPrefmag table.
-- Return > 0 for success, <= 0 if error, Netmag row DNE for input key, or
-- magnitude priority test fails by MagPrefPriority table rules.
-- Commits transaction if successful.
-- Example: call EPREF.setprefmag_origin(1111) into :v_status;
  FUNCTION setprefmag_origin(p_magid NETMAG.magid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- As above, plus extra flag to control the magnitude priority rule application:
-- p_check_priority > 0, set prefmag only if input magnitude satisfies magnitude priority rules. 
--                  <=0, ignore magnitude priority rules. 
-- Returns value > 0 on success, <= 0 if not.
  FUNCTION setprefmag_origin(p_magid NETMAG.magid%TYPE, p_check_priority PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- Set the orid value of the Netmag table row whose key corresponds to input p_magid
-- Origin row with a key equal to p_orid must already exist.
-- Return 1 for success, <= 0 if table rows do not already exist for the input keys.
-- Commits transaction if successful.
-- Example: call EPREF.set_magorid(1111, 2222) into :v_status;
  FUNCTION set_magorid(p_orid ORIGIN.orid%TYPE, p_magid NETMAG.magid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return magid of the preferred magnitude for the input magnitude type and event id.
-- Returns 0 if no matching data is found. Queries the EVENTPREFMAG table.
-- Example: call EPREF.prefmagid(1111, 'l') into :v_magid;
  FUNCTION prefmagid(p_evid Event.evid%TYPE, p_magtype VARCHAR2) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Set preferred origin for an event corresponding to the type of the input orid
-- if input orid <> event.prefor and the event.prefor is the same type as orid,
-- updates both the Event table prefor and EVENTPREFOR table. 
-- Insert a new EVENTPREFOR table row if none already exists.
--
-- If the ORGPREFPRIORITY table has rules that apply to the input origins's type,
-- the new origin must have the highest ranked priority >= 0, otherwise this function is a no-op,
-- no row updates and no row insertions.
--
-- A row must already exist in the Event table whose key is input evid.
-- A row must already exist in the Origin table whose key is orid value of the Origin row.
-- If either the Event or the Origin row is missing the transaction fails.
--
-- Bumps version if event preferred origin (prefor) changes, commits changes.
--
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
-- Example: call EPREF.setprefor_type(1111, 2222) into :v_status;
  FUNCTION setprefor_type(p_evid EVENT.EVID%TYPE, p_orid Origin.orid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- As above checks priority rule, with added control flags:
-- p_evtpref  > 0, set p_orid event preferred regardless of type.
--
-- p_bump     > 0, bump event version if Event.prefor changed.
--              NOTE: if Event.prefor is NULL or existing Event.prefor has same type
--              as input orid you must set p_bump=1 for Event.version to be incremented. 
--
-- p_commit   > 0, commit transaction changes. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
  FUNCTION setprefor_type(p_evid    EVENT.evid%TYPE,
                          p_orid    Origin.orid%TYPE,
                          p_evtpref PLS_INTEGER,
                          p_bump    PLS_INTEGER,
                          p_commit  PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- As above, plus added control flag for the origin priority rule application:
-- p_check_priority > 0, set prefor only if input origin satisfies ORGPREFPRIORITY rules. 
--                  <=0, ignore origin priority rules. 
--
-- NOTE: if Event.prefor is NULL or existing Event.prefor has same type
--       as input orid you must set p_bump=1 for Event.version to be incremented. 
--
-- p_commit   > 0, commit transaction changes. 
--
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
  FUNCTION setprefor_type(p_evid           EVENT.evid%TYPE,
                          p_orid           Origin.orid%TYPE,
                          p_otype          Origin.type%TYPE,
                          p_evtpref        PLS_INTEGER,
                          p_bump           PLS_INTEGER,
                          p_commit         PLS_INTEGER,
                          p_check_priority PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
--
-- If the ORGPREFPRIORITY table has rules that apply to the input origins's type,
-- the new origin must have the highest ranked priority >= 0, otherwise this function is a no-op,
-- no row updates and no row insertions.
--
-- Set Event.prefor table row prefor value to input orid regardless of its type.
-- Update or insert row in EventPrefor table corresponding to the Origin.type
-- Bumps version if event preferred origin changed, commits changes.
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
--
-- Does NOT check for foreign key consistency, i.e. 
-- the corresponding Origin.evid or Event.prefmag's Netmag.orid.
--
-- Example: call EPREF.setprefor_event(1111, 2222) into :v_status;
  FUNCTION setprefor_event(p_evid EVENT.EVID%TYPE, p_orid ORIGIN.orid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- As above, updates Event.prefor and updates EventPrefor table.
-- with additional programming control flags:
-- p_bump     > 0, bump event version if Event.prefor is changed.
-- p_commit   > 0, commit transaction with changes. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
  FUNCTION setprefor_event(p_evid EVENT.EVID%TYPE, p_orid ORIGIN.orid%TYPE,
                            p_bump PLS_INTEGER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Return orid of the preferred origin of the input type associated with event id.
-- Returns 0 if no matching data is found. Queries the EVENTPREFOR table.
-- Example: call EPREF.preforid(1111, 'H') into :v_orid;
  FUNCTION preforid(p_evid Event.evid%TYPE, p_otype ORIGIN.type%TYPE) RETURN NUMBER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Deletes the EVENTPREFOR row corresponding to inputs if it is not (Event.prefor)
-- the event preferred origin. Upon success commits transaction.
-- Returns 1 if row was deleted, returns 0 if no such row, or input orid is the
-- event preferred orid. Returns -1 if other exception.
  FUNCTION delete_prefor(p_evid Event.evid%TYPE, p_orid Origin.orid%TYPE) RETURN PLS_INTEGER;
--
-- Deletes the EVENTPREFOR row corresponding to inputs if it is not (Event.prefor)
-- the event preferred origin. Upon success commits transaction.
-- Returns 1 if row was deleted, returns 0 if no such row, or input type is the
-- event preferred type. Returns -1 if other exception.
  FUNCTION delete_prefor(p_evid Event.evid%TYPE, p_otype Origin.type%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Set the prefmec of Event row whose key corresponds to input p_evid. 
-- Updates EventPrefmec history table.
-- Wrapper queries database for the mech type of p_mecid and then invokes:
--     setprefmec_mechtype(p_evid, p_mecid, v_mechtype, 1, p_bump, p_commit);
--
-- Bumps event version if event prefmec changes, commits changes.
-- p_bump     > 0, bump event version if Event.prefmag changed.
-- p_commit   > 0, commit transaction with changes. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmec was updated.
  FUNCTION setprefmec_event(p_evid Event.evid%TYPE, p_mecid Mec.mecid%TYPE,
                            p_bump PLS_INTEGER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- As above, a wrapper that invokes: setprefmec_event(p_evid, p_mecid, 1, 1)
-- Bumps the version and commits transaction if the prefmec changed.
-- Updates EventPrefmec history table.
-- Returns value > 0 on success, <= 0 if not. 
-- Example: call EPREF.setprefmec_event(1111, 2222) into :stat; 
  FUNCTION setprefmec_event(p_evid Event.evid%TYPE, p_mecid Mec.mecid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Sets the prefmec of Event row whose key corresponds to input p_evid. 
-- Updates EventPrefmec history table.
-- p_bump     > 0, bump event version if Event.prefmag changed.
-- p_commit   > 0, commit transaction with changes. 
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmec was updated.
  FUNCTION setprefmec_mechtype(p_evid Event.evid%TYPE,
                               p_mecid Mec.mecid%TYPE,
                               p_mechType VARCHAR2,
                               p_evtpref PLS_INTEGER,
                               p_bump PLS_INTEGER,
                               p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- As above, a wrapper that queries the database for the mech type of the input p_mecid
-- and then invokes:
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefmec was updated.
  FUNCTION setprefmec_mechtype(p_evid Event.evid%TYPE,
                               p_mecid Mec.mecid%TYPE,
                               p_evtpref PLS_INTEGER,
                               p_bump PLS_INTEGER,
                               p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Set the prefmec of Origin row whose key corresponds to input p_orid. 
-- Implementation makes no use of EventPrefmec or EventPrefor table.
-- Commits transaction if corresponding origin prefmec value changed.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setprefmec_origin(1111, 2222) into :stat; 
  FUNCTION setprefmec_origin(p_orid Origin.orid%TYPE, p_mecid Mec.mecid%TYPE) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Return string describing the id, subsource, and who attribute of the preferred origin and magnitude.
  FUNCTION prefIdSrcWho(p_evid EVENT.evid%TYPE) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
  -- Return String concatenating remarks associated with input commid in Remark table..
  -- Returns null if no matches rows.
  FUNCTION getComment(p_commid REMARK.commid%TYPE) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
  -- Inserts row with specified keys into Remark table. Updates row if row matching input keys already exists.
  -- Returns 1 on success.
  FUNCTION insertComment(p_commid REMARK.commid%TYPE, p_lineno REMARK.lineno%TYPE, p_comment VARCHAR2) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Inserts AssocArO table row if no row exists matching input key ids, else it's a no-op, no update.
-- Returns row count of rows inserted, 0 for no-op, raises an exception on error.
  FUNCTION insertAssocArO(
              p_orid AssocArO.ORID%TYPE,
              p_arid AssocArO.ARID%TYPE,
              p_auth AssocArO.AUTH%TYPE,
              p_ssrc AssocArO.SUBSOURCE%TYPE,
              p_iphs AssocArO.IPHASE%TYPE,
              p_impt AssocArO.IMPORTANCE%TYPE,
              p_dist AssocArO.DELTA%TYPE,
              p_azim AssocArO.SEAZ%TYPE,
              p_iwgt AssocArO.IN_WGT%TYPE,
              p_owgt AssocArO.WGT%TYPE,
              p_tres AssocArO.TIMERES%TYPE,
              p_ema  AssocArO.EMA%TYPE,
              p_sdly AssocArO.SDELAY%TYPE,
              p_rflg AssocArO.RFLAG%TYPE
           ) RETURN PLS_INTEGER;
--
-- Create AssocArO table row is none exists matching input orid,ampid otherwise update existing row.
-- Returns 1 if row inserted or updated, raises exception on error.
  PROCEDURE upsertAssocArO(
              p_orid AssocArO.ORID%TYPE,
              p_arid AssocArO.ARID%TYPE,
              p_auth AssocArO.AUTH%TYPE,
              p_ssrc AssocArO.SUBSOURCE%TYPE,
              p_iphs AssocArO.IPHASE%TYPE,
              p_impt AssocArO.IMPORTANCE%TYPE,
              p_dist AssocArO.DELTA%TYPE,
              p_azim AssocArO.SEAZ%TYPE,
              p_iwgt AssocArO.IN_WGT%TYPE,
              p_owgt AssocArO.WGT%TYPE,
              p_tres AssocArO.TIMERES%TYPE,
              p_ema  AssocArO.EMA%TYPE,
              p_sdly AssocArO.SDELAY%TYPE,
              p_rflg AssocArO.RFLAG%TYPE
           );
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Inserts AssocAmO table row if no row exists matching input key ids, else it's a no-op, no update.
-- Returns 1 if row inserted, 0 for no-op, raises exception on error.
  FUNCTION insertAssocAmO(
              p_orid AssocAmO.ORID%TYPE,
              p_ampid AssocAmO.AMPID%TYPE,
              p_auth AssocAmO.AUTH%TYPE,
              p_ssrc AssocAmO.SUBSOURCE%TYPE,
              p_dist AssocAmO.DELTA%TYPE,
              p_azim AssocAmO.SEAZ%TYPE,
              p_rflg AssocAmO.RFLAG%TYPE
           ) RETURN PLS_INTEGER;
--
-- Create AssocAmO table row is none exists matching input orid,ampid otherwise update existing row.
-- Returns 1 if row inserted or updated, raises exception on error.
  PROCEDURE upsertAssocAmO(
              p_orid AssocAmO.ORID%TYPE,
              p_ampid AssocAmO.AMPID%TYPE,
              p_auth AssocAmO.AUTH%TYPE,
              p_ssrc AssocAmO.SUBSOURCE%TYPE,
              p_dist AssocAmO.DELTA%TYPE,
              p_azim AssocAmO.SEAZ%TYPE,
              p_rflg AssocAmO.RFLAG%TYPE
           );
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Inserts AssocCoO table row if no row exists matching input key ids, else it's a no-op, no update.
-- Returns 1 if row inserted, 0 for no-op, raises exception on error.
  FUNCTION insertAssocCoO(
              p_orid AssocCoO.ORID%TYPE,
              p_coid AssocCoO.COID%TYPE,
              p_auth AssocCoO.AUTH%TYPE,
              p_ssrc AssocCoO.SUBSOURCE%TYPE,
              p_dist AssocCoO.DELTA%TYPE,
              p_azim AssocCoO.SEAZ%TYPE,
              p_rflg AssocCoO.RFLAG%TYPE
           ) RETURN PLS_INTEGER;
--
-- Create AssocCoO table row is none exists matching input orid,coid otherwise update existing row.
-- Returns 1 if row inserted or updated, raises exception on error.
  PROCEDURE upsertAssocCoO(
              p_orid AssocCoO.ORID%TYPE,
              p_coid AssocCoO.COID%TYPE,
              p_auth AssocCoO.AUTH%TYPE,
              p_ssrc AssocCoO.SUBSOURCE%TYPE,
              p_dist AssocCoO.DELTA%TYPE,
              p_azim AssocCoO.SEAZ%TYPE,
              p_rflg AssocCoO.RFLAG%TYPE
           );
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Inserts AssocAmM table row if no row exists matching input key ids, else it's a no-op, no update.
-- Returns 1 if row inserted, 0 for no-op, raises exception on error.
  FUNCTION insertAssocAmM(
              p_magid AssocAmM.MAGID%TYPE,
              p_ampid AssocAmM.AMPID%TYPE,
              p_auth AssocAmM.AUTH%TYPE,
              p_ssrc AssocAmM.SUBSOURCE%TYPE,
              p_wt AssocAmM.WEIGHT%TYPE,
              p_iwgt AssocAmM.IN_WGT%TYPE,
              p_mag AssocAmM.MAG%TYPE,
              p_mres AssocAmM.MAGRES%TYPE,
              p_mcorr AssocAmM.MAGCORR%TYPE,
              p_rflag AssocAmM.RFLAG%TYPE
           ) RETURN PLS_INTEGER;
--
-- Create AssocAmM table row is none exists matching input magid,ampid otherwise update existing row.
-- Returns 1 if row inserted or updated, raises exception on error.
  PROCEDURE upsertAssocAmM(
              p_magid AssocAmM.MAGID%TYPE,
              p_ampid AssocAmM.AMPID%TYPE,
              p_auth AssocAmM.AUTH%TYPE,
              p_ssrc AssocAmM.SUBSOURCE%TYPE,
              p_wt AssocAmM.WEIGHT%TYPE,
              p_iwgt AssocAmM.IN_WGT%TYPE,
              p_mag AssocAmM.MAG%TYPE,
              p_mres AssocAmM.MAGRES%TYPE,
              p_mcorr AssocAmM.MAGCORR%TYPE,
              p_rflag AssocAmM.RFLAG%TYPE
           );
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Inserts AssocCoM table row if no row exists matching input key ids, else it's a no-op, no update.
-- Returns 1 if row inserted, 0 for no-op, raises exception on error.
  FUNCTION insertAssocCoM(
              p_magid AssocCoM.MAGID%TYPE,
              p_coid AssocCoM.COID%TYPE,
              p_auth AssocCoM.AUTH%TYPE,
              p_ssrc AssocCoM.SUBSOURCE%TYPE,
              p_wt AssocCoM.WEIGHT%TYPE,
              p_iwgt AssocCoM.IN_WGT%TYPE,
              p_mag AssocCoM.MAG%TYPE,
              p_mres AssocCoM.MAGRES%TYPE,
              p_mcorr AssocCoM.MAGCORR%TYPE,
              p_rflag AssocCoM.RFLAG%TYPE
           ) RETURN PLS_INTEGER;
--
-- Create AssocCoM table row is none exists matching input magid,coid otherwise update existing row.
-- Returns 1 if row inserted or updated, raises exception on error.
  PROCEDURE upsertAssocCoM(
              p_magid AssocCoM.MAGID%TYPE,
              p_coid AssocCoM.COID%TYPE,
              p_auth AssocCoM.AUTH%TYPE,
              p_ssrc AssocCoM.SUBSOURCE%TYPE,
              p_wt AssocCoM.WEIGHT%TYPE,
              p_iwgt AssocCoM.IN_WGT%TYPE,
              p_mag AssocCoM.MAG%TYPE,
              p_mres AssocCoM.MAGRES%TYPE,
              p_mcorr AssocCoM.MAGCORR%TYPE,
              p_rflag AssocCoM.RFLAG%TYPE
           );
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--  Wrapper function updates the auth code only in event table row (uses p_level=0).
--  Commits transaction if p_commit > 0
--  Returns value > 0 on success, < 0 error, 0 no-op
  FUNCTION setAuthByRegion(p_evid EVENT.evid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
--
--  Wrapper function updates the auth column in tables only if a non-null value is returned  
--  from a function call to geo_region.authOfRegionInGroup(origin.lat, origin.lon, 'AUTH')
--  for the event table prefor associated with input p_evid and that value is not the same 
--  as the table rows's current auth value. The level of "update" is specified by input flag p_level:
--  p_level = 0 auth in event row.
--  p_level = 1 auth in event row, origin, mec, and netmag for magtype 'd','l','w','e'.
--  p_level = 2 auth in assocaro, assocamo, assoccoo, assocamm, assoccom for event prefor and eventprefmag magid of selected subtypes.
--  p_level = 3 auth in arrival, amp, coda associated with preferred origin and eventprefmag magnitude subtypes.
--  Commits transaction if p_commit > 0
--  Returns value > 0 on success, < 0 error, 0 no-op
  FUNCTION setAuthByRegion(p_evid EVENT.evid%TYPE, p_level PLS_INTEGER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
--
--  Update the auth column those table rows affiliated with input p_evid only if the input p_auth value
--  is not the same as the row's current value. The level of "update" is specified by input flag p_level:
--  p_level = 0 auth in event row 
--  p_level = 1 auth in event row, origin (prefor), mec (prefmec), and netmag for magtype 'd','l','w','e' in eventprefmag.
--  p_level = 2 auth in assocaro, assocamo, assoccoo, assocamm, assoccom for event prefor and eventprefmag magid of selected subtypes.
--  p_level = 3 auth in arrival, amp, coda associated with preferred origin and eventprefmag magnitude subtypes.
--  Commits transaction if p_commit > 0
--  Returns value > 0 on success, < 0 error, 0 no-op
  FUNCTION setAuthByRegion(p_evid EVENT.evid%TYPE, p_auth EVENT.auth%TYPE, p_level PLS_INTEGER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Call with input value 1 or 0 to enable/disable DBMS_OUTPUT.PUTLINE debugging output.
  PROCEDURE debug(p_debug PLS_INTEGER);
--
END epref;
```  
  
## Example  
  
There are three steps in correctly writing a new magnitude to the database.  
  
1.  Write the magnitude data in a new NETMAG row and get its MAGID with   
  
```
new_magid = EPREF.insertNetMag(
			p_orid,
                        p_magVal,
                        p_magType,
                        p_auth,
                        p_subsrc,
                        p_algo,
                        p_nsta,
                        p_unc,
                        p_gap,
                        p_dist,
                        p_qual,
                        p_rflag
            );
```
where the input p_orid is the id of the origin associated with the calculated magnitude. This procedure will create a new NETMAG table row but will NOT do any change preferred magnitude.   
  
2.  Then call ``EPREF.setPrefMag_MagType(p_evid, p_magid)`` which either creates a EVENTPREFMAG row or updates an existing one for the MAGTYPE associated with the MAGID.  
3.  Finally call ``MAGPREF.setPrefMagOfEvent(evid)`` to use the MAGPREFPRIORITY table rules and set EVENT.PREFMAG to the MAGID of the highest ranked of all magnitude types associated with the event *evid* in the EVENTPREFMAG table.   

