# QUARRY PACKAGE {#quarry-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE quarry AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: quarry_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Function tests whether an event is a quarry blast based upon its
-- magnitude, depth, time, and location with respect to the known 
-- quarry metadata. The metadata for each quarry are the maximum
-- magnitude of its blasts, the max distance allowed from the event to 
-- the quarry location, and the window of blast times for the days
-- of the week.
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--
-- <DEPENDS ON > (loaded on server)
-- NCEDC Parametric event, origin, and netmag tables accessible in the login schema
-- plus ancillary gazetteerquarry and gazetteerpt tables 
-- references package TRUETIME WHERES installation
-- Database defined user object types: qb_time, qb_schedule
--
-- In the gazetteerquarry table, the column qb_schedule object is nested table of
-- qb_time objects. The qb_time data elements are (days, stime, etime) where
-- days element is 7-char String mask and  stime (start) and etime (end) are
-- UTC 24hr hh24:mi:ss strings. The mask maps Su-Sa to char positions 1-7.
-- A day's char position is set to values of  '1' or '0' to indicate whether the 
-- blast time window was active or inactive for that day. Example mask strings:
-- '1111111' all days, '0111110' weekdays only, 0111111 not Sunday
--
-- For certain cases with a 1 hour daylight saving time ambiguity and/or the possibility of
-- a UTC day change you may need to insert a second qb_time(days,stime,etime) object
-- entry into a gazetteerquarry row sched column nested table. For example, a quarry blasting
-- between 3:30 PM and 5:30 PM on Fridays in California year round would have the equivalent UTC
-- time window of 22:30 Friday to 01:30 Saturday, so the SCHED column value would be specified as:
-- qb_schedule(qb_time('0000010','22:30:00','23:59:59'), qb_time('0000001', '00:00:00','01:30:00'))
--
-- In an example row insert for gazid=1234 this would done as:
-- insert into gazetteerquarry
--  (gazid, ondate, offdate, radius, mdepth, mmag, sched, tflg, remark) values
--  (1234, to_date('1932/01/01','YYYY/MM/DD'),to_date('3000/01/01','YYYY/MM/DD'), 5.00, 
--  15., 3.2, qb_schedule(qb_time('0000010','22:30:00','23:59:59'),qb_time('0000001', '00:00:00','01:30:00'))
-- 1,'open-pit mine');
--
-- If a blasting schedule is defined for a quarry, for it to be used by the functions condition filter,
-- you must also set gazetteerquarry.tflg=1. The gazetteerquarry table schedule field is ignored by
-- the function test logic when tflg is NULL or != 1. Set tfg to NULL or 0 if the row's SCHED value is NULL.
--
--------------------------------------------------------------------------------------------
-- Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
-- Re-initialize package quarry list with current quarry table data (in case of updates).
  PROCEDURE init_list;
--------------------------------------------------------------------------------------------
-- Outputs a summary text listing of the package quarries.
  PROCEDURE dump_list;
--------------------------------------------------------------------------------------------
-- Return gazid of best fitting quarry, 0 if no match.
  FUNCTION isquarryevid(p_evid EVENT.EVID%TYPE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------------------
-- Return gazid of best fitting quarry, 0 if no match.
  FUNCTION isquarryorid(p_orid ORIGIN.ORID%TYPE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------------------
-- Return gazid of best fitting quarry, 0 if no match.
  FUNCTION isquarry(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE,
                    p_time ORIGIN.DATETIME%TYPE, p_depth ORIGIN.DEPTH%TYPE,
                    p_mag NETMAG.MAGNITUDE%TYPE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------------------
-- Return gazid of closest quarry, 0 if no match.
  FUNCTION closestquarry(p_orid ORIGIN.ORID%TYPE) RETURN PLS_INTEGER;
  FUNCTION closestquarry(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE) RETURN PLS_INTEGER;
  FUNCTION closestquarry(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE,
                         p_dist OUT NUMBER , p_name OUT VARCHAR2 ) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------------------
-- Return name of closest quarry, NULL if no match.
  FUNCTION closestquarryname(p_orid ORIGIN.ORID%TYPE) RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
  FUNCTION closestquarryname(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE) RETURN VARCHAR2;
--------------------------------------------------------------------------------------------
  PROCEDURE setDebugOn;
  PROCEDURE setDebugOff;
--------------------------------------------------------------------------------------------
END quarry;
```