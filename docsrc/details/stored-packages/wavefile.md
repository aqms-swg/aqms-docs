# WAVEFILE PACKAGE {#wavefile-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE wavefile AUTHID CURRENT_USER AS 
   PKG_SPEC_ID VARCHAR2(80) := '$Id: wavefile_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Functions return path, filename, or other data descriptive of a waveform archive
-- Function inputs are data descriptive of the archive type.
--
-- Contact: Ellen Yu SCEDC, Stephane Zuzlewski at NCEDC
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   Package WAVE
--   Java org.trinet.jasi.TN.WaveformTN
--
-- <DEPENDS ON>
--   Package TRUETIME
--   Tables AssocWaE, Filename, Waveform, Subdir, Waveroots in parametric schema
--   User Oracle type ID_TABLE, defined to be table of number
--
TYPE PATH_REC IS RECORD ( wcopy NUMBER(6), filepath VARCHAR2(132));
TYPE PATH_TAB IS TABLE OF PATH_REC;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return only the name of file containing the waveform specified by input Waveform.wfid.
-- Example: call WAVEFILE.dfile(1111) into :v_string;
  FUNCTION dfile(p_wfid Waveform.wfid%TYPE) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return Event.evid associated with input waveform id (Waveform.wfid) in AssocWaE table.
-- If multiple evid are associated with wfid return the evid of earliest loaded row.
-- Example: call WAVEFILE.getHotEvid(1111) into :v_number;
  FUNCTION getHotEvid(p_wfid Waveform.wfid%TYPE) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return the complete pathname of the file specified by input Waveform.wfid whose WAVEFORM
-- table row column values match those of a WAVEROOT table row whose wcopy column value is 1
-- and whose net column value either matches exactly or is '*'.
-- Example: call WAVEFILE.name(1111) into :v_string;
  FUNCTION name(p_wfid Waveform.wfid%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return the complete pathname of the file specified by input Waveform.wfid whose WAVEFORM
-- table row column values match those of a WAVEROOT table row whose wcopy column value matches 
-- the input parameter value and whose net column value either matches exactly or is '*'.
  FUNCTION name(p_wfid Waveform.wfid%TYPE, p_copy Waveroots.wcopy%TYPE) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return only the directory part of the pathname of the file specified by input Waveform.wfid
-- whose WAVEFORM table row column values match those of a WAVEROOT table row whose wcopy column
-- value is 1 and whose net column value either matches exactly or is '*'.
-- Example: call WAVEFILE.path(1111) into :v_string;
  FUNCTION path(p_wfid Waveform.wfid%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return only the directory part of the pathname of the file specified by input Waveform.wfid
-- whose WAVEFORM table row column values match those of a WAVEROOT table row whose wcopy column
-- value matches the input parameter value and whose net column value either matches exactly or is '*'.
  FUNCTION path(p_wfid Waveform.wfid%TYPE, p_copy Waveroots.wcopy%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
-- Return all wcopy, complete filepath values associated with the input waveform id.
-- Usage: select wcopy,filepath from table (wavefile.wcopypathsByWfid(:p_evid)) order by 1,2;
  FUNCTION wcopypathsByWfid(p_wfid Waveform.wfid%TYPE) RETURN PATH_TAB PIPELINED;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return all wcopy, filepath values associated with the input waveform id.
-- Usage: select distinct wcopy,filepath from table (wavefile.wcopypathsByFileid(:p_fileid)) order by 1,2;
  FUNCTION wcopypathsByFileid(p_fileid Filename.fileid%TYPE) RETURN PATH_TAB PIPELINED;
-- ---------------------------------------------------------------------------------
--
-- Return ALL wcopy, filepath values from WAVEROOTS for all waveforms associated with input evid.
-- Usage: select distinct wcopy,filepath from table (wavefile.wcopypathsByEvid(:p_evid)) order by 1,2;
  FUNCTION wcopypathsByEvid(p_evid Event.evid%TYPE) RETURN PATH_TAB PIPELINED;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- A helper function used by other pipelined functions. Input cursor must return collection of wfid.
-- Not intended for general user.
  FUNCTION wcopypathsByIdX(p_c SYS_REFCURSOR) RETURN PATH_TAB PIPELINED;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
-- Return the directory part of the pathname of the waveform file associated with input Event.evid.
-- The archive is specified by a WAVEROOT table row whose values match those of a WAVEFORM table row
-- and whose wcopy column value is 1, and whose net column value either matches exactly or is '*'.
-- Example: call WAVEFILE.pathByEvid(2222) into :v_string;
  FUNCTION pathByEvid(p_evid Event.evid%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
-- Return the directory part of the pathname of the waveform file associated with input Event.evid.
-- The archive is specified by a WAVEROOT table row whose values match those of a WAVEFORM table row
-- and whose wcopy column matched the input parameter and whose net column value either matches
-- exactly or is '*'.
  FUNCTION pathByEvid(p_evid Event.evid%TYPE, p_copy Waveroots.wcopy%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
-- Return the root directory path of the waveform file archive whose WAVEROOT table row 
-- matches the specified input parameters and is valid (active) for the current time
-- and whose wave_fmt column values is 2 (miniseed), whose wcopy column value is 1,
-- and whose net column value matches exactly or is '*'.
-- Null input net defaults to '*'.
  FUNCTION root(p_net Waveform.net%TYPE,
                p_archive Waveform.archive%TYPE,
                p_wavetype Waveform.wavetype%TYPE,
                p_status Waveform.status%TYPE
               ) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return the root directory path of the waveform file archive whose WAVEROOT table row 
-- matches the specified input parameters and is valid (active) for the current time
-- and whose wcopy column value is 1, and whose net column value matches exactly or is '*'.
-- Null input net defaults to '*'.
  FUNCTION root(p_net Waveform.net%TYPE,
                p_archive Waveform.archive%TYPE,
                p_wavetype Waveform.wavetype%TYPE,
                p_status Waveform.status%TYPE,
                p_fmt Waveform.wave_fmt%TYPE
               ) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return root directory path of the file archive whose WAVEROOT table row matching the
-- specified by input parameters, whose net column value matches exactly or is '*'.
-- Null input net defaults to '*'.
-- Null input datetime corresponds to those rows active for current time, i. e.
-- rows whose datetime_on to datetime_off range includes the time.
-- input datetime time should be same time base as database (true or nominal)
-- Example: call WAVEFILE.root('BK', 'NCEDC', 'T', 'A', 2, NULL, 1) into :v_string;
  FUNCTION root(p_net Waveform.net%TYPE,
                p_archive Waveform.archive%TYPE,
                p_wavetype Waveform.wavetype%TYPE,
                p_status Waveform.status%TYPE,
                p_fmt Waveform.wave_fmt%TYPE,
                p_datetime Waveform.datetime_on%TYPE,
                p_copy Waveroots.wcopy%TYPE
               ) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return count of WAVEROOT table rows matching specified the input parameters,
-- where net column value matches exactly or is '*', (i.e. rows having different wcopy values).
-- Null input net defaults to '*'.
-- Null input datetime corresponds to those rows active for current time.
-- input datetime time should be same time base as database (true or nominal)
-- Example: call WAVEFILE.rootCount('BK', 'NCEDC', 'T', 'A', 2, NULL) into :v_number;
  FUNCTION rootCount(p_net Waveform.net%TYPE,
                     p_archive Waveform.archive%TYPE,
                     p_wavetype Waveform.wavetype%TYPE,
                     p_status Waveform.status%TYPE, 
                     p_fmt Waveform.wave_fmt%TYPE,
                     p_datetime Waveform.datetime_on%TYPE
                    ) RETURN PLS_INTEGER;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return table of the wcopy number values from the WAVEROOT table rows
-- matching the specified input parameters, where net matches exactly or is '*'.
-- Null input net defaults to '*'.
-- Null input datetime corresponds to those rows active for current time.
-- input datetime time should be same time base as database (true or nominal)
-- Example: select * from TABLE(WAVEFILE.rootCopyIds(NULL, 'SCEDC', 'T', 'A', 2, NULL);
  FUNCTION rootCopyIds(p_net Waveform.net%TYPE,
                       p_archive Waveform.archive%TYPE,
                       p_wavetype Waveform.wavetype%TYPE,
                       p_status Waveform.status%TYPE, 
                       p_fmt Waveform.wave_fmt%TYPE,
                       p_datetime Waveform.datetime_on%TYPE
                      ) RETURN id_table;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Debugging function, prints to server output the WAVEROOTS table row's
-- net, fmt, rootfile and wcopy for input parameters, where net matches exactly or is '*'.
-- Null input net defaults to '*'.
-- Null input datetime corresponds to those rows active for current time.
-- input datetime time should be same time base as database (true or nominal)
-- Example: call WAVEFILE.printRoots(NULL, 'SCEDC', 'T', 'A', 2, NULL);
  PROCEDURE printRoots(p_net Waveform.net%TYPE,
                       p_archive Waveform.archive%TYPE,
                       p_wavetype Waveform.wavetype%TYPE,
                       p_status Waveform.status%TYPE, 
                       p_fmt Waveform.wave_fmt%TYPE,
                       p_datetime Waveform.datetime_on%TYPE
                      );
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END wavefile;
```