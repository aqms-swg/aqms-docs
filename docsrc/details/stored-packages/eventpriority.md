# EVENTPRIORITY PACKAGE {#eventpriority-stored-package}   
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE eventpriority AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: eventpriority_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Used in production: no, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  unknown
--
-- <DEPENDS ON>
--  Depends on ancillary table EventPriorityParams
--  Java class 'org.trinet.storedprocs.CatEntry.class (loaded on database server)
--  Java class 'org.trinet.storedprocs.DbaseConnection.class (loaded on database server)
--
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
--  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return prority value for an event with these characteristics.
    -- Priority value decreases with age and increases with event magnitude,
    -- lower origin quality events have higher priority
    -- Input event origin time UTC epoch seconds is converted to elapsed age in days. 
    -- Example: call EVENTPRIORITY.getPriority(4.5, 104000034., 1.) into :v_priority;
    FUNCTION getPriority(p_mag NUMBER, p_origintime NUMBER, p_quality NUMBER) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return prority value for this event, returns 0.0 if no such event in dbase. 
    -- Wrapper invokes above function
    -- Example: call EVENTPRIORITY.getPriority(1111) into :v_priority;
    FUNCTION getPriority(p_evid NUMBER) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the value of the magFactor.
    -- Example: call EVENTPRIORITY.getMagFactor() into :v_factor;
    FUNCTION getMagFactor RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the value of the ageFactor.
    -- Example: call EVENTPRIORITY.getAgeFactor() into :v_factor;
    FUNCTION getAgeFactor RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the value of the qualFactor.
    -- Example: call EVENTPRIORITY.getQualFactor() into :v_factor;
    FUNCTION getQualFactor RETURN NUMBER;
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END eventpriority;
```