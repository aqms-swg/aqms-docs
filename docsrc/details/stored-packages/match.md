# MATCH PACKAGE {#match-stored-package}
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE match AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: match_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- --------------------------------------------------------------------------------
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   org.trinet.jasi.Solution.class, org.trinet.jiggle.Jiggle 
--
-- <DEPENDS ON > (loaded on server)
-- NCEDC Parametric tables Event, Origin, AssocArO, Arrival and EventMatchPassingScore
-- references Wheres package
-- Java org.trinet.util.gazetteer.GeoidalConvert is referenced by Wheres package
-- --------------------------------------------------------------------------------
--
-- Package is used when an event is saved (committed) to the database
-- to determine when two events in database are possibly the same (duplicates).
-- Events are "duplicates" when their comparison score is less then or equal
-- to the value of a set score value that is derived from criteria stored 
-- in table columns of a table named EventMatchPassingScore.
--
-- --------------------------------------------------------------------------------
--
-- Input parameters:
--     p_lat is the lat decimal degees of the preferred Origin
--     p_lon is the lon decimal degees of the preferred Origin
--     p_datetime is the UTC epoch time seconds of the preferred Origin
--
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
    -- Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
    --
    -- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Find by lat,lon,time, and origin subsource a evid different from but matching input evid
    -- (where events's selectFlag = 1 and prefors's subsources must match if p_match_src > 0).
    -- Returns 0 if no matching evid is found, the evid of a matching event, or error code.
    FUNCTION getMatch(p_evid NUMBER, p_match_src NUMBER) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Find by lat,lon,time, and origin subsource a evid different from but matching input evid
    -- (where events's selectFlag = 1 and prefors's subsources don't have to match).
    FUNCTION getMatch(p_evid NUMBER) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Find matching evid by lat,lon,time (the origin subsources don't have to match).
    -- Returns 0 if no matching evid is found, the evid of a matching event, or error code.
    -- Example: call MATCH.getMatch(36.,-118.,1009843200) into :v_evid;
    FUNCTION getMatch(p_auth VARCHAR2, p_lat NUMBER, p_lon NUMBER,
                      p_datetime NUMBER) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Find matching evid by lat,lon,time, and origin subsource
    -- (subsources don't have to match if input subsource := NULL).
    -- Returns 0 if no matching evid is found, the evid of a matching event, or error code.
    FUNCTION getMatch(p_auth VARCHAR2, p_lat NUMBER, p_lon NUMBER,
                      p_datetime NUMBER, p_subsource VARCHAR2) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    FUNCTION getMatch(p_evid NUMBER, p_orid NUMBER, p_auth VARCHAR2, p_lat NUMBER, p_lon NUMBER,
                      p_datetime NUMBER, p_subsource VARCHAR2) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Calculate the score based upon deltaOriginTime, epicentral separation, and/or
    -- the median of arrivals times differences. Difference units are time seconds
    -- and kilometers distance.
    -- Example: call MATCH.getScore(111,222) into :v_score;
    FUNCTION getScore(p_evid1 NUMBER, p_evid2 NUMBER) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Set the origin time search window bounds to +/- the input p_secs of 
    -- any input event origin time for which to search for any duplicates.
    -- The default value is 60.0 seconds.
    PROCEDURE setWindowSize(p_secs NUMBER);
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Return current value derived from the database criteria for the passingScore.
    -- Example: call MATCH.getPassingScore('CI') into :v_score;
    FUNCTION getPassingScore(p_auth VARCHAR2) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END match;
```