# DB_CLEANUP PACKAGE {#db_cleanup-stored-package}  
  
## Specification  
  
```
--CALL schema_util.revoke_all_privs('DB_CLEANUP');
--
CREATE OR REPLACE PACKAGE db_cleanup AUTHID CURRENT_USER AS
--
-- Used in production: yes, UTC version
--
-- Package of PL/SQL procedures that delete table rows associated with specific event or origin ids.
-- This package must be installed in the user schema that owns the tables (e.g. TRINETDB), not the
-- CODE schema, as are the other packages.
--
-- Principal User:
--  Database Administrator to remove all records of unselected event "triggers"
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   no Java classes reference this
--   Package COPYDATA refs delete_event function
--
-- <DEPENDS ON>
--   refs package PCS installation on server
--   Java class org.trinet.util.FileUtil.class loaded on server
--
--------------------------------------------------------------------------------
    v_pkg_defaultState VARCHAR2(16)    := 'TRIGDEL';  -- package default pcs posting state
    v_pkg_defaultRank  PLS_INTEGER     := 100;        -- package default pcs posting rank
    --
    v_pkg_verbose      PLS_INTEGER     := 1;          -- package default logging level
    v_pkg_trace        BOOLEAN         := FALSE;      -- package default logging level
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Package initialize checks for user table names used by package
    PROCEDURE init;
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    --  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Set trace debugging features on default = false
    PROCEDURE set_trace(p_trace BOOLEAN DEFAULT FALSE);
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Set output verbosity for debugging; default = 1, [0, 1, 2, 3]
    PROCEDURE set_verbosity(p_level PLS_INTEGER DEFAULT 1);
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Turn global wavefile deletion on/off, default is TRUE
    PROCEDURE set_wavefile_delete(p_delete_files BOOLEAN DEFAULT TRUE);
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Turn global wavefile deletion on/off, default is TRUE
    PROCEDURE set_wavefile_delete(p_delete_files PLS_INTEGER DEFAULT 1);
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes data associated uniquely with any triggered event originating 
    -- on the specified input date and not selected for archiving.
    -- returns number of triggers deleted.
    FUNCTION delete_triggers(p_date DATE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes data associated uniquely with any triggered event originating 
    -- on the specified input date and not selected for archiving.
    -- returns number of triggers deleted.
    -- Input string FORMAT 'YYYY/MM/DD'
    FUNCTION delete_triggers(p_date VARCHAR2) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes data associated uniquely with any triggered event originating
    -- within the input date range inclusively and not selected for archiving.
    -- returns number of triggers deleted.
    FUNCTION delete_triggers(p_startDate DATE, p_endDate DATE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes data associated uniquely with any triggered event originating
    -- within the input date range inclusively and not selected for archiving.
    -- returns number of triggers deleted.
    -- Input string FORMAT 'YYYY/MM/DD'
    FUNCTION delete_triggers(p_startDate VARCHAR2, p_endDate VARCHAR2) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes all database data associated uniquely with the input event id parameter.
    -- only if the event was not selected for archiving.
    -- Includes event and origin table associations and the triggered waveform data files.
    -- returns number of rows deleted
    FUNCTION delete_trigger(p_evid event.evid%TYPE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes all database data associated uniquely with the input event id parameter.
    -- Includes event and origin table associations and the triggered waveform data files.
    -- NOTE THIS FUNCTION WILL DELETE DATA FOR EVENTS SELECTED FOR ARCHIVING !!!
    -- returns number of rows deleted
    FUNCTION delete_event(p_evid event.evid%TYPE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    -- Deletes all waveform related table rows and waveform file FROM disk.
    -- Check file existance before attempting to delete it.
    FUNCTION delete_waveforms(p_evid event.evid%TYPE) RETURN PLS_INTEGER;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
END db_cleanup;
```