# ASSOCAMP PACKAGE {#assocamp-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE assocamp AUTHID DEFINER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: assocamp_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- User DEFINER For permission to invoke newInstance to create Jasi TN subtypes -aww 05/02/2005
-- Uses objects (or synonyms) defined on compilation in the CODE name space not USER's objects
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   Java class org.trinet.storedprocs.association.AmpAssocEngine.class (prepared statement)
--   Java class org.trinet.apps.Gmp2Db.class (expects above class to be loaded into server)
--
-- <DEPENDS ON>
--   NCEDC parametric schema tables:
--   Event, Origin, AssocAmO, Amp, UnassocAmp, AmpSet, AssocEvAmpSet, and AmpSetTypes
--   Java class org.trinet.storedprocs.association.AmpAssocEngine.class (refs loaded into server)
--   Java class org.trinet.storedprocs.association.AmpAssoc.class (refs loaded into server)
--
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    --  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
    --
    -- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Return the Event ID of the best associated event. Returns 0 if no match.
    FUNCTION getAssocEvent(p_net      VARCHAR2,
                           p_sta      VARCHAR2,
                           p_seedchan VARCHAR2,
                           p_loc      VARCHAR2,
                           p_datetime NUMBER,
                           p_ampvalue    NUMBER) RETURN NUMBER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Return the Event ID of the best associated event. Returns 0 if no match.
    FUNCTION getAssocEvent(p_ampid NUMBER) RETURN NUMBER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Associate Amp once the correct EVID is determined
    -- This means:
    --  1) Create new ampset if no valid set exists for specified evid, ampsettype, and subsource
    --  2) Copy amp row FROM UnassocAmp table to Amp table
    --  3) Insert Ampset row 
    --  4) Insert AssocAmO row using event's prefor
    --  5) Delete old UnassocAmp
    --  6) Commit the changes
    -- Returns ampsetid on success, return <= 0 for bad input or SQL error.
    FUNCTION MoveAndAssoc(p_ampid Amp.ampid%TYPE,
                          p_evid Event.evid%TYPE,
                          p_subsrc Amp.subsource%TYPE,
                          p_ampsettype AssocEvAmpset.ampsettype%TYPE,
                          p_dist AssocAmo.delta%TYPE,
                          p_az AssocAmo.seaz%TYPE) RETURN NUMBER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Associate Amp once the correct EVID is determined
    -- This means:
    --  1) Create new ampset if no valid set exists for specified evid, ampsettype, and subsource
    --  2) Copy amp row FROM UnassocAmp table to Amp table
    --  3) Insert Ampset row 
    --  4) Insert AssocAmO row using input orid
    --  5) Delete old UnassocAmp
    --  6) Commit the changes if p_commit > 0, else no commit
    -- Returns ampsetid on success, return <= 0 for bad input or SQL error.
    FUNCTION MoveAndAssoc(p_ampid Amp.ampid%TYPE,
                          p_evid Event.evid%TYPE,
                          p_orid Event.prefor%TYPE,
                          p_subsrc Amp.subsource%TYPE,
                          p_ampsettype AssocEvAmpset.ampsettype%TYPE,
                          p_dist AssocAmo.delta%TYPE,
                          p_az AssocAmo.seaz%TYPE,
                          p_commit PLS_INTEGER) RETURN NUMBER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Wrapper around above function to allow "executeBatch" call in Oracle JDBC PreparedStatement.
    -- Batching works for procedures with inputs only, no outputs, like functions returning values.
    -- Does not commit, user must do that add batching to save changes.
    PROCEDURE MoveAndAssocAmp(p_ampid Amp.ampid%TYPE,
                              p_evid Event.evid%TYPE,
                              p_orid Event.prefor%TYPE,
                              p_subsrc Amp.subsource%TYPE,
                              p_ampsettype AssocEvAmpset.ampsettype%TYPE,
                              p_dist AssocAmo.delta%TYPE,
                              p_az  AssocAmo.seaz%TYPE);
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Delete duplicate UnassocAmp rows. Compares net, sta, seedchan, amptype, amplitude, and lddate.
    FUNCTION removeDuplicateUnassocAmps RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Reset to value of Origin.totalamps to the actual count of associated amps.
    FUNCTION updateTotalAmp(p_orid Origin.orid%TYPE) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Delete amps FROM UnassocAmp table that are older than this (days).
    FUNCTION cleanUp(p_daysback NUMBER) RETURN NUMBER;
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
    -- Delete amps FROM UnassocAmp table that are older than the default age. (7 days)
    FUNCTION cleanUp RETURN NUMBER;
-- ------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Returns ampsetid of the row in the assocevampset table with isvalid=1 and whose ampsettype
--  and subsource matches the input, the subsource can contain wildcards and is used in LIKE
--  match condition -- (e.g. subsource LIKE 'ampgen%')
--  Returns ampsetid on success, else 0 if none, or <0 if error
    FUNCTION getValidAmpSetId(p_evid AssocEvAmpset.evid%TYPE,
                            p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                            p_subsrc AssocEvAmpset.subsource%TYPE
                          ) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Returns a valid ampsetid for a row in the assocevampset table where isvalid=1 and whose ampsettype
--  and subsource matches the literal input, if a valid set as described by input does not already exist
--  it inserts a new row in the assocevampset table and commits it if p_commit flag is 1.
--  Returns ampsetid on success, =<0, if error.
    FUNCTION getValidAmpSetId(p_evid AssocEvAmpset.evid%TYPE,
                            p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                            p_subsrc AssocEvAmpset.subsource%TYPE,
                            p_commit PLS_INTEGER
                          ) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Creates an new ampsetid row matching specified inputs in assocevampset table
--  Commits insert if p_commit > 0
--  Returns the new ampsetid on success, else returns <= 0 if error.
    FUNCTION insertValidAmpSet(p_evid AssocEvAmpset.evid%TYPE,
                               p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                               p_subsrc AssocEvAmpset.subsource%TYPE,
                               p_commit PLS_INTEGER
                              ) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Set validity flag false (0) for specified ampsettype in assocevampset, if subsrc in null all subsources.
--  Otherwise subsource can contain wildcards and is used in a LIKE match condition (e.g LIKE 'ampgen%')
--  Commits changes if p_commit > 0
--  Returns count of sets invalidated on success, else <= 0
    FUNCTION invalidateAllAmpSets(p_evid AssocEvAmpset.evid%TYPE,
                                  p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                                  p_subsrc AssocEvAmpset.subsource%TYPE,
                                  p_commit PLS_INTEGER
                                 ) RETURN PLS_INTEGER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Associate amp with a valid set for specified event, ampsettype, and subsource,
--  and if no set exists creates new one. Commits changes if p_commit > 0.
--  Returns ampsetid on success, else <= 0
    FUNCTION assocAmpWithValidSet(p_ampid AmpSet.ampid%TYPE,
                                  p_evid AssocEvAmpset.evid%TYPE,
                                  p_ampsettype AssocEvAmpset.ampsettype%TYPE, 
                                  p_subsrc AssocEvAmpset.subsource%TYPE,
                                  p_commit PLS_INTEGER) RETURN NUMBER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  Associate amp with a specified ampsetid.
--  Commits changes if p_commit > 0.
--  Returns ampsetid on success, else <= 0.
    FUNCTION assocAmpWithSet(p_ampid AmpSet.ampid%TYPE,
                             p_ampsetid AmpSet.ampsetid%TYPE, 
                             p_commit PLS_INTEGER) RETURN NUMBER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  Delete all amp, ampset, assocevampset row(s) associated with the input ampsetid,
--  and the assocamo rows associated with these amps.
    FUNCTION deleteAmpSet(p_ampsetid AssocEvAmpset.ampsetid%TYPE) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  Delete all amp, ampset, and assocevampset row(s) associated with the input evid
--  and the assocamo rows associated with these amps.
    FUNCTION deleteAllAmpSets(p_evid AssocEvAmpset.evid%TYPE) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  Delete all amp, ampset, and assocevampset row(s) associated with the input evid
--  and the assocamo rows associated with these amps where the assocevampset 
--  ampsetype equals p_type.
    FUNCTION deleteAllAmpSets(p_evid AssocEvAmpset.evid%TYPE,
                              p_type AssocEvAmpset.ampsettype%TYPE) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  Delete all amp, ampset, and assocevampset row(s) associated with the input evid 
--  and the assocamo rows associated with these amps where the assocevampset
--  isvalid column value is 0.
    FUNCTION deleteAllInvalidAmpSets(p_evid AssocEvAmpset.evid%TYPE) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  Delete all amp, ampset, and assocevampset row(s) associated with the input evid 
--  and the assocamo rows associated with these amps where the assocevampset isvalid
--  column value is 0 and ampsetype equals p_type.
    FUNCTION deleteAllInvalidAmpSets(p_evid AssocEvAmpset.evid%TYPE,
                                     p_type AssocEvAmpset.ampsettype%TYPE) RETURN PLS_INTEGER;
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
END assocamp;
```