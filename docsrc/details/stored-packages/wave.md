# WAVE PACKAGE {#wave-stored-package}
  
# Specification  
  
```
CREATE OR REPLACE PACKAGE wave AUTHID DEFINER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: wave_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- Problem with Java access to create "newInstance" for WFSegment TimeSeries Channel object
-- requires package rights as DEFINER vs CURRENT_USER in order to create these objects. -aww
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  WAVE.getWaveformBlob(p_filename...) is invoked in SQL query by method in:
--  Java org.trinet.jasi.seed.DbBlobReader
--  Jiggle application and other apps invoke WaveformTN methods
--  to load waveforms from database archive via SeedReader methods
--  which invoke DbBlobReader method.
--
--  org.trinet.jasi.DataSource utcCompliant method invokes isJavaUTC function
-- 
--  Jiggle uses to read waveforms into its GUI from local miniseed files.
--
--- <PACKAGE DEPENDS ON >
--  refs package WAVEFILE installation
--  Java org.trinet.waveserver.dc.WaveformBLOB (on server, refs classes below )
--  Java org.trinet.jasi.seed.SeedReader (on server)
--  Java org.trinet.jasi.seed.SeedHeader (on server)
--  All other Java classes included by above classes
--
-- SPECIFICATION:
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, waveform timeseries SEED mini-packets,
-- that exist within the bounds of the input timespan for the input waveform identifier.
-- The data file location is recovered by SQL query of waveform associated parametric tables.
-- Filename path is that from the WAVEROOTS table whose row wcopy column value is 1.
  FUNCTION get_waveform_blob(p_wfid INTEGER, p_startTime NUMBER, p_endTime NUMBER) RETURN BLOB;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Like above except WAVEROOTS table row wcopy column value matches the first input parameter.
  FUNCTION get_waveform_blob(p_copy INTEGER, p_wfid INTEGER, p_startTime NUMBER, p_endTime NUMBER) RETURN BLOB;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, wavefrom timeseries SEED mini-packets,
-- that exist within the bounds of input timespan, for the input event id (evid), seedchan, 
-- and as well as location, if input channel location parameter is not null.
-- The data file location is recovered by SQL query of waveform associated parametric tables.
-- Filename path is that from the WAVEROOTS table whose row wcopy column value is 1.
  FUNCTION get_waveform_blob(p_evid INTEGER, p_net VARCHAR2, p_sta VARCHAR2, p_chan VARCHAR2,
               p_loc VARCHAR2, p_startTime NUMBER, p_endTime NUMBER) RETURN BLOB;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Like above except WAVEROOTS table row wcopy column value matches the first input parameter.
  FUNCTION get_waveform_blob(p_copy INTEGER, p_evid INTEGER, p_net VARCHAR2, p_sta VARCHAR2,
               p_chan VARCHAR2, p_loc VARCHAR2, p_startTime NUMBER, p_endTime NUMBER) RETURN BLOB;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, wavefrom timeseries SEED mini-packets,
-- that exist within the bounds of the input timespan, from the named input file,
-- starting data retrieval at the specified byte offset, for the specified length.
-- The starting and ending bytes must be the beginning and the end of valid packets. 
  FUNCTION get_waveform_blob(p_filename VARCHAR2, p_traceoff NUMBER, p_nbytes NUMBER,
                             p_startTime NUMBER, p_endTime NUMBER) RETURN BLOB;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, waveform timeseries SEED mini-packets,
-- from the named input file starting at the specified byte offset, for 
-- the specified total length in bytes.
-- The starting and ending bytes must be at the beginning and end of valid packets. 
  FUNCTION get_waveform_blob(p_filename VARCHAR2, p_traceoff NUMBER, p_nbytes NUMBER) RETURN BLOB;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- The database stored java is the UTC version 
  FUNCTION isJavaUTC RETURN NUMBER;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
END wave;
```