# COPYDATA PACKAGE {#copydata-stored-package}  
  
## Specification  
  
  
```
CREATE OR REPLACE PACKAGE copydata AUTHID CURRENT_USER AS
--
-- Procedures/Functions needed to copy all NCEDC parametric table data
-- associated with a event id (evid) from the specified database link 
-- and owning schema to the like named tables of the user's login schema.
--
-- Used in production: no, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   NONE
--
-- <DEPENDS ON>
--   All NCEDC parametric tables
--
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Level of processing info to output to log:
  --   0= only errors (or missing data), and event summary
  --   1= above plus per table totals logging
  --   2= above plus duplicate row key insert attempts
  PROCEDURE set_verbosity(p_ilevel PLS_INTEGER);
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Copy to user login schema all event data associated with the specified input
  -- event id in the named input owning schema at the named input dbLink.
  -- Example: call COPYDATA.copy_event(1111, 'TRINETDB', 'IRONDB') into :v_status;
  FUNCTION copy_event(p_eid NUMBER, p_own VARCHAR2, p_dbLinkName VARCHAR2) RETURN PLS_INTEGER;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Initialize package variables, checks user schema for all tables required by procedures/functions
  PROCEDURE init;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Output a string of the form  'You are: <userName> at <databaseName>' 
  -- Example: call dbms_output.put_line(COPYDATA.whoami());
  FUNCTION whoami RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  /*
  FUNCTION copy_comment(p_cid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_credit(p_cid NUMBER, p_table VARCHAR2) RETURN PLS_INTEGER;
  FUNCTION copy_evt_origins(p_eid NUMBER, p_eprefoid NUMBER, p_eprefmid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_org_netmags(p_eid NUMBER, p_oid NUMBER, p_eprefoid NUMBER,
      p_oprefmid NUMBER, p_eprefmid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_org_arrivals(p_oid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_org_codas(p_oid NUMBER) RETURN PLS_INTEGER; 
  FUNCTION copy_org_amps(p_oid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_mag_codas(p_mid NUMBER, p_oid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_mag_amps(p_mid NUMBER, p_oid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_evt_waveforms(p_eid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_wf_filenames(p_eid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_evt_prefmags(p_eid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_mag(p_mid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_mec(p_mid NUMBER) RETURN PLS_INTEGER;
  FUNCTION copy_prefmag(p_eid NUMBER, p_oid number) RETURN PLS_INTEGER; // NEED new routine for PrefMag
  */
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END copydata;
```