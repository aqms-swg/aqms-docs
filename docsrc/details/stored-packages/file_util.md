# FILE_UTIL PACKAGE {#file_util-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE file_util AUTHID CURRENT_USER IS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: file_util_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Used in production: no, UTC version
--
-- Package of PL/SQL functions to:
-- 1) delete or create a disk files on local machine file system.
-- 2) get values of Java System properties. 
-- Note: java access permissions must be configured on server to allow file i/o, System properties i/o.
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   no stored Java classes reference this package
--   no stored PL/SQL packages reference this package
--
-- <DEPENDS ON> (loaded on server)
--    Java class org.trinet.util.FileUtil.class
--
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
--  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Delete the disk file described by an input path string.
    -- Returns 1 if successful, 0 if no such file, value < 0 if error or exception.
    -- Example: call FILE_UTIL.delete_file('C:\temp\garbage.log') into :v_status;
    FUNCTION delete_file(p_name VARCHAR2) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Create an empty disk file described by an input path string.
    -- Returns 1 if successful, 0 if file already exists, value < 0 if error or exception.
    -- Example: call FILE_UTIL.create_file('C:\temp\garbage.log') into :v_status;
    FUNCTION create_file(p_name VARCHAR2) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return value Java System property user.dir, the working directory.
    -- Returns null if no such named property is defined or access exception is thrown. 
    -- Example: call FILE_UTIL.user_dir into :v_string;
    FUNCTION user_dir  RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return value Java System property user.home.
    -- Returns null if no such named property is defined. 
    -- Example: call FILE_UTIL.user_home() into :v_string;
    FUNCTION user_home RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return value of Java System property whose name matches input string.
    -- Returns null if no such named property is defined or access exception is thrown. 
    -- Example: call get_java_sys_prop('user.dir') into :v_string
    FUNCTION get_java_sys_prop(propName VARCHAR2) RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END file_util;
```