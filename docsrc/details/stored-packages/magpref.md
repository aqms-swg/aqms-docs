# MAGPREF PACKAGE {#magpref-stored-package}
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE magpref AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: magpref_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Functions for determining the priority of a Magnitude.
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  Java class org.trinet.jiggle.Jiggle.class (references Magnitude abstract method)
--  Java class org.trinet.jasi.TN.MagnitudeTN.class (method prepared statement ref)
--  Java class org.trinet.jasi.Magnitude.class (references MagnitudeTN method)
--  Java class org.trinet.jasi.TN.Solution.class (references Magnitude abstract method)
--  Java class org.trinet.jasi.TN.SolutionTN.class (method prepared statement ref)
--  Stored Package EPREF 
--  tpp/bin/perlmodules/Magnitude.pm
-- <DEPENDS ON>
--   NCEDC parametric schema tables Event, Origin, Netmag, EventPrefMag
--   and tables Gazetteer_Region and MagPrefPriority
--   Stored Package EPREF installed
--     
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
    --
    -- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns max priority value for a magnitude described by the input values using the rule
    -- constraints found in the MagPrefPriority table for the input region name key and datetime.
    -- Rules applied are those enabled for the specied input datetime; input datetime is the event's
    -- origin time in epoch seconds.
    -- Returns -1 if no matching priority constraints are found for the input.
    -- Example: call MAGPREF.getMagPriority('CI', 'l','CI','jiggle','SoCalMl', 50, 3., 0.1) into :v_priority;
    FUNCTION getMagPriority( 
                             p_region      Gazetteer_Region.name%TYPE,
                             p_datetime    Origin.datetime%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- As above, except only use the magnitude priority rules enabled for the current system time.
    FUNCTION getMagPriority( 
                             p_region      Gazetteer_Region.name%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns max priority value for the magnitude described by input values.
    -- Input datetime is the event's origin time in epoch seconds; rules used are
    -- those enabled for this datetime.  Input lat, lon should be inside one
    -- or more regions defined in Gazetteer_Region table that also have priority
    -- constraints declared in the MagPrefPriority table, otherwise rules matching
    -- MagPrefPriority.region_name = 'DEFAULT' are used.
    -- Returns -1 if lat/lon are NULL or no matching priority is found in table.
    FUNCTION getMagPriority(
                             p_datetime    Origin.datetime%TYPE,
                             p_lat         Origin.lat%TYPE,
                             p_lon         Origin.lat%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- As above, except only use the magnitude priority rules enabled for the current system time.
    FUNCTION getMagPriority(
                             p_lat         Origin.lat%TYPE,
                             p_lon         Origin.lat%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns max priority value for the magnitude matching Netmag.magid.
    -- Input datetime is the event's origin time in epoch seconds; rules used are
    -- those enabled for this datetime. Input lat, lon should be inside one
    -- or more regions defined in Gazetteer_Region table that also have priority
    -- constraints declared in the MagPrefPriority table, otherwise rules matching
    -- MagPrefPriority.region_name = 'DEFAULT' are used.
    -- Returns -1 if lat/lon are NULL or no matching priority is found in table.
    FUNCTION getMagPriority(p_magid NetMag.magid%TYPE, p_datetime Origin.datetime%TYPE,
                            p_lat Origin.lat%TYPE, p_lon Origin.lon%TYPE) RETURN NUMBER;
    --
    -- As above, except only use the magnitude priority rules enabled for the current system time.
    FUNCTION getMagPriority(p_magid NetMag.magid%TYPE, p_lat Origin.lat%TYPE, p_lon Origin.lon%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Return max priority value for the magnitude matching NetMag.magid.
    -- Convenience wrapper around above function, using magnitude's associated
    -- origin's datetime,lat,lon as inputs.
    -- Returns -1 if no matching priority row constraints are found.
    FUNCTION getMagPriority(p_magid NetMag.magid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the max priority value for the magnitude whose NetMag.magid=Event.prefmag
    -- for the Event row matching the input evid value.
    -- Convenience wrapper around above function, using magnitude's associated
    -- origin's datetime,lat,lon as inputs.
    -- Event.prefmag is usually, but DOES NOT have to be, associated with Event.prefor.
    -- Returns -1 if no matching priority row constraints are found.
    -- Example: call MAGPREF.getMagPriorityOfEvent(1111) into :v_priority;
    FUNCTION getMagPriorityOfEvent(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the max priority value for the magnitude described by the NetMag
    -- row whose key equals the magid value in the EventPrefmag table row whose 
    -- whose key matches both the input evid value and magtype.
    -- Convenience wrapper around above function, using magnitude's associated
    -- origin's datetime,lat,lon as inputs.
    -- This magnitude may or may not be the 'event' preferred magnitude (Event.prefmag)
    -- but it DOES NOT have to be associated with the event preferred Origin (prefor).
    -- Returns -1 if no matching priority row constraints are found.
    -- Example: call MAGPREF.getMagPriorityOfPrefMagType(1111,'l') into :v_priority;
    FUNCTION getMagPriorityOfPrefMagType(p_evid Event.evid%TYPE, p_magtype Netmag.magtype%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    --
    -- Return the magid of the highest priority magnitude. If equal priorities,
    -- return new magid (2nd argument), when both priority's are < 0 
    -- (both failed rules) return the old magid (1st argument).
    FUNCTION highest_priority(p_old_magid Netmag.magid%TYPE, p_new_magid Netmag.magid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Calculates a maximum priority for every magnitude associated with the 'prefor'
    -- of the Event row whose key equals the input p_evid.
    -- Set the row's 'Event.prefmag' to the magid having the highest calculated priority 
    -- and also set the Event.prefor's Origin row's 'Origin.prefmag' to this same magid 
    -- if it's associated with the prefor, that is, it's Netmag.orid = Event.prefor.
    -- Commits the transaction if update is successful.
    -- WARNING: If current event.prefmag is NOT associated with the Event.prefor or it
    -- is NOT the prefmag of the prefor (e.g. a Mw mag with a different orid from prefor)
    -- it WILL BE replaced !
    --
    -- Returns:
    --    > 0 updated, prefmag value changed to the returned number
    --    < 0 updated, prefmag value did NOT change, return is -prefmag
    --      0 prefmag could not be updated; no mags, bad evid, etc.
    -- Example: call MAGPREF.setPrefMagOfEventByPrefor(1111) into :v_magid;
    FUNCTION setPrefMagOfEventByPrefor(p_evid Event.evid%TYPE) RETURN NUMBER;
    --
    -- As above, but commits the transaction only if input p_commit > 0.
    FUNCTION setPrefMagOfEventByPrefor(p_evid Event.evid%TYPE, p_commit PLS_INTEGER) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Calculates a maximum priority for every magnitude whose magid is associated
    -- with the input p_evid in the EventPrefMag table.
    -- Set the row's 'Event.prefmag' to the magid having the highest calculated priority 
    -- and also set the Event.prefor's Origin row's 'Origin.prefmag' to this same magid 
    -- if it's associated with the prefor, that is, it's Netmag.orid = Event.prefor.
    -- Commits the transaction if update is successful.
    -- Returns:
    --    > 0 updated, prefmag value changed to the returned number
    --    < 0 updated, prefmag value did NOT change, return is -prefmag
    --      0 prefmag could not be updated; no mags, bad evid, etc.
    -- Example: call MAGPREF.setPrefMagOfEvent(1111) into :v_magid;
    FUNCTION setPrefMagOfEvent(p_evid Event.evid%TYPE) RETURN NUMBER;
    --
    -- As above, but commits the transaction only if the input p_commit > 0.
    FUNCTION setPrefMagOfEvent(p_evid Event.evid%TYPE, p_commit PLS_INTEGER) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns Event.prefmag value of the Event table row whose key matches the input evid.
    -- Returns -1 if Event.prefmag is NULL.
    FUNCTION getPrefMagOfEvent(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the Netmag.magid of the magnitude with the highest priority that is
    -- associated with Event.prefor, or is prefor's prefmag, in which case the
    -- prefmag's orid DOES NOT HAVE TO EQUAL prefor.
    -- Returns 0 if no magnitude is found in table that this satisfies priority rules.
    FUNCTION bestMagidForEventPrefor(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the NetMag.magid of the magnitude with the highest priority that is
    -- associated with the input orid, or is its prefmag, in which case 
    -- the prefmag's orid DOES NOT HAVE TO EQUAL the input p_orid.
    -- Returns 0 if no magnitude is found in table that this satisfies priority rules.
    FUNCTION bestMagidForOriginPref(p_orid Origin.orid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the NetMag.magid of the magnitude with the highest priority that is
    -- associated with the input orid and magtype, or is its prefmag, in
    -- which case the prefmag's orid DOES NOT HAVE TO EQUAL the input p_orid.
    -- Returns 0 if no magnitude is found in table that this satisfies priority rules.
    FUNCTION bestMagidForOriginPref(p_orid Origin.orid%TYPE, p_magtype Netmag.magtype%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns the NetMag.magid of the magnitude with the highest priority 
    -- that is associated with the input evid in the EventPrefMag table;
    -- this magnitude does not have to associated with Event.prefor.
    -- Returns 0 if no magnitude is found in table that this satisfies priority rules.
    FUNCTION bestMagidOfEventPrefMag(p_evid Event.evid%TYPE) RETURN NUMBER;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Returns string summarizing the data of the Netmag row whose key matches the input magid.
    -- These data are those used to determine priority when compared to the rule constraints
    -- of the MagprefPriority table.  Returns NULL if no matching Netmag row key exists.
    FUNCTION describeMag(p_magid NetMag.magid%TYPE) RETURN VARCHAR2;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Write to server output a list of region names initialized from GAZETTEER_REGION table.
    PROCEDURE dump_region_names;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
    -- Spaced delimited list of the region names in GAZETTEER_REGION table that are 
    -- possible for MAGPREFPRIORITY table row key elements.
    FUNCTION get_region_names RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
END magpref;
```