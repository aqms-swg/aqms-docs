# WAVEARC PACKAGE {#wavearc-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE wavearc AUTHID DEFINER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: wavearc_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- DEFINER rights needed to create Jasi TN subtype via newInstance(String) -aww 05/02/2005
-- Uses objects (or synonyms) defined on compilation in the CODE name space not USER's objects
--
-- Functions to generate and insert waveform request "records" into database
-- for processing by waveform file archiving code.
--
-- Used in production: no, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  unknown ? 
--
-- <DEPENDS ON>
--  Java org.trinet.storedprocs.DbaseConnection loaded on server as well as
--  Java classes in org.trinet.jasi
--  Java classes in org.trinet.jdbc.datatypes
--  Java classes in org.trinet.jdbc.tables
--  Java classes in org.trinet.util
--  Java class org.trinet.storedprocs.waveformrequest.RequestGenerator.class
--  Java class org.trinet.storedprocs.waveformrequest.RequestCardTN.class
--  Java class org.trinet.jdbc.table.RequestCard.class
--  Java class org.trinet.jdbc.table.TableRowRequestCard.class
--
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
--  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Generate Waveform requests for the given Event ID.
    -- Returns NUMBER of requests generated, 0 if no match.
    -- Example: call WAVEARC.generateRequests(1111) into :v_count;
    FUNCTION GenerateRequests(p_evid NUMBER) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Returns NUMBER of Waveform requests the given Event ID will generate but doesn't generate them.
    -- Returns NUMBER of requests generated, 0 if no match.
    -- Example: call WAVEARC.generateRequestCount(1111) into :v_count;
    FUNCTION GenerateRequestCount(p_evid NUMBER) RETURN NUMBER;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END wavearc;
```