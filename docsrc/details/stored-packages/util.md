# UTIL PACKAGE {#util-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE util AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: util_pkg.sql 7769 2015-11-18 22:59:47Z awwalter $';
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   unknown ?
--
-- <DEPENDS ON> (loaded on server) 
-- NCEDC Parametric tables
-- package TRUETIME WHERES
-- Java org.trinet.util.EpochTime
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Returns a string containing event summary data with column header title.
  FUNCTION eventInfo(p_evid EVENT.evid%TYPE) RETURN VARCHAR2;
--
-- Returns a string containing event summary data with column header title 
-- when input parameter p_title = 1. Column header tags:
--    ET = event type
--     R = prefor rflag
--     S = event selectflag
--     B = origin bogus flag
--     V = event version
--  AMPS = # of Ml assocmam amps
--   WFS = # of assocwae wfid
--    SM = # of valid assocevampset amps
  FUNCTION eventInfo(p_evid EVENT.evid%TYPE, p_title PLS_INTEGER) RETURN VARCHAR2;
--
-- Returns a string containing only the event summary info column header title.
  FUNCTION infotitle RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Print summary listing of all primary system events in lasts p_hrs.
  PROCEDURE cattail(p_hrs NUMBER);
--
-- Print summary listing of all events in last p_hrs whose selectflag matches input flag.
-- Null input flag value includes all primary, shadow and deleted events.
  PROCEDURE cattail(p_hrs NUMBER, p_sflag PLS_INTEGER);
--
-- Print summary listing of all events in last p_hrs whose selectflag matches input flag,
-- and whose etype matches input type. Null flag or type input parameters value includes
-- all values.
  PROCEDURE cattail(p_hrs NUMBER, p_sflag PLS_INTEGER, p_etype EVENT.etype%TYPE);
  PROCEDURE cattail(p_hrs NUMBER, p_sflag PLS_INTEGER, p_etype EVENT.etype%TYPE, p_gtype Origin.gtype%TYPE);
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Get value the version number of the event row whose key is the input evid.
-- Returns version, or -1 if no matching event id is found, or 0 if event version is NULL.
-- Example: call UTIL.get_version(1111);
  FUNCTION get_version(p_evid Event.evid%TYPE) RETURN NUMBER;
--
-- Get string that identifies who created Origin row corresponding to input orid
-- Returns NULL if no such row or error.
   FUNCTION getWhoOrigin(p_orid Origin.orid%TYPE) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Get string that identifies who created NetMag table row corresponing to input magid.
-- Returns NULL if no such row or error.
   FUNCTION getWhoNetMag(p_magid Netmag.magid%TYPE) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return count of waveforms associated with an event.
-- Example: select UTIL.waveformcount(1111) from dual;
  FUNCTION waveformCount(p_evid IN Event.evid%TYPE) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return String concatenating remarks associated with input commid in Remark table..
-- Returns null if no matches rows.
  FUNCTION getComment(p_commid REMARK.commid%TYPE) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return count of distinct channel waveform SNCL associated with an event.
-- Value is less than waveformCount when any channel has multiple waveform rows.
  FUNCTION waveformChannelCount(p_evid IN Event.evid%TYPE) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return count of amplitudes associated with an event.
-- Example: select UTIL.ampcount(1111) from dual;
  FUNCTION ampCount(p_evid IN Event.evid%TYPE) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return an epoch time as a default string of form "yyyy-MM-dd HH:mm:ss.SSS".
-- Example: select UTIL.epochtostring(0.) from dual;
-- Epoch times < 0 are for dates before Jan 1, 1970.
  FUNCTION epochToString(epochsecs NUMBER) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return an epoch time as a string of form <pattern> in UTC timezone.
-- Example: select UTIL.epochtostring(0.,'dd-MM-yyyy HH:mm') from dual;
  FUNCTION epochToString(epochsecs NUMBER, pattern VARCHAR2) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return an epoch time as a string of form <pattern> in the given timezone.
-- Example: select UTIL.epochtostring(0.,'dd-MM-yyyy hh:mm', 'PST') from dual;
  FUNCTION epochToString(epochsecs NUMBER, pattern VARCHAR2, timezone VARCHAR2) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return epoch time parsed from the input time string in the input pattern.
-- NOTE: "ff" format will not work here.
-- Example: select UTIL.stringtoepoch('01-01-1970 00:00:00.00', 'dd-MM-yyyy hh:mm:ss.ss') from dual;
  FUNCTION stringToEpoch(timeString VARCHAR2, pattern VARCHAR2) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return the local session time zone DATE equivalent to input UTC date string of the specified format.
  FUNCTION utcstring2date(v_datestr VARCHAR2, v_format VARCHAR2) RETURN DATE;
--
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- Return the local DATE equivalent the input epoch LEAP seconds (UTC).
  FUNCTION true2date(v_epochsecs NUMBER) RETURN DATE;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return epoch LEAP seconds equivalent to the input local session DATE.
  FUNCTION date2true(v_date DATE) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return the local DATE equivalent the input epoch NOMINAL seconds (UTC).
  FUNCTION nominal2date(v_epochsecs NUMBER) RETURN DATE;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return epoch NOMINAL seconds equivalent to the input local session DATE.
  FUNCTION date2nominal(v_date DATE) RETURN NUMBER;
--
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- Return UTC date string equivalent to the input DATE in the DEFAULT (TRUETIME) format.
  FUNCTION date2utcstring(v_date DATE) RETURN VARCHAR2;
--
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- Return UTC date string equivalent to the input DATE in the format specified.
  FUNCTION date2utcstring(v_date DATE, v_format VARCHAR2) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- Return String for station name plus closest town.
-- Example: select UTIL.longname('CI','PAS') from dual;
  FUNCTION longName(p_net VARCHAR2, p_sta VARCHAR2) RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
  -- Print input strings showing index where they first differ
  -- ( e.g. length of shortest string) else prints 'strings identical'.
  PROCEDURE print_str_diff(p_str_1 VARCHAR2, p_str_2 VARCHAR2);
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
  -- Return index where input strings first differ (or length of shortest string)
  -- return value is -1 if input strings are identical.
  FUNCTION str_diff(p_str_1 VARCHAR2, p_str_2 VARCHAR2) RETURN PLS_INTEGER;
--
/* Procedure to print long character strings using DBMS_OUTPUT 
  Author: Oleg Savkin Nov 2005
  Parameters:
    IN_TEXT         - text to print
    IN_TEXT_LENGTH  - output string length. Default is 255 (maximum allowed for DBMS_OUTPUT)
    IN_DIVIDER      - divider between words. 
                      Used to do not split the whole word when start new print line
                      Default is SPACE
    IN_NEW_LINE     - new line divider. If there is this divider withing string to print out, then
                      string will be first printed till this divider, and then start from new line.
                      Default NULL
    Examples:
      --
      print_out(<text>);
      print_out(<text>, 80);
      print_out(<text>, 20);
      print_out(<text>, 255, ' ');
      print_out(<text>, 250, ' ', chr(10));
      --
        Last example: print text breaking it by spaces. 
        If there is new line character within test, it will be printed on the different line.
*/
  PROCEDURE print_out( IN_TEXT        VARCHAR2, 
                       IN_TEXT_LENGTH NUMBER   DEFAULT 255,
                       IN_DIVIDER     VARCHAR2 DEFAULT CHR(32),
                       IN_NEW_LINE    VARCHAR2 DEFAULT NULL);
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- Pragma below means package in not persistent and variable values are not
-- retained between calls. See Oracle8 Bible, pg. 481
-- Otherwise, it would persist for duration of a session.
--  PRAGMA SERIALLY_REUSABLE;
END util;
```