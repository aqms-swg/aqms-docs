# FORMATS PACKAGE {#format-stored-package}  
  
## Specification
  
```
REATE OR REPLACE PACKAGE formats AUTHID DEFINER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: formats_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- NOTE:
-- USES objects as defined in DEFINER namespace, not the USER's objects, because it must
-- create a SolutionTN by newInstance Class.forName(String), and thus have
-- java security access permissions
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   TPP scripts: cubeMessage and QDDSsend
--
-- <DEPENDS ON>
--   NCEDC parametric schema Event, Origin, Netmag, EventPrefMag tables to get db data for an input evid
--   and MessageText table for text message headers
--   Java class org.trinet.storedprocs.formats.Formats.class (loaded onto server)
--   Java class org.trinet.formats.CubeFormat.class (loaded onto server)
--   Java class org.trinet.formats.HeaderStrings.class (loaded onto server)
--   Java class org.trinet.formats.AftershockProbabilityReport.class (loaded onto server)
--
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return Cube event message for the input evid.
    -- Version defaults to event database table version.
    -- Example: call FORMATS.getCubeEFormat(1111) into :v_string;
    FUNCTION getCubeEformat(p_evid NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return Cube event message for the input evid tagged with input version.
    -- Null input version defaults to event database table version.
    FUNCTION getCubeEformat(p_evid NUMBER, p_version VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return Cube event summary email message for the input evid.
    -- Version defaults to event database table version.
    -- Event summary includes distances to nearest town,bigtown,quarry,fault, and volcano
    -- subtypes that have entries in the gazetteer tables.
    -- Example: call FORMATS.getCubeEFormat(1111) into :v_string;
    FUNCTION getCubeEmailFormat(p_evid NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return CUBE event summary email message for the input evid. 
    -- Version defaults to event database table version.
    -- p_flag = 0 distances to nearest town,bigtown,quarry,fault, and volcano are included
    --          for those subtypes that have entries in the gazetteer tables.
    --  else set flag to integer value constructed from the bitwise OR of these integer masks:
    --     1 include distance to nearest town
    --     2 include distance to nearest big town (city)
    --     4 include distance to nearest quarry
    --     8 include distance to nearest fault 
    --    16 include distance to nearest volcano
    FUNCTION getCubeEmailFormat(p_evid NUMBER, p_flag PLS_INTEGER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return CUBE event summary email message for input evid tagged with input version.
    -- Null input version defaults to event database table version.
    -- Event summary includes distances to nearest town,bigtown,quarry,fault, and volcano
    -- subtypes that have entries in the gazetteer tables.
    FUNCTION getCubeEmailFormat(p_evid NUMBER, p_version VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return CUBE event summary email message tagged with the input version id.
    -- Null input version defaults to event database table version.
    --- p_flag = 0 distances to nearest town,bigtown,quarry,fault, and volcano are included
    --             for those subtypes that have entries in the gazetteer tables.
    --  else set flag to integer value constructed from the bitwise OR of these integer masks:
    --     1 include distance to nearest town
    --     2 include distance to nearest big town (city)
    --     4 include distance to nearest quarry
    --     8 include distance to nearest fault 
    --    16 include distance to nearest volcano
    FUNCTION getCubeEmailFormat(p_evid NUMBER, p_version VARCHAR2, p_flag PLS_INTEGER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return CUBE event summary email wrapped with CANCELLED header, footer text
    -- Event version defaults to event database table version.
    -- Summary text includes distances to known databases subtypes.
    FUNCTION getCubeCancelEmailFormat(p_evid NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return CUBE event summary email tagged with input version wrapped with CANCELLED header, footer text.
    -- Summary text includes distances to known databases subtypes.
    FUNCTION getCubeCancelEmailFormat(p_evid NUMBER, p_version VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return CUBE event summary email tagged with input version wrapped with CANCELLED header, footer text.
    --- p_flag = 0 distances to nearest town,bigtown,quarry,fault, and volcano are included,
    --             for those subtypes that have entries in the gazetteer tables.
    --  else set flag to integer value constructed from the bitwise OR of these integer masks:
    --     1 include distance to nearest town
    --     2 include distance to nearest big town (city)
    --     4 include distance to nearest quarry
    --     8 include distance to nearest fault 
    --    16 include distance to nearest volcano
    FUNCTION getCubeCancelEmailFormat(p_evid NUMBER, p_version VARCHAR2, p_flag PLS_INTEGER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return Cube delete message for this event ID, tagged with input net code and comment
    -- Example: call FORMATS.getCubeDEFormat(1111, 'NC', 'noise') into :v_string;
    FUNCTION getCubeDEformat(p_evid NUMBER, p_net VARCHAR2, p_comment VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return Cube delete message for this event ID
    -- Defaults network code to value of Origin.auth.
    -- Leaves comment blank.
    -- Example: call FORMATS.getCubeDEFormat(1111) into :v_string;
    FUNCTION getCubeDEformat(p_evid NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return description on database connection
    -- For debug, not for production use
    FUNCTION getConnectionInfo(p_dummy NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return current Probability Report for this event.
    -- The probabilities vary with time so the results depend on when this is run.
    -- Defaults to event database table version.
    FUNCTION getProbabilityReport(p_evid NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return current Probability Report for this event tagged with input version.
    -- Null input version defaults to event database table version.
    -- The probabilities vary with time so the results depend on when this is called.
    FUNCTION getProbabilityReport(p_evid NUMBER, p_version VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    -- Return current Probability Report for this event tagged with input version in HTML format.
    -- Null input version defaults to event database table version.
    -- The probabilities vary with time so the results depend on when this is called.
    FUNCTION getProbabilityReportHTML(p_evid NUMBER, p_version VARCHAR2) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    FUNCTION getProbabilityReport(p_evid NUMBER, p_version VARCHAR2,
                                  p_logIntensity NUMBER, p_timeDecayExponent NUMBER,
                                  p_bValue NUMBER, p_timeOffsetDays NUMBER,
                                  p_MHigh NUMBER, p_MLow NUMBER, p_duration NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    FUNCTION getProbabilityReport(p_evid NUMBER, p_version VARCHAR2, p_MHigh NUMBER,
                                  p_MLow NUMBER, p_duration NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    FUNCTION getProbabilityReportHTML(p_evid NUMBER, p_version VARCHAR2,
                                  p_logIntensity NUMBER, p_timeDecayExponent NUMBER,
                                  p_bValue NUMBER, p_timeOffsetDays NUMBER,
                                  p_MHigh NUMBER, p_MLow NUMBER, p_duration NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    FUNCTION getProbabilityReportHTML(p_evid NUMBER, p_version VARCHAR2, p_MHigh NUMBER,
                                      p_MLow NUMBER, p_duration NUMBER) RETURN VARCHAR2;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
    PROCEDURE setProbabilityModel(p_logIntensity NUMBER, p_timeDecayExp NUMBER,
                                  p_vValue NUMBER, p_timeOffsetDays NUMBER);
-- -------------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
END formats;
```