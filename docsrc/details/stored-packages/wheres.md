# WHERES PACKAGE {#wheres-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE wheres AUTHID CURRENT_USER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: wheres_pkg.sql 7424 2014-08-26 03:19:34Z awwalter $';
--
-- Functions/Procedures to retrieve geographic description information for
-- points specified by lat,lon, or name.
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  Package MATCH
--  Package QUARRY
--  Package UTIL
--
--  /tpp/bin/cattail.pl
--  /tpp/bin/ampdump.pl
--  /tpp/bin/stadist.pl
--  /tpp/bin/archive_db_total.pl
--
--  Java org.trinet.gazetteer.TN.WhereIsClosest... subclasses (CLASSES NOT IN USE?)
--  Java org.trinet.jasi.TN.SolutionTN 
--  Java org.trinet.jiggle.CatalogPanel
--  Java org.trinet.util.graphics.table.CatalogPanel
--  Java org.trinet.util.graphics.table.JasiSolutionListPanel
--  Java org.trinet.util.graphics.table.AbstractJasiSolutionEditorPanel
--
-- <DEPENDS ON> (objects needing to be loaded on server)
--   Java org.trinet.util.gazetteer.TN.WheresFromServer
--   Java org.trinet.util.gazetteer.GeoidalConvert
--   Java classes included by above Java classes and the schema GAZETTEERxxx tables
-- -------------------------------------------------------------------------------------------------
--
--  Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
  FUNCTION getPkgSpecId RETURN VARCHAR2;
-- 
-- sets the default distance units as kilometers for reporting locale(...) functions
-- or town, bigtown, quarry, quake, fault, eqPOI, volcano, or station procedures
    PROCEDURE dist_units_km; 
--
-- sets the default distance units as miles for reporting in locale(...) functions
-- or town, bigtown, quarry, quake, fault, eqPOI, volcano, or station procedures
    PROCEDURE dist_units_miles; 
--
-- Return distance to gazid in km, null if no id match.
    FUNCTION dist2id(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE, p_id GAZETTEERPT.GAZID%TYPE) RETURN NUMBER;
--
-- Return km distance and direction of named place to input location,
-- returns null if no match for name and type in GAZETTEERPT table.
    FUNCTION from_name_type(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE,
                             p_name GAZETTEERPT.NAME%TYPE, p_type GAZETTEERPT.TYPE%TYPE) RETURN VARCHAR2;
--
-- Return km distance and direction of name associated with gazid to input location,
-- returns null if no match for gazid in GAZETTEERPT table.
    FUNCTION from_id_pt(p_lat ORIGIN.LAT%TYPE, p_lon ORIGIN.LON%TYPE, p_id GAZETTEERPT.GAZID%TYPE) RETURN VARCHAR2;
--
-- Returns 4-character left-justified string describing a compass direction.
    FUNCTION compass_pt ( p_az IN NUMBER ) RETURN VARCHAR2;
--
-- input decimal degrees latitude and longitude of two locations returns azimuth from point1 to point2.
    FUNCTION az_to ( p_lat1 IN NUMBER,
                     p_lon1 IN NUMBER,
                     p_lat2 IN NUMBER,
                     p_lon2 IN NUMBER ) RETURN NUMBER;
--
-- input decimal degrees latitude and longitude of two locations, elevation/-depth in kilometers.
-- returns elevation in degrees from point1 to point2.
    FUNCTION elev_to ( p_lat1 IN NUMBER,
                       p_lon1 IN NUMBER,
                       p_z1   IN NUMBER,
                       p_lat2 IN NUMBER,
                       p_lon2 IN NUMBER,
                       p_z2   IN NUMBER ) RETURN NUMBER;
--
-- input decimal degrees latitude and longitude of two locations
-- returns horizontal kilometers distance from point1 to point2.
    FUNCTION separation_km ( p_lat1 IN NUMBER,
                             p_lon1 IN NUMBER,
                             p_lat2 IN NUMBER,
                             p_lon2 IN NUMBER ) RETURN NUMBER;
--
-- input decimal degrees latitude and longitude of two locations, elevation/-depth in kilometers.
-- returns 3-D distance from point1 to point2.
    FUNCTION separation_km ( p_lat1 IN NUMBER,
                             p_lon1 IN NUMBER,
                             p_z1   IN NUMBER,
                             p_lat2 IN NUMBER,
                             p_lon2 IN NUMBER,
                             p_z2   IN NUMBER ) RETURN NUMBER;
--
-- input decimal degrees latitude and longitude of two locations
-- returns horizontal miles distance from point1 to point2.
    FUNCTION separation_mi ( p_lat1 IN NUMBER,
                             p_lon1 IN NUMBER,
                             p_lat2 IN NUMBER,
                             p_lon2 IN NUMBER ) RETURN NUMBER;
--
-- input degrees and decimal minutes, returns decimal degrees
    FUNCTION ddeg ( p_deg IN NUMBER, p_min IN NUMBER ) RETURN NUMBER;
--
-- input degrees, decimal minutes and seconds, returns decimal degrees
    FUNCTION ddeg ( p_deg IN NUMBER, p_min IN NUMBER, p_sec IN NUMBER ) RETURN NUMBER;
--
-- input parameter is decimal degrees
-- OUT parameters are degrees and decimal minutes
    PROCEDURE ddeg_to_dmin ( p_deg_in  IN NUMBER,
                             p_deg_out OUT NUMBER,
                             p_min_out OUT NUMBER );
--
-- input kilometer, returns feet.
    FUNCTION km_to_ft ( p_km IN NUMBER ) RETURN NUMBER;
--
-- input feet, returns kilometers.
    FUNCTION ft_to_km ( p_feet IN NUMBER ) RETURN NUMBER;
--
-- input kilometers, returns miles.
    FUNCTION km_to_mi ( p_km IN NUMBER ) RETURN NUMBER;
--
-- input miles, returns kilometers.
    FUNCTION mi_to_km ( p_miles IN NUMBER ) RETURN NUMBER;
--
--
    FUNCTION km_to_fault(p_lat NUMBER, p_lon NUMBER, p_name IN VARCHAR2) RETURN NUMBER;
--
-- Return string describing closest town to input coordinates
-- with a State address two-letter code appended like: "Barstow, CA"  
-- Example: select WHERES.getnearesttown(34., -118.) from dual;
  FUNCTION getNearestTown(p_lat NUMBER, p_lon NUMBER) RETURN VARCHAR2;
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest town to specified source.
    PROCEDURE town ( p_lat    IN NUMBER, 
                     p_lon    IN NUMBER,
                     p_z      IN NUMBER,
                     p_dist  OUT NUMBER,
                     p_az    OUT NUMBER,
                     p_elev  OUT NUMBER,
                     p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest big town to specified source.
    PROCEDURE bigtown ( p_lat    IN NUMBER, 
                        p_lon    IN NUMBER,
                        p_z      IN NUMBER,
                        p_dist  OUT NUMBER,
                        p_az    OUT NUMBER,
                        p_elev  OUT NUMBER,
                        p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest quarry to specified source.
    PROCEDURE quarry ( p_lat    IN NUMBER, 
                       p_lon    IN NUMBER,
                       p_z      IN NUMBER,
                       p_dist  OUT NUMBER,
                       p_az    OUT NUMBER,
                       p_elev  OUT NUMBER,
                       p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest quake to specified source.
    PROCEDURE quake ( p_lat    IN NUMBER, 
                      p_lon    IN NUMBER,
                      p_z      IN NUMBER,
                      p_dist  OUT NUMBER,
                      p_az    OUT NUMBER,
                      p_elev  OUT NUMBER,
                      p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest station to specified source.
    PROCEDURE station ( p_lat    IN NUMBER, 
                        p_lon    IN NUMBER,
                        p_z      IN NUMBER,
                        p_dist  OUT NUMBER,
                        p_az    OUT NUMBER,
                        p_elev  OUT NUMBER,
                        p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest fault to specified source.
    PROCEDURE fault ( p_lat    IN NUMBER, 
                      p_lon    IN NUMBER,
                      p_z      IN NUMBER,
                      p_dist  OUT NUMBER,
                      p_az    OUT NUMBER,
                      p_elev  OUT NUMBER,
                      p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest volcano to specified source.
    PROCEDURE volcano ( p_lat    IN NUMBER, 
                        p_lon    IN NUMBER,
                        p_z      IN NUMBER,
                        p_dist  OUT NUMBER,
                        p_az    OUT NUMBER,
                        p_elev  OUT NUMBER,
                        p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest eq POI to specified source.
    PROCEDURE eqpoi ( p_lat    IN NUMBER, 
                      p_lon    IN NUMBER,
                      p_z      IN NUMBER,
                      p_dist  OUT NUMBER,
                      p_az    OUT NUMBER,
                      p_elev  OUT NUMBER,
                      p_place OUT VARCHAR2 );
--
-- input point gazetteertype name, latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest point of input type to specified source.
  PROCEDURE point (  p_type   IN VARCHAR2, 
                     p_lat    IN NUMBER, 
                     p_lon    IN NUMBER,
                     p_z      IN NUMBER,
                     p_dist  OUT NUMBER,
                     p_az    OUT NUMBER,
                     p_elev  OUT NUMBER,
                     p_place OUT VARCHAR2 );
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- returns String summarizing distance, azimuth, and elevation from the closest N of specific object type.
-- Types are identified by name strings: 'town', 'bigtown', 'quarry', 'quake', 'fault', 'eqpoi', 'volcano' and 'station'.
-- Example: call WHERES.LOCALE(34.0,-118.0, 0., 3, 'town') into :v_string;
  FUNCTION locale_by_type ( p_lat  IN NUMBER,
                            p_lon  IN NUMBER,
                            p_z    IN NUMBER,
                            p_num  IN NUMBER,
                            p_type IN VARCHAR2 ) RETURN VARCHAR2;
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- returns String summarizing distance, azimuth, and elevation from the closest N of each object type.
-- Example: call WHERES.LOCALE(33.5,-120.5) into :v_string;
    FUNCTION locale ( p_lat IN NUMBER, p_lon IN NUMBER ) RETURN VARCHAR2;
--
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- returns String summarizing distance, azimuth, and elevation from the closest N of each object type.
    FUNCTION locale ( p_lat IN NUMBER,
                      p_lon IN NUMBER,
                      p_z   IN NUMBER,
                      p_num IN NUMBER ) RETURN VARCHAR2;
--
-- input source latitude and longitude in degrees, decimal minutes, and elevation/-depth in kilometers.
-- returns String summarizing distance, azimuth, and elevation from the closest N of each object type.
    FUNCTION locale ( p_lat    IN NUMBER,
                      p_latmin IN NUMBER,
                      p_lon    IN NUMBER,
                      p_lonmin IN NUMBER,
                      p_z      IN NUMBER,
                      p_num    IN NUMBER ) RETURN VARCHAR2;
--
-- return 1 if input location (lat,lon) is within radius of the set reference place,
-- else return 0.
  FUNCTION within_radius_of ( p_lat       IN NUMBER,
                              p_lon       IN NUMBER,
                              p_radius_km IN NUMBER DEFAULT 0.) RETURN NUMBER;
--
-- return 1 if input location (lat,lon) is within radius of named place of interest,
-- else return 0. If input place name has not already been set (see set_reference(...)),
-- it is retrieved from the database GAZETTEERPT table.
  FUNCTION within_radius_of( p_lat       IN NUMBER,
                             p_lon       IN NUMBER,
                             p_name      IN VARCHAR2,
                             p_radius_km IN NUMBER DEFAULT 0.) RETURN NUMBER;
--
-- set the reference place name and location (lat, lon) used by functions within_radius_of(...). 
  PROCEDURE set_reference_place( p_name IN VARCHAR2 DEFAULT 'unknown',
                                 p_lat  IN NUMBER DEFAULT 0.,
                                 p_lon  IN NUMBER DEFAULT 0.);
--
-- print reference place name and location (lat, lon).
  PROCEDURE print_reference_place;
--
-- prints out (DBMS_OUTPUT) summary of distance, azimuth, and elevation from the closest of each object type.
-- input source latitude and longitude in decimal degrees.
  PROCEDURE report_locale ( p_lat IN NUMBER, p_lon IN NUMBER );
--
-- prints out (DBMS_OUTPUT) summary of distance, azimuth, and elevation from the closest of each object type.
-- input source latitude and longitude in decimal degrees and elevation/-depth in kilometers.
  PROCEDURE report_locale ( p_lat IN NUMBER, p_lon IN NUMBER, p_z IN NUMBER );
--
-- 
END wheres;
```