# SEQUENCE PACKAGE {#sequence-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE SEQUENCE AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: sequence_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Functions to retrieve sequence numbers from a database sequence.
--
-- Contact: Ellen Yu SCEDC CIT
--
-- Currently used in production: yes
--
-- <LIST DEPENDENT APPLICATIONS HERE>
-- RT C++ source modules named like Database.*  invoke package functions
-- org.trinet.jasi.TN.AmpListTN.java
-- org.trinet.jasi.TN.PhaseListTN.java
-- org.trinet.jasi.TN.CodaListTN.java
--
-- <DEPENDS ON > (loaded on server)
-- no stored java class dependencies
-- --------------------------------------------------------------------------------------------
--
--  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
--
-- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- --------------------------------------------------------------------------------------------
--
-- Return the "increment by" value for sequence named by p_seqname.
-- Sequence is referenced via ALL_SEQUENCES view. 
    FUNCTION getIncrement(p_seqname VARCHAR2) RETURN INTEGER;
-- --------------------------------------------------------------------------------------------
--
-- Return a string containing a set of requested sequence numbers.
-- The sequence name is specified by input p_seqname. 
-- The number of desired sequenced numbers is specified by in/out p_numseq.
-- The number of sequence numbers returned may be less than that requested, 
-- the total is returned to the p_numseq parameter.
--
    FUNCTION getNext(p_seqname IN VARCHAR2, p_numseq IN OUT INTEGER) RETURN VARCHAR2;
-- --------------------------------------------------------------------------------------------
--
-- Pipelined function return can be cast as table to get result set
-- e.g. "Select column_value from table (sequence.getSeqTable('ARSEQ',?));"
-- where the number of desired sequenced numbers is specified by bind variable ?
    FUNCTION getSeqTable(p_seqname IN VARCHAR2, p_numseq IN INTEGER) RETURN ID_TABLE PIPELINED;
-- --------------------------------------------------------------------------------------------
END sequence;
```