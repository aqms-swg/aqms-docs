# TRUETIME PACKAGE {#truetime-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE TRUETIME AUTHID DEFINER AS
   PKG_SPEC_ID VARCHAR2(80) := '$Id: truetime_pkg.sql 7400 2014-06-20 00:15:39Z awwalter $';
--
-- Used in production: yes, UTC version
-- <LIST DEPENDENT APPLICATIONS HERE>
-- Database Stored packages:
-- db_cleanup
-- epref
-- magpref
-- match
-- quarry
-- util
-- wavefile
--
-- All java classes that query or insert database datetime values.
--
-- <DEPENDS ON>
--     No Java classes
--     No other packages
--     Existance of table LEAP_SECONDS in schema
--
--  /***********************************************************************
--  * Truetime.sql ---> PL/SQL Package for conversion between :		   *
--  *									   *
--  *		- String format time.					   *
--  *		- Nominal epoch time.					   *
--  *		- True epoch time.					   *
--  *									   *
--  * Stephane Zuzlewski 1998-2004					   *
--  ***********************************************************************/
--
--  /***********************************************************************
--  * The table leap_seconds contains information about all the current	   *
--  * leap seconds.							   *
--  ***********************************************************************/
--
--  /***********************************************************************
--  * Return SVN Id string.
--  ***********************************************************************/    
    FUNCTION getPkgId RETURN VARCHAR2;
--
--  /***********************************************************************
--  * Return SVN Id for package specification.
--  ***********************************************************************/    
    FUNCTION getPkgSpecId RETURN VARCHAR2;
-- 
--  /***********************************************************************
--  * The function string2nominal converts a string format time to a 	   *
--  * nominal epoch time.						   *
--  * Input string must have format of 'YYYY/MM/DD HH24:MI:SS' where the
--  * field delimiters are optional or other punctuation can be substituted.
--  * Returns NULL if the time is a leap second.			   *
--  ***********************************************************************/
	FUNCTION string2nominal (d VARCHAR) RETURN INTEGER DETERMINISTIC;
--  /***********************************************************************
--  * The function string2nominalf converts a string format time to a 	   *
--  * nominal epoch time (including 1/10000 of seconds).		   *
--  * Input string must have format of 'YYYY/MM/DD HH24:MI:SS' but the field
--  * element delimiters are optional or other punctuation can be substituted.
--  *									   *
--  * Returns NULL if the time is a leap second.			   *
--  ***********************************************************************/
	FUNCTION string2nominalf(d VARCHAR) RETURN FLOAT   DETERMINISTIC;
--  /***********************************************************************
--  * The function nominal2string converts a nominal epoch time to a       *
--  * string format time.						   *
--  ***********************************************************************/
	FUNCTION nominal2string (n INTEGER) RETURN VARCHAR DETERMINISTIC;
--  /***********************************************************************
--  * The function nominal2stringf converts a nominal epoch time to a      *
--  * string format time (including 1/10000 of seconds).		   *
--  ***********************************************************************/
	FUNCTION nominal2stringf(n FLOAT)   RETURN VARCHAR DETERMINISTIC;
--  /***********************************************************************
--  * The function nominal2true converts a nominal epoch time to a true    *
--  * epoch time.							   *
--  ***********************************************************************/
	FUNCTION nominal2true 	(n INTEGER) RETURN INTEGER DETERMINISTIC;
--  /***********************************************************************
--  * The function nominal2truef converts a nominal epoch time to a true   *
--  * epoch time (including 1/10000 of seconds).			   *
--  ***********************************************************************/
	FUNCTION nominal2truef  (n FLOAT)   RETURN FLOAT   DETERMINISTIC;
--  /***********************************************************************
--  * The function true2nominal converts a true epoch time to a nominal    *
--  * epoch time.							   *
--  *									   *
--  * Returns NULL if the time is a leap second.			   *
--  ***********************************************************************/
	FUNCTION true2nominal 	(t INTEGER) RETURN INTEGER DETERMINISTIC;
--  /***********************************************************************
--  * The function true2nominalf converts a true epoch time to a nominal   *
--  * epoch time (including 1/10000 of seconds).			   *
--  *									   *
--  * Returns NULL if the time is a leap second.			   *
--  ***********************************************************************/
	FUNCTION true2nominalf  (t FLOAT)   RETURN FLOAT   DETERMINISTIC;
--  /***********************************************************************
--  * The function string2true converts a string format time to a true     *
--  * Input string must have format of 'YYYY/MM/DD HH24:MI:SS' but the field
--  * element delimiters are optional or other punctuation can be substituted.
--  * epoch time.							   *
--  ***********************************************************************/
	FUNCTION string2true 	(d VARCHAR) RETURN INTEGER DETERMINISTIC;
--  /***********************************************************************
--  * The function string2truef converts a string format time to a true    *
--  * Input string must have format of 'YYYY/MM/DD HH24:MI:SS' but the field
--  * element delimiters are optional or other punctuation can be subsituted.
--  * epoch time (including 1/10000 of seconds).			   *
--  ***********************************************************************/
	FUNCTION string2truef   (d VARCHAR) RETURN FLOAT   DETERMINISTIC;
--  /***********************************************************************
--  * The function true2string converts a true epoch time to a string      *
--  * format time.							   *
--  ***********************************************************************/
	FUNCTION true2string 	(t INTEGER) RETURN VARCHAR DETERMINISTIC;
--  /***********************************************************************
--  * The function true2stringf converts a true epoch time to a string     *
--  * format time (including 1/10000 of seconds).			   *
--  ***********************************************************************/
	FUNCTION true2stringf 	(t FLOAT)   RETURN VARCHAR DETERMINISTIC;
--
-- ========================================================================
-- Extensions for LeapSecond abstraction tier - DDG Sept. 2007
-- ========================================================================
--  /***********************************************************************
--  * Returns 'TRUE', if the timebase of the database is a UTC timebase,
--  * and 'NOMINAL', if it is UNIX/POSIX.
--  ***********************************************************************/
    FUNCTION getTimeBase RETURN VARCHAR2 DETERMINISTIC;
--
--  /***********************************************************************
--  * The function returns 1 if the native timebase of the dbase is "true"
--  * that is, UTC time including leap seconds. Otherwise, returns 0
--  * meaning the native timebase is UNIX (aka POSIX) time without leap seconds.
--  * This is intended for INTERNAL DATABASE use only. 
--  * Calling applications should NEVER care.
--  ***********************************************************************/
    FUNCTION timeBaseIsTrue  RETURN NUMBER DETERMINISTIC;
--
--  /***********************************************************************
--  * Given a native dbase epoch time 't' return returns the equivalent epoch  
--  * time in the timebase specified by 'baseOut'.
--  * Use in SELECT statements.
--  * Valid 'baseOut' values: ('UTC', 'TRUE') and ('POSIX', 'UNIX', 'NOMINAL')
--  * Lower case or mixed case strings are OK (will be upcased internally)
--  * Example: Select TrueTime.getEpoch(Origin.datetime, 'UTC')) into myUtcTime
--  *             where Origin = 123456;
--  ***********************************************************************/
    FUNCTION getEpoch (t NUMBER, baseOut VARCHAR2) RETURN NUMBER DETERMINISTIC;
--   
--  /***********************************************************************
--  * Given an epoch time 't' in the timebase specified by 'baseIn' returns the 
--  * equivalent epoch time in the dbase's native timebase.
--  * Use in INSERT or UPDATE statements.
--  * Valid epoch type values: ('UTC', 'TRUE') and ('POSIX', 'UNIX', 'NOMINAL')
--  * Lower case or mixed case strings are OK (will be upcased internally)
--  * Example: Update origin set datetime = TrueTime.putEpoch(myUtcTime, 'UTC')
--  *             where Origin = 123456;
--  ***********************************************************************/
    FUNCTION putEpoch (t NUMBER, baseIn VARCHAR2) RETURN NUMBER DETERMINISTIC;
--
--  /***********************************************************************
--  * Return a UTC time string for the given dbase datetime value 't'.
--  * Format example: "2007/09/07 07:07:26"
--  * Handles any convesion to account for dbase's native representation.
--  * Use in SELECT statements.
--  * Example: Select TrueTime.getString(Origin.datetime)) into myTimeString
--  *             where Origin = 123456;
--  ***********************************************************************/
    FUNCTION getString (t NUMBER) RETURN VARCHAR2 DETERMINISTIC;
--
--  /***********************************************************************
--  * Return a UTC time string w/ fractional seconds for the given dbase 
--  * datetime value 't'.
--  * Format example: "2007/09/07 07:07:26.8300" (NOTE: four decimal places)
--  * Handles any convesion to account for dbase's native representation.
--  * Use in SELECT statements.
--  * Example: Select TrueTime.getString(Origin.datetime)) into myTimeString
--  *             where Origin = 123456;
--  ***********************************************************************/
    FUNCTION getStringf (t NUMBER) RETURN VARCHAR2 DETERMINISTIC;
--    
--  /***********************************************************************
--  * Given a UTC time string 's' returns the epoch value in the database's timebase.
--  * Format example: "2007/09/07 07:07:26"
--  * Handles any convesion to account for dbase's native representation.
--  * Use in INSERT or UPDATE statements.
--  * Example: Update origin set datetime = TrueTime.putString('2007/09/07 07:07:26')
--  *             where Origin = 123456;
--  ***********************************************************************/
    FUNCTION putString (s VARCHAR2) RETURN NUMBER DETERMINISTIC;
--    
--  /***********************************************************************
--  * Given a UNIX epoch time return the number of leap seconds it is missing. 
--  * This is the number of leap seconds to add to get UTC epoch.
--  ***********************************************************************/    
    FUNCTION leaps2add (nomSec NUMBER) RETURN NUMBER DETERMINISTIC;
--    
--  /***********************************************************************
--  * Given a UTC epoch time return the number of leap seconds it contains.
--  * This is also the number of leap seconds to subtract to get UNIX epoch.
--  ***********************************************************************/    
    FUNCTION leaps2subtract (truSec NUMBER) RETURN NUMBER DETERMINISTIC;
--
--  /***********************************************************************
--  * Given a UTC time string of the form 'YYYY/MM/DD HH24:MI:SS' return
--  * an ORACLE date compatiblie POSIX string. 
--  * UTC leap seconds values of :60 or :61 are replaced with :59.
--  ***********************************************************************/    
    FUNCTION utc2posix (utcDateStr VARCHAR2) RETURN VARCHAR2 DETERMINISTIC;
--
--  /***********************************************************************
--  * Reinitialize the package after a LEAP_SECONDS table change. 
--  ***********************************************************************/    
END TRUETIME;
```
  
## EPOCHTIMEBASE Table  

The one-value table *EpochTimeBase* was added to hold the flag indicating if the epoch datetime values in a particular database instance are "True" leap-second compliant values or "Nominal" Unix-epoch values.   

``` 
Create Table EpochTimeBase (
 base VARCHAR2(1) NOT NULL,
 ondate DATE NOT NULL,
 offdate DATE,
 PRIMARY KEY (base, ondate),
 CONSTRAINT CHECK (base IN ('T','N'))
)

Where: 
T = true
N = nominal
```