# SCHEMA_UTIL PACKAGE {#schema_util-stored-package}  
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE schema_util AUTHID CURRENT_USER AS
--
-- Procedures to grant or revoke privileges to objects in schema
--
-- <DEPENDS ON>
--     table privileges defined in table PROD_TAB_PRIVS
--
-- 
-- Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
--
-- Grant the privileges declared in table PROD_TAB_PRIVS to all objects and grantees.
  PROCEDURE grant_privs;
--
-- Grant the privileges declared in table PROD_TAB_PRIVS for the input object name
-- in the invoking schema (USER) to ALL grantees.
  PROCEDURE grant_privs(p_table VARCHAR2);
--
-- Grant the privileges declared in table PROD_TAB_PRIVS for the input object name
-- and owner to ALL grantees.
  PROCEDURE grant_privs(p_table VARCHAR2, p_owner VARCHAR2);
--
-- Grant the privileges declared in table PROD_TAB_PRIVS for the input object name
-- and owner to the input grantee. ALL grantees are included if input grantee is NULL.
  PROCEDURE grant_privs(p_grantee VARCHAR2, p_table VARCHAR2, p_owner VARCHAR2);
--
-- Grant the privileges declared for input grantee in table PROD_TAB_PRIVS
-- for all objects owned by the invoking schema (USER).
  PROCEDURE grant_privs_for_all(p_grantee VARCHAR2);
--
-- Grant the privileges declared for input grantee in table PROD_TAB_PRIVS
-- for all objects owned by the input schema 
  PROCEDURE grant_privs_for_all(p_grantee VARCHAR2, p_owner VARCHAR2);
--
-- Grant the privileges declared for input grantee in table PROD_TAB_PRIVS
-- for the input table object owned by the input schema.
  PROCEDURE grant_privs_for(p_grantee VARCHAR2, p_table VARCHAR2, p_owner VARCHAR2);
--
-- Grant the privileges declared for input grantee in table PROD_TAB_PRIVS
-- for input table object owned by the invoking schema (USER)
  PROCEDURE grant_privs_for(p_grantee VARCHAR2, p_table VARCHAR2);
--
-- Grant all privileges to all grantees declared in table PROD_TAB_PRIVS
-- declared for any JAVA class objects owned by the invoking schema (USER)
  PROCEDURE grant_user_java_privs;
--
-- Revoke the privileges declared in table PROD_TAB_PRIVS from all objects and grantees.
  PROCEDURE revoke_privs;
--
-- Revoke the privileges declared in table PROD_TAB_PRIVS for the input object name
-- in the invoking schema (USER) from ALL grantees.
  PROCEDURE revoke_privs(p_table VARCHAR2);
--
-- Revoke the privileges declared in table PROD_TAB_PRIVS for the input object name
-- and owner from ALL grantees.
  PROCEDURE revoke_privs(p_table VARCHAR2, p_owner VARCHAR2);
--
-- Revoke the privileges declared in table PROD_TAB_PRIVS for the input object name
-- and owner from input grantee. ALL grantees are included if input grantee is NULL.
  PROCEDURE revoke_privs(p_grantee VARCHAR2, p_table VARCHAR2, p_owner VARCHAR2);
--
-- Revoke the privileges declared in table PROD_TAB_PRIVS for objects owned by
-- the invoking schema (USER) and the input grantee.
  PROCEDURE revoke_privs_for_all(p_grantee VARCHAR2);
--
-- Revoke the privileges declared in table PROD_TAB_PRIVS for objects owned by
-- the input schema and the input grantee.
  PROCEDURE revoke_privs_for_all(p_grantee VARCHAR2, p_owner VARCHAR2);
--
-- Revoke all privileges from the input grantee on input object belonging to input owner.
  PROCEDURE revoke_privs_for(p_grantee VARCHAR2, p_table VARCHAR2, p_owner VARCHAR2);
--
-- Revoke all privileges from input grantee on input object owned by invoking schema (USER).
  PROCEDURE revoke_privs_for(p_grantee VARCHAR2, p_table VARCHAR2);
--
-- Revoke all privileges on input object owned by invoking schema (USER).
  PROCEDURE revoke_all_privs(p_table VARCHAR2);
--
-- Output lines listing known package versions (getPkgId) and last_ddl_time of java class CubeFormat
--   FUNCTION checkcode RETURN VARCHAR2;
--
END schema_util;
```