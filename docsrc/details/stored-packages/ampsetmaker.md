# AMPSETMAKER PACKAGE {#ampsetmaker-stored-package} 
  
Functions to create new Strong Ground Motion (sm) ampset and assocevampset table rows for those events that pre-date production use of the ampgen/gmp2db/AmpGenPp code versions that populate these tables.  
  
  
```
CREATE OR REPLACE PACKAGE AMPSETMAKER AS
--
-- Use package functions to create new Strong Ground Motion (sm) ampset and assocevampset table rows
-- for those events that pre-date production use of the ampgen/gmp2db/AmpGenPp code versions
-- that populate these tables.
--
-- Process all evid whose prefmag >=3.5, selectflag=1 and etype='eq' and gtype in 'l','r' having a prefor UTC datetime
-- between the specified input dates. Input date strings are TRUETIME.string2true format ('YYYY/MM/DD HH:MM:SS')
-- For each candidate p_evid this function calls function evid2ampset(p_evid), Error ends loop processing.
-- Returns total number of candidate evid processed, or an error code < 0
FUNCTION evid2ampset(p_startdate VARCHAR2, p_enddate VARCHAR2) RETURN PLS_INTEGER;
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
--
-- Process all evid whose prefmag >=p_minmag, selectflag=1 and etype='eq' and gtype in 'l','r' having a prefor UTC datetime
-- between the specified input dates. Input date strings are TRUETIME.string2true format ('YYYY/MM/DD HH:MM:SS')
-- If input p_minmag is NULL the minimum prefmag magnitude value defaults to 3.5.
-- For each candidate p_evid this function calls function evid2ampset(p_evid).
-- Returns total number of candidate evid processed, or an error code < 0
FUNCTION evid2ampset(p_startdate VARCHAR2, p_enddate VARCHAR2, p_minmag NUMBER) RETURN PLS_INTEGER;
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
--
-- This function calls function evid2ampid(p_evid,p_ampsrc,p_setsrc) in the order:
--  evid2ampset(p_evid, 'RT', 'ampgen')  -- to make new ampset for any RT ampgen amps
--  evid2ampset(p_evid, 'GMP', 'Gmp2Db') -- to make new ampset for any imported Gmp2Db amps
--  evid2ampset(p_evid, 'AmpGenPp', 'AmpGenPp') -- to override RT ampset with new ampset for AmpGenPp
-- Returns number of new assocevampset rows created for input event, or an error code < 0
FUNCTION evid2ampset(p_evid EVENT.EVID%TYPE) RETURN PLS_INTEGER;
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
--
-- For input event make new ampset for those amps whose lowercase 1st 2-chars
-- of subsource matches that of p_ampsrc (e.g. 'RT1', 'AmpGenPp' 'GMP') and p_setsrc is the
-- corresponding program subsource set in the new assocevampset table row for this amp subsource,
-- the amp subsource to program subsource mappings are: RT=>ampgen, AmpGenPp=>AmpGenPp, and GMP=>Gmp2Db.
-- Invalidates pre-existing ampsets of same p_setsrc type for the input evid, before creating new entries.
-- Commits newly inserted rows if successful, else does rollback.
-- Returns number of new ampset rows created for the associated event ampset, or an error code < 0
FUNCTION evid2ampset(p_evid EVENT.EVID%TYPE, p_ampsrc AMP.SUBSOURCE%TYPE,
                    p_setsrc ASSOCEVAMPSET.subsource%TYPE) RETURN PLS_INTEGER;
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
--
-- Details of processing per event are logged using DBMS_OUTPUT.PUT_LINE until output buffer size is reached.
-- Enabling this mode is not recommended when processing a large number of events over a date range.
-- Setting max buffer size "set serveroutput on size 1000000"  is about 3000 events.
PROCEDURE LOGGING_ON;
PROCEDURE LOGGING_OFF;
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
--  Return SVN Id string.
FUNCTION getPkgId RETURN VARCHAR2;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
END AMPSETMAKER;
```