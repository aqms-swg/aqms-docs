# IMPORTEVENT PACKAGE {#importevent-stored-package}
  
## Specification  
  
```
-- NOTE: A right side assigment reference to an uninitialized table type element index
--      raises a NO_DATA_FOUND exception (e.g. tab_pkg_orid(ii) where ii value was never assigned).
--
-- TODO: Define records for each table type and explicitly declare each field in both
-- select and insert to allow for fact that physical table layout may differ between instances 
--
--    A.AMPID,A.COMMID,A.DATETIME,A.STA,A.NET,A.AUTH,A.SUBSOURCE,A.CHANNEL,A.CHANNELSRC,A.SEEDCHAN,A.LOCATION,
--    A.IPHASE,A.AMPLITUDE,A.AMPTYPE,A.UNITS,A.AMPMEAS,A.ERAMP,A.FLAGAMP,A.PER,A.SNR,A.TAU,A.QUALITY,A.RFLAG,
--    A.CFLAG,A.WSTART,A.DURATION,A.LDDATE
--
--   (AMPID,COMMID,DATETIME,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN,LOCATION,
--   IPHASE,AMPLITUDE,AMPTYPE,UNITS,AMPMEAS,ERAMP,FLAGAMP,PER,SNR,TAU,QUALITY,RFLAG,
--   CFLAG,WSTART,DURATION,LDDATE)
--
--CALL schema_util.revoke_all_privs('IMPORTEVENT');
--
-- Needs more row records defined to remove dependency on assumption that select "*" works.
--
-- NOTE: This implementation does not copy the event's AmpSet AssocEvAmpSet rows
--
CREATE OR REPLACE PACKAGE importevent AUTHID CURRENT_USER AS
--
-- Procedures/Functions needed to import parametric table data
-- associated with an event id (evid) from the specified database link 
-- and owning schema to the like named tables of the user's login schema.
-- New keys are assigned to data rows by the user's table sequencers.
--
-- Used in production: no, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   NONE
--
-- <DEPENDS ON>
--   All NCEDC parametric tables
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Return SVN Id string.
  FUNCTION getPkgId RETURN VARCHAR2;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Initialize package variables, checks user schema for all tables required by procedures/functions
  PROCEDURE init;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Level of processing info to output to log:
  --   0= only errors (or missing data) and event summary
  --   1= above plus per table totals logging
  --   2= above plus duplicate row key insert attempts
  PROCEDURE set_verbosity(p_ilevel PLS_INTEGER);
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- One-time special case here not mode for general use; invocation causes existing wfid to be propagated)
  PROCEDURE set_preserve_wfids(p_preserve BOOLEAN); 
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
  -- Copy to user login schema all event data associated with the specified input
  -- event id in the named input owning schema at the named input dbLink.
  -- Create new key id for event row and new keys all other parametric table rows.
  -- Returns event id number assigned in user schema.
  -- Example: call IMPORTEVENT.import_event(1111, 'TRINETDB', 'IRONDB') into :v_status;
  FUNCTION import_event(p_eid NUMBER, p_own VARCHAR2, p_dbLinkName VARCHAR2) RETURN NUMBER;
  --
  -- Like above, but if p_make_new_event = FALSE, the old event id key is preserved,
  -- only the related data rows will have new id keys.
  -- Returns new evid number if flag is true, otherwise reuses and returns imported evid.
  FUNCTION import_event(p_eid NUMBER, p_own VARCHAR2, p_dbLinkName VARCHAR2, p_make_new_event BOOLEAN) RETURN NUMBER;
-- -------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------
END importevent;
```