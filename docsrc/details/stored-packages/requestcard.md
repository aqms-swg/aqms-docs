# REQUESTCARD PACKAGE {#requestcard-stored-package} 
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE requestcard AUTHID CURRENT_USER AS
--
-- Procedure/Functions related to request_cards.
--
-- Used in production: by Jiggle only, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   Java org.trinet.jiggle.Jiggle (JiggleMenuBar)
--   Jiggle menu bar option uses batch procedure 
--
-- <DEPENDS ON>
--   No other packages or java, modifies NCEDC Application schema REQUEST_CARD table
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
    --  Return SVN Id string.
    FUNCTION getPkgId RETURN VARCHAR2;
    --
    -- Return SVN Id for package specification.
    FUNCTION getPkgSpecId RETURN VARCHAR2;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
    -- Batch processing procedure invoked by Jiggle menu bar option, this is just a wrapper around the make_t_request
    -- function since you can't do "batching" of function calls in Oracle jdbc.
    PROCEDURE batch_t_request(p_evid REQUEST_CARD.evid%TYPE,
                            p_auth REQUEST_CARD.auth%TYPE,
                            p_subsource REQUEST_CARD.subsource%TYPE,
                            p_staauth REQUEST_CARD.staauth%TYPE,
                            p_net REQUEST_CARD.net%TYPE,
                            p_sta REQUEST_CARD.sta%TYPE,
                            p_seedchan REQUEST_CARD.seedchan%TYPE,
                            p_location REQUEST_CARD.location%TYPE,
                            p_channel REQUEST_CARD.channel%TYPE,
                            p_start REQUEST_CARD.datetime_on%TYPE,
                            p_end REQUEST_CARD.datetime_off%TYPE,
                            p_commit PLS_INTEGER);
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Create request_card table rows for only for those channels that do not already have a waveform row associated with
-- an event evid in AssocWaE table. If an AssocWaE row already exists and also a request_card row exists for same channel
-- it resets that row's retry value=0. If no request_card entry exists for the channel and the AssocWaE row exists its a no-op. 
    FUNCTION make_t_request(p_evid REQUEST_CARD.evid%TYPE,
                            p_auth REQUEST_CARD.auth%TYPE,
                            p_subsource REQUEST_CARD.subsource%TYPE,
                            p_staauth REQUEST_CARD.staauth%TYPE,
                            p_net REQUEST_CARD.net%TYPE,
                            p_sta REQUEST_CARD.sta%TYPE,
                            p_seedchan REQUEST_CARD.seedchan%TYPE,
                            p_location REQUEST_CARD.location%TYPE,
                            p_channel REQUEST_CARD.channel%TYPE,
                            p_start REQUEST_CARD.datetime_on%TYPE,
                            p_end REQUEST_CARD.datetime_off%TYPE,
                            p_commit PLS_INTEGER) RETURN PLS_INTEGER;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return request processing priority using formula:
-- 90. + eventPrefMag - least(7,deltaDays(currentTime-requestStartTime)) - least(7,deltaDays(currentDate-requestLoadDate)) - (retry/4.)
-- If event prefmag magnitude is NULL, magnitude value defaults to 1.0. Returns 0. when no data found.
--
    FUNCTION getPriorityByEvid( p_evid  REQUEST_CARD.evid%TYPE, p_retry REQUEST_CARD.retry%TYPE) RETURN NUMBER;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return request processing priority using formula:
-- 90. + inputMagnitude - least(7,deltaDays(currentTime-originDatetime)/2.) - least(7,deltaDays(currentDate-originLddate)/2.) - (retry/4.)
-- If input input magnitude is NULL a value of 1.0 is used. Returns 0. when no data found.
--
    FUNCTION getPriorityByMag( 
                     p_mag  NETMAG.magnitude%TYPE,
                     p_retry REQUEST_CARD.retry%TYPE,
                     p_lddate ORIGIN.lddate%TYPE,
                     p_datetime ORIGIN.datetime%TYPE
    ) RETURN NUMBER;
-- ---------------------------------------------------------------------------------
END requestcard;
```