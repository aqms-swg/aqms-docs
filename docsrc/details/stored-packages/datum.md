# DATUM PACKAGE {#datum-stored-package} 
  
## Specification  
  
  
```
CREATE OR REPLACE PACKAGE DATUM AS
  --
  FUNCTION getPkgId RETURN VARCHAR2;
  --
  -- Calculate model offset datum as average of V_PKG_AVG_STA_CNT station elevations.
  V_PKG_AVG_STA_CNT PLS_INTEGER := 5;
  --
  -- Database default crust type (= 'H' for hypoinverse CRH) for update.
  V_PKG_CRUST_TYPE VARCHAR2(1) := 'H';
  --
  -- Default depth type for database ( = 'M' for model depth used by CISN)
  V_PKG_DEPTH_BASE VARCHAR2(2) := 'M';
  --
  -- Set value to match default in hypoinverse 1.41 value of -120 meters 
  V_PKG_MIN_ELEV PLS_INTEGER := -120;
  --
  -- Minimum elev meters of acceptable station (reject stations below this elev).
  PROCEDURE set_min_elev(p_elev PLS_INTEGER);
  --
  -- Return avg sta elev km of closest V_PKG_AVG_STA_CNT or fewer sta within 100 km with arrivals associated with the event.prefor origin.
  -- For case of 0 stations with elevations != 0 within 100 km, return the elevation of the closest arrival associated station.
  -- Return NULL when unable to calculate or no sta have elevations != 0 .
  -- e.g. no prefor data in database for matching the input evid.
  -- Return -999 for other execution exception.
  -- NOTE: Expects non-null assocaro.delta values for distances.
  FUNCTION event_prefor_datum(p_evid EVENT.evid%TYPE) RETURN NUMBER;
  --
  -- Return avg sta elev km of closest V_PKG_AVG_STA_CNT or fewer sta within 100 km with arrivals associated with the input origin.
  -- For case of 0 stations with elevations != 0 within 100 km, return the elevation of the closest arrival associated station.
  -- Return NULL when unable to calculate or no sta have elevations != 0 .
  -- e.g. no prefor data in database for matching the input evid.
  -- Return -999 for other execution exception.
  -- NOTE: Expects non-null assocaro.delta values for distances.
  FUNCTION origin_datum(p_orid ORIGIN.orid%TYPE) RETURN NUMBER;
  --
  -- Return avg sta elev km of closest p_num or fewer sta within 100 km with arrivals associated with the input origin.
  -- For case of 0 stations with elevations != 0 within 100 km, return the elevation of the closest arrival associated station.
  -- Return NULL when unable to calculate or no sta have elevations != 0 .
  -- e.g. no prefor data in database for matching the input evid.
  -- Return -999 for other execution exception.
  -- NOTE: Expects non-null assocaro.delta values for distances.
  FUNCTION origin_datum(p_orid ORIGIN.orid%TYPE, p_num PLS_INTEGER) RETURN NUMBER;
  --
  -- Return avg sta elev km of closest p_num or fewer sta within 100 km with arrivals associated with the input origin.
  -- For case of 0 stations with elevations != 0 within 100 km, return the elevation of the closest arrival associated station.
  -- Return NULL when unable to calculate or no sta have elevations != 0 .
  -- e.g. no prefor data in database for matching the input evid.
  -- Return -999 for other execution exception.
  -- NOTE: Calculates station distances using origin and station_data table lat,lon, thus much
  -- slower than using prepopulated assocaro.delta values as done by orgin_datum function.
  FUNCTION origin_datum2(p_orid ORIGIN.orid%TYPE, p_num PLS_INTEGER) RETURN NUMBER;
  --
  -- Return 1 if at least p_num assocaro arrivals exist having station elevation != 0 within 100 km (maybe not the closest); 0 otherwise.
  FUNCTION hasAssocArElev(p_orid ORIGIN.orid%TYPE, p_num PLS_INTEGER) RETURN NUMBER;
  --
  -- Return 1 if p_num closest distance assocaro arrivals have station elevation != 0 within 100 km; 0 otherwise.
  FUNCTION hasClosestAssocArElev(p_orid ORIGIN.orid%TYPE, p_num PLS_INTEGER) RETURN NUMBER;
  --
  -- Set depth to (depth - (avg sta elev km of closest V_PKG_AVG_STA_CNT or fewer sta with arrivals associated with input orid)),
  -- and set crust_type to V_PKG_CRUST_TYPE (default = 'H').
  -- NOTE: origin row's depth remains unchanged if the datum calculation failed for origin.
  -- Does a commit upon success.
  -- Return 1 on success.
  -- Return 0 when no update done for input orid.
  -- Return -999 for other execution exception.
  FUNCTION update_depth(p_orid ORIGIN.orid%TYPE) RETURN PLS_INTEGER;
  --
  -- Set depth to (depth - (avg sta elev km of closest V_PKG_AVG_STA_CNT or fewer sta with arrivals associated with input orid)),
  -- and set crust_type to V_PKG_CRUST_TYPE (default = 'H').
  -- NOTE: origin row's depth remains unchanged if the datum calculation failed for origin.
  -- Does a commit if p_commit >0.
  -- Return 1 on success.
  -- Return 0 when no update done for input orid.
  -- Return -999 for other execution exception.
  FUNCTION update_depth(p_orid ORIGIN.orid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  -- Update depth to depth minus datum for all origin rows associated with the input evid, where datum is value returned
  -- from origin_datum function, and set crust_type to V_PKG_CRUST_TYPE (default = 'H').
  -- Calls update_depth function for each origin. Does a database commit. 
  -- NOTE: an origin row's depth remains unchanged if the datum calculation failed for that origin.
  -- Return number of origins updated for event evid on success.
  -- Return 0 when no origin was updated for input evid.
  -- Return -999 for other execution exception.
  FUNCTION update_depths(p_evid EVENT.evid%TYPE) RETURN PLS_INTEGER;
  --
  -- Update depth to depth minus datum for all origin rows associated with the input evid, where datum is value returned
  -- from origin_datum function, and set crust_type to V_PKG_CRUST_TYPE (default = 'H').
  -- Calls update_depth function for each origin. Does a database commit in input p_commit=1. 
  -- NOTE: an origin row's depth remains unchanged if the datum calculation failed for that origin.
  -- Return number of origins updated for event on success.
  -- Return 0 when no update done for input evid.
  -- Return -999 for other execution exception.
  FUNCTION update_depths(p_evid EVENT.evid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  -- Update depth to (depth - datum) and set crust_type to V_PKG_CRUST_TYPE (default = 'H') of all events matching query
  -- filter conditions whose origins have UTC (leap) datetimes between the input datetimes UTC (leap).
  -- The filter conditions are: (WHAT ABOUT HAND FIXED DEPTH?)
  -- etype not in ('st','ts','sn','sh','nt','th') and bogusflag=0 and lon != 0. and algorithm != 'TMTS'
  -- These wrapper functions calls "update_depths" for each evid, which calculated a datum from the closet V_PKG_AVG_STA_CNT
  -- or fewer sta with arrivals associated with input orid.
  -- NOTE: origin row's depth remains unchanged if the datum calculation failed for origin.
  -- Return the number events updated on success.
  -- Return -999 for other execution exception.
  FUNCTION update_event_depths(p_startDate DATE, p_endDate DATE) RETURN PLS_INTEGER;
  FUNCTION update_event_depths(p_startString VARCHAR2, p_endString VARCHAR2) RETURN PLS_INTEGER;
  FUNCTION update_event_depths(p_startTime NUMBER, p_endTime NUMBER) RETURN PLS_INTEGER;
  FUNCTION update_event_depths(p_startTime NUMBER, p_endTime NUMBER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  -- Update depth to (depth - datum) and set crust_type to V_PKG_CRUST_TYPE (default = 'H') of all origins matching query
  -- filter conditions whose datetimes UTC (leap) are between -the input datetimes UTC (leap).
  -- The filter conditions are: (WHAT ABOUT HAND FIXED DEPTH?)
  -- etype not in ('st','sn','sh','nt','th') and o.gtype != 't' and bogusflag=0 and lon != 0. and algorithm != 'TMTS'
   --
  -- Wrapper function calls origin_datum function for each orid and datum calculation using V_PKG_AVG_STA_CNT
  -- or fewer sta with arrivals associated with input orid.
  -- NOTE: origin row's depth remains unchanged if the datum calculation failed for origin.
  -- Return value on success is the number of origins updated.
  -- Return the number origins updated on success.
  -- Return -999 for other execution exception.
  FUNCTION update_origin_depths(p_startTime NUMBER, p_endTime NUMBER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  FUNCTION update_origin_depths(p_startString VARCHAR2, p_endString VARCHAR2, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  -- FOR TESTING: 
  -- use after UPDATE_DEPTH function to set origin depth=depthM and crustal_type=NULL
  FUNCTION revert_depth(p_orid ORIGIN.orid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  -- use after UPDATE_DEPTHS function to set origin depth=depthM and crustal_type=NULL
  FUNCTION revert_depths(p_evid EVENT.evid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  -- use after UPDATE_EVENT_DEPTHS function to set origin depth=depthM and crustal_type=NULL
  FUNCTION revert_event_depths(p_startTime NUMBER, p_endTime NUMBER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  -- use after UPDATE_ORIGIN_DEPTHS function to set origin depth=depthM and crustal_type=NULL
  FUNCTION revert_origin_depths(p_startTime NUMBER, p_endTime NUMBER,  p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  -- use after UPDATE_ORIGIN_DEPTHS function to set origin depth=depthM and crustal_type=NULL
  FUNCTION revert_origin_depths(p_startString VARCHAR2, p_endString VARCHAR2, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  -- Return an origin depth of requested type, 'G' or 'M', using the specified inputs, where inputs should be those
  -- values read from the input orid's origin table row, or set NULL when columns not implemented in table or unknown.
  -- When the input MDEPTH value is null and the input CRUST_TYPE = 'E'|'L'|'V, the ORIGIN.DEPTH is presumed 'geoidal';
  -- for input CRUST_TYPE = 'H'|'T' the ORIGIN.DEPTH is presumed model depth. If geoidal depth is requested but only 
  -- model depth is available, returned depth is model depth minus the datum determined from the avg sta elev km of 
  -- the closest V_PKG_AVG_STA_CNT or fewer sta having arrivals associated with the input origin.
  -- Return -99 unable to determine requested depth, e.g. missing database data for datum calculation.
  -- Return -999 for other execution exception.
  FUNCTION getDepth(p_orid ORIGIN.orid%TYPE, p_depth NUMBER, p_mdepth NUMBER,
                    p_crust_type VARCHAR2, p_depth_type VARCHAR2) RETURN NUMBER;
  --
  -- Return G for a geoidal depth or M for a model depth legacy database configuration (CISN).
  FUNCTION getDepthBase RETURN VARCHAR2;
  --
  -- Change the value of the V_PKG_AVG_STA_CNT parameter, the number of station elevations averaged for the depth datum.
  PROCEDURE setAvgStaCount(p_count PLS_INTEGER);
  --
  -- Change value of the V_PKG_DEPTH_BASE parameter, 'M' for a model depth database, 'G' for a geoidal depth database.
  PROCEDURE setDepthBase(p_base VARCHAR2);
  --
  -- Change value of the V_PKG_CRUST_TYPE parameter, default is 'H' for CRH model depths.
  PROCEDURE setCrustType(p_type VARCHAR2);
  --
END datum;
```