# DELTAZ PACKAGE {#deltaz-stored-package}
  
## Specification  
  
```
CREATE OR REPLACE PACKAGE DELTAZ AS
  --
  FUNCTION getPkgId RETURN VARCHAR2;
  --
  FUNCTION update_deltaz(p_orid ORIGIN.orid%TYPE) RETURN PLS_INTEGER;
  FUNCTION update_deltaz(p_orid ORIGIN.orid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  FUNCTION update_deltazs(p_evid EVENT.evid%TYPE) RETURN PLS_INTEGER;
  FUNCTION update_deltazs(p_evid EVENT.evid%TYPE, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  FUNCTION update_event_deltazs(p_startDate DATE, p_endDate DATE) RETURN PLS_INTEGER;
  FUNCTION update_event_deltazs(p_startString VARCHAR2, p_endString VARCHAR2) RETURN PLS_INTEGER;
  FUNCTION update_event_deltazs(p_startTime NUMBER, p_endTime NUMBER) RETURN PLS_INTEGER;
  FUNCTION update_event_deltazs(p_startTime NUMBER, p_endTime NUMBER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
  FUNCTION update_origin_deltazs(p_startTime NUMBER, p_endTime NUMBER, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  FUNCTION update_origin_deltazs(p_startString VARCHAR2, p_endString VARCHAR2, p_commit PLS_INTEGER) RETURN PLS_INTEGER;
  --
END deltaz;
```