# %Amplitudes {#aqms-amplitude-product}  
  
    
%Amplitude computation is done as part of AQMS' automated event detection. %Peak amplitude values are calculated and stored in the AQMS database. This is done by a program called [ampgen](https://aqms-swg.gitlab.io/aqms-docs/man-ampgen.html). 

    
A program called [rad2](https://aqms-swg.gitlab.io/aqms-docs/man-rad2.html) takes raw waveform data from broadband and strong motion instruments and computes amplitude information for a specific length time window in real time. These amplitudes are return to the ADA (%Amplitude Data Area) for downstream modules to retrieve and process.  

The above two programs are automated and run in real-time.   
  
Ampgen can also be run in post-processing mode, the difference here is it calculates peak ground motion parameters from scratch by scanning the timeseries instead of using the peak amplitude values stored in the ADA. This is done via the [ampgenpp](https://aqms-swg.gitlab.io/aqms-docs/man-ampgenpp.html) utility. 
  

## %Amplitude data types <a name="amp-data-types"></a>  
  
\subpage amp-data-types describe the types of amplitude data used in the AQMS system.  
  
  

## %Amplitude exchange  
    
Selected strong motion amplitude information can be exchanged between seismic networks. For detailed information regarding this exchange, please see [Strong Motion Amplitude Exchange](ampexc.html)  

Imported amplitudes are associated with local events via [Amplitude Association](amp-association.html).      
  
### Data formats used for amplitude exchange  
    
- \subpage cisn-ground-motion-format
- \subpage strong-motion-format  
  

## Source code  
  
* **ampgen** : https://gitlab.com/aqms-swg/aqms-amplitude-generator  
* **rad2**   : https://gitlab.com/aqms-swg/aqms-rapid-amplitude-data
