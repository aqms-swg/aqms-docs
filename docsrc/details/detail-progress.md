Progress of the move to gitlab.com {#detail-progress}
=================================

Now that AQMS can use a PostgreSQL backend (Hartog et al., 2019),
the AQMS SWG decided it was worth spending the effort to move the source code
from subversion to git, and to host a public version at gitlab.com.

The goal is to finish this transfer by April or May of 2020.

Our progress is slow because we are doing this when we can find time inbetween
other duties.

The svn repository for AQMS is quite large and we decided to break the code
up in smaller chunks  each in its own git repository (aka gitlab project). All the projects are organized under a shared AQMS.SWG group. To be able to deploy documentation and code, we plan to provide several git "supermodules" (such as aqms-docs)
and use git submodules to incorporate the dependencies.

For more details see the SVN to git plan:

\ref detail-git-plan

* 2019/11/18 - Made the AQMS.SWG gitlab group public, also two of the repos: aqms-docs, aqms-dbselect.
* 2020/05/11 - AQMS.SWG/aqms-jiggle public.

\htmlonly
<a href="detail-git-plan.html">SVN to Git Plan</a> or back to
<a href="index.html">Starting Page</a>.
\endhtmlonly
