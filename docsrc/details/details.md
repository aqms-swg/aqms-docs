Detail Pages {#details}
========================
Detail Pages are explanatory pages about a topic relevant to AQMS.

- \subpage detail-progress
- \subpage detail-git-plan
