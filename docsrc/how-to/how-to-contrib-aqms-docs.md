How to contribute to the AQMS documentation {#how-to-contrib-aqms-docs}
========================================= 
  
## How to add documentation that is not generated from source code     

**NOTE :  All documentation is written in markdown and rendered into HTML via doxygen.** Markdown is easy to learn and has few simple formatting rules, please do not be daunted.  

**STEPS**  

1.  Go to gitlab.com and get an account, if you do not already have an account.  
2.  Email aqms-swg group to get access to https://gitlab.com/aqms-swg.
3.  **Enter a ticket under the documentation repo  https://gitlab.com/aqms-swg/aqms-docs/-/issues**    
4.  Navigate to aqms-docs → docsrc → details →\<your html file\>.md   
  
    For e.g. 
    HTML : https://aqms-swg.gitlab.io/aqms-docs/aqms-origin-product.html   
    Markdown file : https://gitlab.com/aqms-swg/aqms-docs/-/blob/master/docsrc/details/aqms-origin-product.md  
  
    **If you cannot find a .md file corresponding to your .html file, please mention in the ticket you've created in 3.** A stub will be created for you to fill the contents in for.  
  
5. To make your changes   

    a.  Click on the blue "Edit in Web IDE" button. This will open the .md file for editing, you can make your changes now. Please do not edit the title of the document.  

    b.  When you are ready to save your changes, click on the blue "Commit" button on the left hand panel. You can also view the differences between the changes you've made vs the existing version of documentation.   
      * Add a Commit Message.   
      * Keep "Create a new branch" selected. DEFAULT.    
      * Keep "Start a new merge request" checked. DEFAULT.    
      * Click on Commit to commit.  

    c. You will be taken to the page titled "New merge request". You can  
      * add a URL to the ticket or ticket number in the comments section.  
      * select "Assignees" and "Reviewers". Feel free to mark the Assignee as yourself using "Assign to me". 
      * select appropriate reviewers. Reviewers should review the document and provide feedback via the ticket create in 3.  
      
      Click on "Create merge request".  

    d. Once review is completed, you can merge your documentation. To do so, go to your merge request by clicking on "Merge requests" on the left panel. Ensure that "Delete source branch" is checked. Click on the blue "Merge" button.   

    e. Update your ticket to indicate the changes you have made. 
 
<br><br> 
## How to add source code documentation  
  

For a full explanation of how the AQMS documentation is built, see the details page \ref detail-docs.  This page just walks you to the required steps to add
documentation to AQMS source code in an AQMS repository, and if needed a manpage, or other documentation page. The AQMS Software WG has decided to use
Doxygen for the documentation, this document describes the practical rules to follow.  
  
Doxygen allows for quite a bit of flexibility, but it is important to understand following behaviors:

* Markdown files are automatically interpreted as "pages" and pages have their own hierarchy.
* Documentation for each AQMS component will be listed under the correct module (aka repository, aka project) if a group is defined for the module and documentation of the module include the `\ingroup group_modulename` doxygen tag.
* doxygen uses relative paths, relative to the directory from which it is run.

Definitions
-------------
Page types
* **Detail page**: Explanatory page that goes into detail about how or why. Use \c {\#detail-descriptive-name} as an identifier.
* **How-to page**: Succinct step-by-step instruction on how to do a task. Use \c {\#how-to-descriptive-name} as an identifier.
* **Man page**: Linux-style manpage. Use \c {\#man-program-name} as an identifier.
* **Tutorial**: Learn-by-example style tutorials, to step someone through an example. Use \c {\#tutorial-descriptive-name} as an identifier.

Repositories/Projects
* **aqms-docs**: the root "superproject", all the other AQMS repositories large git submodules of this project.
* **your repository**: the repository (aka project) that contains the code or program that you are writing documentation for.

Rules
-----
1. Pages are written in Markdown (.md) or reStructured text (.dox) in self- contained files inside a docsrc directory (folder).
2. Each executable has to have at least a manpage, with doxygen tag \c {\#man-program-name} behind the headline so that it can be referenced.
3. Code (classes, functions, methods, etc.) can be documented using any of the Doxygen code documentation options, see (http://www.doxygen.nl/manual/docblocks.html).
4. Do not add a doxygen \c \\page tag inside source file documentation, this disrupts the layout of the overall documentation pages.
5. Each AQMS repository has to have a group definition in the \c aqms-docs/doxygen/group_definitions.dox file.
6. Each documentation *page* has to be referred to by a \c \\subpage entry in one of the following files:
 * \c aqms-docs/docsrc/manpages/manpages.md
 * \c aqms-docs/docsrc/how-to/how-to.md
 * \c aqms-docs/docsrc/tutorials/tutorials.md
 * \c aqms-docs/docsrc/details/details.md

Steps
-----------------
1. Create a docsrc directory at the root of *your repository* if there is not one already.
2. Modify doc-strings to be doxygen compliant, or add new doxygen tags.
3. Add the \c \\ingroup group_modulename doxygen directive to each file in the repository to ensure it will be all grouped together. Leave off the
aqms- part of the module name, e.g. use \c group_dbselect, or \c group_libs, instead of \c group_aqms-dbselect and \c group_aqms-libs.
4. Write a manpage, and if appropriate, an how-to-install page, and, if you want, a detail page or tutorial as well.
5. Add a \c \\subpage identifier entry to the appropriate file in aqms-docs/docsrc, see Rule 6.

***Optional*** if you want to be able to create stand-alone documentation for just your repo: This documentation will NOT be used in the overall
documentation generated from aqms-docs. Place a Doxyfile configuration file in your docsrc directory, and specify the following parameters:
  * PROJECT_NAME
  * PROJECT_NUMBER (version number)
  * PROJECT_BRIEF
  * etc.
