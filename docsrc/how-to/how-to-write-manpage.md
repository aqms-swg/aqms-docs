# How to write a useful man page {#how-to-manpage}

- Keep it concise
- Use declarative language
- Describe all possible user options
- Describe file formats and/or environment variables
- List dependencies
- Add links to further documentation in the "More Information" section
