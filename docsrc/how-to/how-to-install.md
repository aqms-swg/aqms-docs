Installing AQMS {#how-to-install}
===============

Hardware and OS
----------------
AQMS has only been tested to run successfully under Linux CentOS7 or RHEL7, however,
it is expected that other linux flavors will work as well.

Most networks that run AQMS use a fully redundant system. To help you decide in what kind of configuration you may want
to run AQMS, check out the following guide:

\c \\subpage how-to-decide-layout

%Database Backend
----------------
If you decide to use a PostgreSQL database backend, read:

\c \\subpage how-to-aqms-db-pg

If you decide to use an Oracle database backend, read:

\c \\subpage how-to-aqms-db-ora

Earthworm
---------
