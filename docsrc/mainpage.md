AQMS Starting Page {#mainpage}
==================

**CAUTION: WORK IN PROGRESS**

The AQMS code is being moved to gitlab. If you are interested in
AQMS, checkout the
[AQMS Trac Wiki](https://vault.gps.caltech.edu/trac/cisn/) hosted
by Caltech and the article published in SRL
[Hartog et al., 2019] (TODO: add url) and return here
**mid-2020**.

This webpage is generated from this repository: 
https://gitlab.com/aqms-swg/aqms-docs

For Users
---------
\ref detail-progress

\ref detail-git-plan

For Contributors
----------------

Hello AQMS Software Working Group Members!

To find out how to contribute to the documentation, check out:

\ref how-to-contrib-aqms-docs

To learn about how this is organized, check out:

\ref detail-contrib-docs


Page Lists for Users
----------
- \subpage aqms-overview
- \subpage manpages 
- \subpage how-to
- \subpage details explanatory pages about an AQMS topic
- \subpage tutorials

Page Lists for Contributors
----------
- \subpage how-to-contrib
- \subpage details-contrib explanatory pages about an AQMS topic
- \subpage tutorials-contrib


\htmlonly
<a href="index.html">Starting Page</a> OR
<a href="detail-progress.html">Next</a>
\endhtmlonly
